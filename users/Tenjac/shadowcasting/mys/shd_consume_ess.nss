//::///////////////////////////////////////////////
//:: Name      Consume Essence
//:: FileName  mys_consume_ess.nss
//:://////////////////////////////////////////////
/**@file CONSUME ESSENCE
Master, Ebon Walls
Level/School: 9th/Necromancy [Death]
Range: Touch
Target: One living creature
Duration: Instantaneous, then 1 round/level (D); see text
Saving Throw: Will negates
Spell Resistance: Yes

The target of this horrid mystery must succeed on a Will
saving throw or die. If the creature succumbs to the mystery
and dies, it immediately returns to life, gains the dark creature
template, and is under your control. The creature remains in
this state for 1 round per level, and then dies again.

Author:    Tenjac
Created:   
*/
//:://////////////////////////////////////////////
//:://////////////////////////////////////////////


#include "prc_inc_spells"
#include "shd_inc_shdfunc"

void main()
{
        if(!X2PreSpellCastCode()) return;
        PRCSetSchool();
        
        
        
        PRCSetSchool();
}