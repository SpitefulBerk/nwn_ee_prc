ARCANE STRIKE [GENERAL]
You can channel arcane energy into your melee attacks.
Prerequisites: Ability to cast 3rd-level arcane spells,
base attack bonus +4.
Benefit: When you activate this feat (a free action that does
not provoke an attack of opportunity), you can channel arcane
energy into a melee weapon, your unarmed strike, or natural
weapons. You must sacrifice one of your spells for the day (of
1st level or higher) to do this, but you gain a bonus on all your
attack rolls for 1 round equal to the level of the spell sacrificed,
as well as extra damage equal to 1d4 points � the level of the
spell sacrificed. The bonus you add to your attack rolls from
this feat cannot be greater than your base attack bonus.