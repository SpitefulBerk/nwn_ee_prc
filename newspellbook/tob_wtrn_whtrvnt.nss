/*
   ----------------
   White Raven Tactics

   tob_wtrn_whtrvnt.nss
   ----------------

    18/08/07 by Stratovarius
*/ /** @file

    White Raven Tactics

    Iron Heart (Boost)
    Level: Crusader 3, Warblade 3
    Initiation Action: 1 Swift Action
    Range: 10 ft.
    Target: One ally

    You can inspire your allies to astounding feats of martial prowess
    With a few short orders, you cajole them into seizing the initiative
    and driving back the enemy.
    
    Your ally gets to go again this turn. You cannot target yourself with this maneuver.
*/

#include "tob_inc_move"
#include "tob_movehook"
#include "prc_alterations"

void main()
{
    if(!PreManeuverCastCode()) return;

    object oInitiator    = OBJECT_SELF;
    object oTarget       = PRCGetSpellTargetObject();
    struct maneuver move = EvaluateManeuver(oInitiator, oTarget);

    if(move.bCanManeuver)
    {
        // 6s time stop for target
        effect eTime = EffectTimeStop();
        if(GetPRCSwitch(PRC_TIMESTOP_LOCAL))
        {
            eTime = EffectAreaOfEffect(VFX_PER_NEW_TIMESTOP);
            eTime = EffectLinkEffects(eTime, EffectEthereal());
        }
        ApplyEffectToObject(DURATION_TYPE_TEMPORARY, ExtraordinaryEffect(eTime), oTarget, 6.0);
    }
}