//::///////////////////////////////////////////////
//:: Name      Eldritch Glaive
//:: FileName  inv_eldrtch_glv.nss
//::///////////////////////////////////////////////
/*

Least Invocation
Blast Shape
2nd Level Spell

Your eldritch blast takes on the physical form of a
glaive. If you hit with the glaive, the target is
hit as if with your eldritch blast, including any
essence applied. The glaive dissipates next round.

*/
//::///////////////////////////////////////////////

#include "prc_inc_combat"
#include "inv_inc_invfunc"

void main()
{
    object oPC = OBJECT_SELF;
    object oTarget = PRCGetSpellTargetObject();
    object oGlaive = CreateItemOnObject("prc_eldrtch_glv", oPC);
    int nAtkBns = GetHasFeat(FEAT_ELDRITCH_SCULPTOR, oPC) ? 2 : 0;
    nAtkBns += GetAttackBonus(oTarget, oPC, OBJECT_INVALID, FALSE, TOUCH_ATTACK_MELEE_SPELL);

    // Construct the bonuses
    itemproperty ipAddon = ItemPropertyOnHitCastSpell(IP_CONST_CASTSPELL_ELDRITCH_GLAIVE_ONHIT, (GetInvokerLevel(oPC, CLASS_TYPE_WARLOCK) + 1) / 2);
    AddItemProperty(DURATION_TYPE_PERMANENT, ipAddon, oGlaive);

    // Force equip
    ClearAllActions();
    AssignCommand(oPC, ActionEquipItem(oGlaive, INVENTORY_SLOT_RIGHTHAND));

    // Make even more sure the glaive cannot be dropped
    SetDroppableFlag(oGlaive, FALSE);
    SetItemCursedFlag(oGlaive, TRUE);

    //Set up to delete after duration ends
    DestroyObject(oGlaive, 6.0f);

    //Schedule the attack
    DelayCommand(1.0, PerformAttackRound(oTarget, oPC, EffectVisualEffect(VFX_IMP_MAGBLUE),
        0.0, nAtkBns, 0, DAMAGE_TYPE_SLASHING, FALSE, "*Eldritch Glaive Hit*", "*Eldritch Glaive Miss*",
        TRUE, TOUCH_ATTACK_MELEE, FALSE, PRC_COMBATMODE_ALLOW_TARGETSWITCH|PRC_COMBATMODE_ABORT_WHEN_OUT_OF_RANGE));

    //Fire cast spell at event for the specified target
    DelayCommand(2.0, SignalEvent(oTarget, EventSpellCastAt(oPC, INVOKE_ELDRITCH_BLAST)));
}