//::///////////////////////////////////////////////
//:: Warlock
//:: inv_warlock.nss
//::///////////////////////////////////////////////
/*
    Handles the passive bonuses for Warlocks
*/
//:://////////////////////////////////////////////
//:: Created By: Fox
//:: Created On: Feb 29, 2008
//:://////////////////////////////////////////////

#include "prc_alterations"
#include "inv_inc_invfunc"

// Armour and Shield combined, up to Medium/Large shield.
/*void ReducedASF(object oCreature, object oSkin)
{
    object oArmor = GetItemInSlot(INVENTORY_SLOT_CHEST, oCreature);
    object oShield = GetItemInSlot(INVENTORY_SLOT_LEFTHAND, oCreature);
    int nAC = GetBaseAC(oArmor);
    int iBonus = GetLocalInt(oSkin, "WarlockArmourCasting");
    int nASF = -1;
    itemproperty ip;

    // First thing is to remove old ASF (in case armor is changed.)
    if (iBonus != -1)
        RemoveSpecificProperty(oSkin, ITEM_PROPERTY_ARCANE_SPELL_FAILURE, -1, iBonus, 1, "WarlockArmourCasting");

    // As long as they meet the requirements, just give em max ASF reduction
    // I know it could cause problems if they have increased ASF, but thats unlikely
    if (3 >= nAC && GetBaseItemType(oShield) != BASE_ITEM_TOWERSHIELD && GetBaseItemType(oShield) != BASE_ITEM_LARGESHIELD)
        nASF = IP_CONST_ARCANE_SPELL_FAILURE_MINUS_25_PERCENT;

    // Apply the ASF to the skin.
    ip = ItemPropertyArcaneSpellFailure(nASF); 

    AddItemProperty(DURATION_TYPE_PERMANENT, ip, oSkin);
    SetLocalInt(oSkin, "WarlockArmourCasting", nASF);
}*/

void main()
{
    object oPC = OBJECT_SELF;
    object oSkin = GetPCSkin(oPC);
    int nClass = GetLevelByClass(CLASS_TYPE_WARLOCK, oPC);
    int nHellFire = GetLevelByClass(CLASS_TYPE_HELLFIRE_WARLOCK, oPC);
    int nTheurge = GetLevelByClass(CLASS_TYPE_ELDRITCH_THEURGE, oPC);

    //ReducedASF(oPC, oSkin);

    //Reduction
    if(nClass > 2 || nTheurge)
    {
        int nRedAmt = (nClass + 1) / 4;
            nRedAmt += (nTheurge + 2) / 3;
        effect eReduction = EffectDamageReduction(nRedAmt, DAMAGE_POWER_PLUS_THREE);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, ExtraordinaryEffect(eReduction), oPC);
    }

    if(GetHasFeat(FEAT_WARLOCK_SHADOWMASTER, oPC))
        AddSkinFeat(FEAT_WARLOCK_SHADOWMASTER_SHADES, IP_CONST_FEAT_SHADOWMASTER_SHADES, oSkin, oPC);

    if(GetHasFeat(FEAT_PARAGON_VISIONARY, oPC))
    {
        effect eTS1 = EffectUltravision();
        effect eTS2 = EffectSeeInvisible();
        int nSkillBonus = max(2 * GetAbilityModifier(ABILITY_WISDOM, oPC), 6);
        effect eSpot = EffectSkillIncrease(SKILL_SPOT, nSkillBonus);
        effect eListen = EffectSkillIncrease(SKILL_LISTEN, nSkillBonus);
        effect eSnsMtv = EffectSkillIncrease(SKILL_SENSE_MOTIVE, nSkillBonus);
        effect eLink = EffectLinkEffects(eTS1, eTS2);
        eLink = EffectLinkEffects(eLink, eSpot);
        eLink = EffectLinkEffects(eLink, eListen);
        eLink = EffectLinkEffects(eLink, eSnsMtv);
        eLink = ExtraordinaryEffect(eLink);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, eLink, oPC);
    }

    //Energy Resistance
    int nResistAmt = nClass >= 20 ? 10 : 5;
    int nMotE = GetHasFeat(FEAT_MASTER_OF_THE_ELEMENTS, oPC) ? 10 : 0;

    if(GetHasFeat(FEAT_WARLOCK_RESIST_ACID, oPC))
    {
        itemproperty ipIP = ItemPropertyDamageResistance(IP_CONST_DAMAGETYPE_ACID, nResistAmt + nMotE);
        IPSafeAddItemProperty(oSkin, ipIP, 0.0, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING, FALSE, FALSE);
    }
    if(GetHasFeat(FEAT_WARLOCK_RESIST_COLD, oPC))
    {
        itemproperty ipIP = ItemPropertyDamageResistance(IP_CONST_DAMAGETYPE_COLD, nResistAmt + nMotE);
        IPSafeAddItemProperty(oSkin, ipIP, 0.0, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING, FALSE, FALSE);
    }
    if(GetHasFeat(FEAT_WARLOCK_RESIST_ELEC, oPC))
    {
        itemproperty ipIP = ItemPropertyDamageResistance(IP_CONST_DAMAGETYPE_ELECTRICAL, nResistAmt + nMotE);
        IPSafeAddItemProperty(oSkin, ipIP, 0.0, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING, FALSE, FALSE);
    }
    if(GetHasFeat(FEAT_WARLOCK_RESIST_SONIC, oPC))
    {
        itemproperty ipIP = ItemPropertyDamageResistance(IP_CONST_DAMAGETYPE_SONIC, nResistAmt + nMotE);
        IPSafeAddItemProperty(oSkin, ipIP, 0.0, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING, FALSE, FALSE);
    }

    int nFireResist;
    if(GetHasFeat(FEAT_WARLOCK_RESIST_FIRE, oPC))
        nFireResist = nResistAmt;
    if(nHellFire > 1)
        nFireResist += 10;
    if(nFireResist)
    {
        itemproperty ipIP = ItemPropertyDamageResistance(IP_CONST_DAMAGETYPE_FIRE, nFireResist + nMotE);
        IPSafeAddItemProperty(oSkin, ipIP, 0.0, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING, FALSE, FALSE);
    }

    //Hellfire Warlock
    if(nHellFire)
    {
        if(GetHasInvocation(INVOKE_ELDRITCH_SPEAR, oPC))
            AddSkinFeat(FEAT_HELLFIRE_SPEAR, IP_FEAT_HELLFIRE_SPEAR, oSkin, oPC);

        if(GetHasInvocation(INVOKE_ELDRITCH_GLAIVE, oPC))
            AddSkinFeat(FEAT_HELLFIRE_GLAIVE, IP_FEAT_HELLFIRE_GLAIVE, oSkin, oPC);

        if(GetHasInvocation(INVOKE_HIDEOUS_BLOW, oPC))
            AddSkinFeat(FEAT_HELLFIRE_BLOW, IP_FEAT_HELLFIRE_BLOW, oSkin, oPC);

        if(GetHasInvocation(INVOKE_ELDRITCH_CHAIN, oPC))
            AddSkinFeat(FEAT_HELLFIRE_CHAIN, IP_FEAT_HELLFIRE_CHAIN, oSkin, oPC);

        if(GetHasInvocation(INVOKE_ELDRITCH_CONE, oPC))
            AddSkinFeat(FEAT_HELLFIRE_CONE, IP_FEAT_HELLFIRE_CONE, oSkin, oPC);

        if(GetHasInvocation(INVOKE_ELDRITCH_LINE, oPC))
            AddSkinFeat(FEAT_HELLFIRE_LINE, IP_FEAT_HELLFIRE_LINE, oSkin, oPC);

        if(GetHasInvocation(INVOKE_ELDRITCH_DOOM, oPC))
            AddSkinFeat(FEAT_HELLFIRE_DOOM, IP_FEAT_HELLFIRE_DOOM, oSkin, oPC);
    }
}