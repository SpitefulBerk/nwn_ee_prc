//::///////////////////////////////////////////////
//:: Name      Chilling Fog
//:: FileName  inv_dra_chillfog.nss
//::///////////////////////////////////////////////
/*

Greater Invocation
6th Level Spell

A misty cloud expands around a point you designate,
granting 20% concealment to any within. The cloud
is so thick any within are slowed to a crawl, and
attacks by those within are at a -2 penalty. In
addition, any within take 2d6 points of cold
damage per round. This cloud lasts for 1 minute per
level before dispersing.

*/
//::///////////////////////////////////////////////

#include "inv_inc_invfunc"
#include "inv_invokehook"

void DispelMonitor(object oAoE);

void main()
{
    if(!PreInvocationCastCode()) return;

    object oCaster = OBJECT_SELF;
    if(GetLocalInt(oCaster, "ChillingFogLock"))
    {
        SendMessageToPC(oCaster, "You can only have one Chilling Fog cast at any given time.");
        return;
    }

    //Declare major variables including Area of Effect Object
    location lTarget = PRCGetSpellTargetLocation();
    int CasterLvl = GetInvokerLevel(oCaster, GetInvokingClass());
    float fDuration = TurnsToSeconds(CasterLvl);
    effect eAOE = EffectAreaOfEffect(INVOKE_AOE_CHILLFOG);
    effect eImpact = EffectVisualEffect(257);
    ApplyEffectAtLocation(DURATION_TYPE_INSTANT, eImpact, lTarget);


    //Create an instance of the AOE Object using the Apply Effect function
    ApplyEffectAtLocation(DURATION_TYPE_TEMPORARY, eAOE, lTarget, fDuration);

    SetLocalInt(oCaster, "ChillingFogLock", TRUE);
    DelayCommand(fDuration, DeleteLocalInt(oCaster, "ChillingFogLock"));

    // Get an object reference to the newly created AoE
    object oAoE = GetFirstObjectInShape(SHAPE_SPHERE, 1.0f, lTarget, FALSE, OBJECT_TYPE_AREA_OF_EFFECT);
    while(GetIsObjectValid(oAoE))
    {
        // Test if we found the correct AoE
        if(GetTag(oAoE) == Get2DACache("vfx_persistent", "LABEL", INVOKE_AOE_CHILLFOG))
        {
            break;
        }
        // Didn't find, get next
        oAoE = GetNextObjectInShape(SHAPE_SPHERE, 1.0f, lTarget, FALSE, OBJECT_TYPE_AREA_OF_EFFECT);
    }
    DispelMonitor(oAoE);
}

void DispelMonitor(object oAoE)
{
    // Has the power ended since the last beat, or does the duration run out now
    if(GetIsObjectValid(oAoE))
    {
        if(DEBUG) DoDebug("inv_dra_chillfog: The lock effect has been removed");
        DeleteLocalInt(OBJECT_SELF, "ChillingFogLock");
    }
    else
       DelayCommand(6.0f, DispelMonitor(oAoE));
}