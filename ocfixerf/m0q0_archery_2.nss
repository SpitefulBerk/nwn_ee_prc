// Added compatibility for PRC base classes
#include "prc_class_const"

void main()
{
    object oAttacker = GetLastAttacker();

//* Should only register if hit with a ranged weapon
    if(GetWeaponRanged(GetLastWeaponUsed(oAttacker)))
    {
        SpeakOneLinerConversation();
        if(GetLocalInt(GetModule(),"NW_G_M0Q01_FIGHTER_TEST") > 0 &&
           GetLocalInt(GetModule(), "NW_PROLOGUE_PLOT") != 99)
        {
            if(GetDistanceToObject(oAttacker) > 12.0)
            {
                if(GetLevelByClass(CLASS_TYPE_ANTI_PALADIN, oAttacker)
                || GetLevelByClass(CLASS_TYPE_BARBARIAN, oAttacker)
                || GetLevelByClass(CLASS_TYPE_BARD, oAttacker)
                || GetLevelByClass(CLASS_TYPE_BOWMAN, oAttacker)
                || GetLevelByClass(CLASS_TYPE_BRAWLER, oAttacker)
                || GetLevelByClass(CLASS_TYPE_CRUSADER, oAttacker)
                || GetLevelByClass(CLASS_TYPE_CW_SAMURAI, oAttacker)
                || GetLevelByClass(CLASS_TYPE_DRAGON_SHAMAN, oAttacker)
                || GetLevelByClass(CLASS_TYPE_DUSKBLADE, oAttacker)
                || GetLevelByClass(CLASS_TYPE_FIGHTER, oAttacker)
                || GetLevelByClass(CLASS_TYPE_HEXBLADE, oAttacker)
                || GetLevelByClass(CLASS_TYPE_KNIGHT, oAttacker)
                || GetLevelByClass(CLASS_TYPE_MARSHAL, oAttacker)
                || GetLevelByClass(CLASS_TYPE_MONK, oAttacker)
                || GetLevelByClass(CLASS_TYPE_NOBLE, oAttacker)
                || GetLevelByClass(CLASS_TYPE_PALADIN, oAttacker)
                || GetLevelByClass(CLASS_TYPE_PSYWAR, oAttacker)
                || GetLevelByClass(CLASS_TYPE_RANGER, oAttacker)
                || GetLevelByClass(CLASS_TYPE_SAMURAI, oAttacker)
                || GetLevelByClass(CLASS_TYPE_SCOUT, oAttacker)
                || GetLevelByClass(CLASS_TYPE_SOHEI, oAttacker)
                || GetLevelByClass(CLASS_TYPE_SOULKNIFE, oAttacker)
                || GetLevelByClass(CLASS_TYPE_SWASHBUCKLER, oAttacker)
                || GetLevelByClass(CLASS_TYPE_SWORDSAGE, oAttacker)
                || GetLevelByClass(CLASS_TYPE_TRUENAMER, oAttacker)
                || GetLevelByClass(CLASS_TYPE_ULTIMATE_RANGER, oAttacker)
                || GetLevelByClass(CLASS_TYPE_WARBLADE, oAttacker))
                {
                    SetLocalInt(GetModule(),"NW_G_M1Q0BRanged",TRUE);
                    if(GetLocalInt(GetModule(),"NW_G_M1Q0BMelee"))
                    {
                        SetLocalInt(GetModule(),"NW_G_M0Q01_FIGHTER_TEST",2);
                    }
                }
                else
                {
                    SetLocalInt(GetModule(),"NW_G_M0Q01_NONFIGHTER_PASS",TRUE);
                }
                AssignCommand(GetNearestObjectByTag("M1Q0BHewwet"),SpeakOneLinerConversation("",oAttacker));
            }
            else
            {
                AssignCommand(GetNearestObjectByTag("M1Q0BHewwet"),SpeakOneLinerConversation("",oAttacker));
            }
        }
    }
}