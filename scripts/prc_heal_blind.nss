/*
    Healer's Remove Blindness/Deafness
*/

#include "prc_alterations"

void main()
{
    // This is all it does.
    DoRacialSLA(SPELL_REMOVE_BLINDNESS_AND_DEAFNESS, GetHitDice(OBJECT_SELF));
}