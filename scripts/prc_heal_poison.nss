/*
    Healer's Remove Poison
*/

#include "prc_alterations"

void main()
{
    // This is all it does.
    DoRacialSLA(SPELL_NEUTRALIZE_POISON, GetHitDice(OBJECT_SELF));
}