//::///////////////////////////////////////////////
//:: Aura of Fear
//:: apal_aurafear.nss
//:://////////////////////////////////////////////
/*
    You gain an evil aura, enemies suffers a -4
    against fear effects and you're immune to fear.
*/
//:://////////////////////////////////////////////

#include "prc_inc_spells"

void main()
{
    object oCaster = OBJECT_SELF;

    if(GetHasSpellEffect(SPELL_ANTIPAL_AURAFEAR))
    {
        PRCRemoveEffectsFromSpell(oCaster, SPELL_ANTIPAL_AURAFEAR);
        return;
    }

    //Set and apply AOE object
    effect eAOE = ExtraordinaryEffect(EffectAreaOfEffect(AOE_MOB_DRAGON_FEAR, "apal_aurafeara", "", "apal_aurafearb"));
    ApplyEffectToObject(DURATION_TYPE_PERMANENT, eAOE, oCaster);

    //Apply immunity effect
    effect eImmu = EffectImmunity(IMMUNITY_TYPE_FEAR);
    ApplyEffectToObject(DURATION_TYPE_PERMANENT, ExtraordinaryEffect(eImmu), oCaster);
}
