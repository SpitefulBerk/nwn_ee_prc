#include "prc_inc_combmove"
#include "prc_inc_clsfunc"

void main()
{
    object oPC = OBJECT_SELF;
    int nClass = GetLevelByClass(CLASS_TYPE_BOWMAN, oPC);
    int nDex = GetAbilityModifier(ABILITY_DEXTERITY, oPC);

    // Speed bonus. +10 feet at level 1
    // remove old effect first
    effect eTest = GetFirstEffect(oPC);
    while(GetIsEffectValid(eTest))
    {
        if(GetEffectType(eTest) == EFFECT_TYPE_MOVEMENT_SPEED_INCREASE)
        {
            if(GetEffectSubType(eTest) == SUBTYPE_EXTRAORDINARY)
            {
                if(GetEffectDurationType(eTest) == DURATION_TYPE_PERMANENT)
                    RemoveEffect(oPC, eTest);
            }
        }
        eTest = GetNextEffect(oPC);
    }
    // Light armour only
    if(GetBaseAC(GetItemInSlot(INVENTORY_SLOT_CHEST, oPC)) < 6)
    {
        DelayCommand(0.0, ApplyEffectToObject(DURATION_TYPE_PERMANENT, ExtraordinaryEffect(EffectMovementSpeedIncrease(33)), oPC));
    }

    //needed from level 5 for Sniper Skill
    if(nClass > 4 && nDex > 0)
    {
        object oItem;
        string sTest = "prc_bowman";
        int iEquip = GetLocalInt(oPC, "ONEQUIP");
        int ipDam = GetIntToDamage(nDex);
        int ipDamType, ipOld;
        //OnEquip - add bonus damage
        if(iEquip == 2)
        {
            oItem = GetItemLastEquipped();
            ipDamType = ipBowmanDmgType(oItem);
            if(ipDamType > -1)
            {
                IPSafeAddItemProperty(oItem, ItemPropertyDamageBonus(ipDamType, ipDam), 99999.0, X2_IP_ADDPROP_POLICY_KEEP_EXISTING, FALSE, FALSE);
                SetLocalInt(oItem, sTest, ipDam);
                return;
            }
        }
        //OnUnequip - remove bonus damage
        else if(iEquip == 1)
        {
            oItem = GetItemLastUnequipped();
            ipDamType = ipBowmanDmgType(oItem);
            if(ipDamType > -1)
            {
                ipOld = GetLocalInt(oItem, sTest);
                RemoveSpecificProperty(oItem, ITEM_PROPERTY_DAMAGE_BONUS, ipDamType, ipOld, 1, "", -1, DURATION_TYPE_TEMPORARY);
                DeleteLocalInt(oItem, sTest);
                return;
            }
        }
        //everything else - update damage if DEX changed
        oItem = GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oPC);
        ipDamType = ipBowmanDmgType(oItem);
        if(ipDamType > -1)
        {
            ipOld = GetLocalInt(oItem, sTest);
            if(ipDam != ipOld)
            {
                RemoveSpecificProperty(oItem, ITEM_PROPERTY_DAMAGE_BONUS, ipDamType, ipOld, 1, "", -1, DURATION_TYPE_TEMPORARY);
                IPSafeAddItemProperty(oItem, ItemPropertyDamageBonus(ipDamType, ipDam), 99999.0, X2_IP_ADDPROP_POLICY_KEEP_EXISTING, FALSE, FALSE);
                SetLocalInt(oItem, sTest, ipDam);
            }
        }
    }
}