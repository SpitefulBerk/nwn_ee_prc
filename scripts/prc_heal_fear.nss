/*
    Healer's Remove Fear
*/

#include "prc_alterations"

void main()
{
    // This is all it does.
    DoRacialSLA(SPELL_REMOVE_FEAR, GetHitDice(OBJECT_SELF));
}