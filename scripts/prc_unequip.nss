//::///////////////////////////////////////////////
//:: Example XP2 OnItemEquipped
//:: x2_mod_def_unequ
//:: (c) 2003 Bioware Corp.
//:://////////////////////////////////////////////
/*
    Put into: OnUnEquip Event
*/
//:://////////////////////////////////////////////
//:: Created By: Georg Zoeller
//:: Created On: 2003-07-16
//:://////////////////////////////////////////////
#include "prc_inc_function"
#include "inc_timestop"
#include "prc_inc_itmrstr"
#include "prc_inc_wpnrest"

void PrcFeats(object oPC, object oItem)
{
     SetLocalInt(oPC,"ONEQUIP",1);
     EvalPRCFeats(oPC);

     //this is to remove effect-related itemproperties
     //in here to take advantage of the local
     //I didnt put it in EvalPRCfeats cos I cant be bothered to wait for the compile
     //Ill probably move it at some point, dont worry
     //Primogenitor
     CheckPRCLimitations(oItem, oPC);

     DeleteLocalInt(oPC,"ONEQUIP");
}

void DoWeaponUnequip(object oPC, object oItem);

//Added hook into EvalPRCFeats event
//  Aaon Graywolf - 6 Jan 2004
//Added delay to EvalPRCFeats event to allow module setup to take priority
//  Aaon Graywolf - Jan 6, 2004
void main()
{
    object oItem = GetItemLastUnequipped();
    object oPC   = GetItemLastUnequippedBy();

//if(DEBUG) DoDebug("Running OnUnEquip, creature = '" + GetName(oPC) + "' is PC: " + DebugBool2String(GetIsPC(oPC)) + "; Item = '" + GetName(oItem) + "' - '" + GetTag(oItem) + "'");

    DoTimestopUnEquip(oPC, oItem);

    // Delay a bit to prevent TMI due to polymorph effect being braindead and running the unequip script for each and
    // bloody every item the character has equipped at the moment of effect application. Without detaching the script
    // executions from the script that does the effect application. So no instruction counter resets.
    // This will probably smash some scripts to pieces, at least when multiple unequips happen at once. Hatemail
    // to nwbugs@bioware.com please.

    DelayCommand(0.0f, PrcFeats(oPC, oItem));

    /* Does not seem to work, the effect is not listed yet when the unequip scripts get run
    if(PRCGetHasEffect(EFFECT_TYPE_POLYMORPH, oPC))
    {
        DoDebug("prc_unequip: delayed call");
        DelayCommand(0.0f, PrcFeats(oPC));
    }
    else
    {
        DoDebug("prc_unequip: direct call");
        PrcFeats(oPC);
    }*/

    DoWeaponUnequip(oPC, oItem);

    // Execute scripts hooked to this event for the player triggering it
    ExecuteAllScriptsHookedToEvent(oPC, EVENT_ONPLAYERUNEQUIPITEM);
    ExecuteAllScriptsHookedToEvent(oItem, EVENT_ITEM_ONPLAYERUNEQUIPITEM);

    // Tag-based scripting hook for PRC items
    SetUserDefinedItemEventNumber(X2_ITEM_EVENT_UNEQUIP);
    ExecuteScript("is_"+GetTag(oItem), OBJECT_SELF);
}

void DoWeaponUnequip(object oPC, object oItem)
{
    //initialize variables
    int nRealSize = PRCGetCreatureSize(oPC);  //size for Finesse/TWF
    int nSize = nRealSize;                    //size for equipment restrictions
    int nWeaponSize = GetWeaponSize(oItem);

    //Powerful Build bonus
    if(GetHasFeat(FEAT_RACE_POWERFUL_BUILD, oPC))
        nSize++;

    if(DEBUG) DoDebug("prc_restwpnsize - OnUnEquip");

    // remove any TWF penalties
    //if weapon was a not light, and there's still something equipped in the main hand(meaning that an offhand item was de-equipped)
    if(nWeaponSize == nRealSize && GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oPC) != OBJECT_INVALID)
    {
        //remove any effects added
        SetCompositeAttackBonus(oPC, "OTWFPenalty", 0);
    }
    //remove any simulated finesse
    if(GetHasFeat(FEAT_WEAPON_FINESSE, oPC) && nWeaponSize != -1)
    {
        //if left hand unequipped, clear 
        //if right hand unequipped, left hand weapon goes to right hand, so should still be cleared
        SetCompositeAttackBonus(oPC, "ElfFinesseLH", 0, ATTACK_BONUS_OFFHAND);
        //if right hand weapon unequipped
        if(GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oPC) == oItem)
            SetCompositeAttackBonus(oPC, "ElfFinesseRH", 0, ATTACK_BONUS_ONHAND);
    }

    //remove any two-handed weapon bonus
    SetCompositeDamageBonusT(oItem, "THFBonus", 0);

    //clean elven thinblade-simulated bonuses
    DoWeaponFeatUnequip(oPC, oItem, ATTACK_BONUS_OFFHAND);
    if(GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oPC) == oItem)
        DoWeaponFeatUnequip(oPC, oItem, ATTACK_BONUS_ONHAND);

    //clean any nonproficient penalties
    SetCompositeAttackBonus(oPC, "Unproficient" + IntToString(ATTACK_BONUS_OFFHAND), 0, ATTACK_BONUS_OFFHAND);
    if(GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oPC) == oItem)
        SetCompositeAttackBonus(oPC, "Unproficient" + IntToString(ATTACK_BONUS_ONHAND), 0, ATTACK_BONUS_ONHAND);
}