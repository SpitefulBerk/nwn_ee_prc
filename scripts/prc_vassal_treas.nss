#include "prc_class_const"
#include "inc_persist_loca"

void main()
{
    object oPC = OBJECT_SELF;
    object oToken = GetHideToken(oPC);
    int nVassal = GetLevelByClass(CLASS_TYPE_VASSAL, oPC);

    if(GetLocalInt(oToken, "VassalLvl") >= nVassal)
        return;

    // *Level 10
    if(nVassal == 10)
    {
        // *platinum Armor +8
        DestroyObject(GetItemPossessedBy(oPC, "PlatinumArmor6"));
        CreateItemOnObject("Platinumarmor8", oPC, 1);
        SetLocalInt(oToken, "VassalLvl", 10);
    }
    // *Level 8
    else if(nVassal == 8)
    {
        // *Shared Trove
        GiveGoldToCreature(oPC, 80000);
        SetLocalInt(oToken, "VassalLvl", 8);
    }
    // *Level 5
    else if(nVassal == 5)
    {
        // *Shared Trove
        GiveGoldToCreature(oPC, 50000);
        // *Platinum Armor +6
        DestroyObject(GetItemPossessedBy(oPC, "PlatinumArmor4"));
        CreateItemOnObject("Platinumarmor6", oPC, 1);
        SetLocalInt(oToken, "VassalLvl", 5);
    }
    // *Level 2
    else if(nVassal == 2)
    {
        // *Shared Trove
        GiveGoldToCreature(oPC, 20000);
        SetLocalInt(oToken, "VassalLvl", 2);
    }
    // *Level 1
    else if(nVassal == 1)
    {
        // *Give him the Platinum Armor+4
        CreateItemOnObject("Platinumarmor4", oPC, 1);
        SetLocalInt(oToken, "VassalLvl", 1);
    }
}