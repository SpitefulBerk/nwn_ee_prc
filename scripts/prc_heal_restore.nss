/*
    Healer's Greater Restoration
*/

#include "prc_alterations"

void main()
{
    // This is all it does.
    DoRacialSLA(SPELL_GREATER_RESTORATION, GetHitDice(OBJECT_SELF));
}