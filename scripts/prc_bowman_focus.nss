/////////////////////////////////////////////////
// Bowman Focus
//-----------------------------------------------
// Created By: Stratovarius
/////////////////////////////////////////////////

#include "prc_inc_combmove"
#include "prc_inc_clsfunc"

void main()
{
    // The pile of effects that are given to the Bowman when it focuses
    object oPC = OBJECT_SELF;
    int nClass = GetLevelByClass(CLASS_TYPE_BOWMAN, oPC);
    int nDex, nAtk;
    float fDur = RoundsToSeconds(3 + nClass/2);

    if(nClass > 18)
    {
        nDex = 10;
        nAtk = 4;
    }
    else if(nClass > 12)
    {
        nDex = 8;
        nAtk = 3;
    }
    else if(nClass > 8)
    {
        nDex = 6;
        nAtk = 2;
    }
    else
    {
        nDex = 4;
        nAtk = 1;
    }

    effect eVis = EffectVisualEffect(VFX_IMP_IMPROVE_ABILITY_SCORE);
    effect eLink = EffectAbilityIncrease(ABILITY_DEXTERITY, nDex);
           eLink = EffectLinkEffects(eLink, EffectModifyAttacks(nAtk));
           eLink = EffectLinkEffects(eLink, EffectVisualEffect(VFX_DUR_CESSATE_POSITIVE));
           eLink = ExtraordinaryEffect(eLink);

    ApplyEffectToObject(DURATION_TYPE_TEMPORARY, eLink, oPC, fDur);
    ApplyEffectToObject(DURATION_TYPE_INSTANT, eVis, oPC);
    DelayCommand(fDur+0.1, ExecuteScript("prc_bowman", oPC));

    //re-apply Sniper Skill bonuses
    if(nClass > 4)
    {
        int nDexMod = GetAbilityModifier(ABILITY_DEXTERITY, oPC);
        if(nDexMod > 0)
        {
            object oItem = GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oPC);
            string sTest = "prc_bowman";
            int ipDamType = ipBowmanDmgType(oItem);
            if(ipDamType > -1)
            {
                int ipDam = GetIntToDamage(nDexMod);
                int ipOld = GetLocalInt(oItem, sTest);
                if(ipDam != ipOld)
                {
                    RemoveSpecificProperty(oItem, ITEM_PROPERTY_DAMAGE_BONUS, ipDamType, ipOld, 1, "", -1, DURATION_TYPE_TEMPORARY);
                    IPSafeAddItemProperty(oItem, ItemPropertyDamageBonus(ipDamType, ipDam), 99999.0, X2_IP_ADDPROP_POLICY_KEEP_EXISTING, FALSE, FALSE);
                    SetLocalInt(oItem, sTest, ipDam);
                }
            }
        }
    }
}