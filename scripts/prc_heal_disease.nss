/*
    Healer's Remove Disease
*/

#include "prc_alterations"

void main()
{
    // This is all it does.
    DoRacialSLA(SPELL_REMOVE_DISEASE, GetHitDice(OBJECT_SELF));
}