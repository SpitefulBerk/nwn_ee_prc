//::///////////////////////////////////////////////
//:: Slayer of Domiel Detect Evil
//:: prc_sod_dtctevil.nss
//::///////////////////////////////////////////////
/*
    Detect Evil as the spell, at will
*/
//:://////////////////////////////////////////////
//:: Created By: Stratovarius
//:: Created On: 27.2.2006
//:://////////////////////////////////////////////

#include "prc_alterations"

void main()
{
    // That was easy
    int nLevel = GetLevelByClass(CLASS_TYPE_SLAYER_OF_DOMIEL, OBJECT_SELF);
    DoRacialSLA(SPELL_DETECT_EVIL, nLevel);
}