/*
    Healer's Resurrection
*/

#include "prc_alterations"

void main()
{
    // This is all it does.
    DoRacialSLA(SPELL_TRUE_RESURRECTION, GetHitDice(OBJECT_SELF));
}