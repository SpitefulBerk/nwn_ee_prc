//::///////////////////////////////////////////////
//:: Draconic Aura Toggle - Secondary Auras
//:: prc_dracxtra_tg2.nss
//::///////////////////////////////////////////////
/*
    Toggles draconic auras using the Double Draconic
    Aura feat.
*/
//:://////////////////////////////////////////////
//:: Created By: xwarren
//:: Created On: Apr 2, 2011
//:://////////////////////////////////////////////

#include "prc_alterations"
#include "prc_inc_dragsham"

void _SetupAura(object oPC, int nSpellID, int nDamageType)
{
    object oAura = GetAuraObject(oPC, "VFX_DRACONIC_AURA_2");
    int nAuraBonus = GetAuraBonus(oPC);
    if(!nDamageType) nDamageType = GetDragonDamageType(oPC);

    SetLocalObject(oPC, "DraconicAura2", oAura);
    SetLocalInt(oAura, "SpellID", nSpellID);
    SetLocalInt(oAura, "AuraBonus", nAuraBonus);
    SetLocalInt(oAura, "DamageType", nDamageType);
}

void main()
{
    object oPC = OBJECT_SELF;
    object oFirstAura = GetLocalObject(oPC, "DraconicAura1");
    object oSecndAura = GetLocalObject(oPC, "DraconicAura2");
    int nFirstAura = GetLocalInt(oFirstAura, "SpellID");
    int nSecndAura = GetLocalInt(oSecndAura, "SpellID");
    int nSpellID = GetSpellId();
    string sMes = "";

    int nDamageType = GetLocalInt(oPC, "DraconicAuraElement");
    DeleteLocalInt(oPC, "DraconicAuraElement");

    //remove the first aura
    if(GetIsObjectValid(oSecndAura))
    {
        DestroyObject(oSecndAura);
        DeleteLocalObject(oPC, "DraconicAura2");
    }

    //it was the same aura - deactivate
    if(nSpellID == nFirstAura)
    {
        sMes = "*Second Draconic Aura Deactivated*";
    }
    else if(GetIsAuraActive(nFirstAura, nSpellID))
    {
        sMes = "That aura is already active.";
    }
    else if(TakeSwiftAction(oPC))
    {
        sMes = "*Second Draconic Aura Activated*";
        effect eAura = ExtraordinaryEffect(EffectAreaOfEffect(AOE_MOB_DRACONIC_AURA_2));
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, eAura, oPC);
        _SetupAura(oPC, nSpellID, nDamageType);
    }

     FloatingTextStringOnCreature(sMes, oPC, FALSE);
}