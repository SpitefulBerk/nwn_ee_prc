//::///////////////////////////////////////////////
//:: Aura of Fear On Enter
//:: apal_aurafeara.nss
//:://////////////////////////////////////////////
/*
    -4 penalty to saving throws vs fear effects
*/
//:://////////////////////////////////////////////

void main()
{
    //Declare major variables
    object oCaster = GetAreaOfEffectCreator();
    object oTarget = GetEnteringObject();

    effect eVis = EffectVisualEffect(VFX_IMP_FEAR_S);
    effect eDur3 = EffectVisualEffect(VFX_DUR_MIND_AFFECTING_NEGATIVE);
    effect eSave = EffectSavingThrowDecrease(SAVING_THROW_ALL, 4, SAVING_THROW_TYPE_FEAR);
    effect eLink = EffectLinkEffects(eSave, eDur3);

    if(GetIsEnemy(oTarget, oCaster))
    {
        //Fire cast spell at event for the specified target
        SignalEvent(oTarget, EventSpellCastAt(oCaster, SPELLABILITY_AURA_FEAR));
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, SupernaturalEffect(eSave), oTarget);
    }
}
