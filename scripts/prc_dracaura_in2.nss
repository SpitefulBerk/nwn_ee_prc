//::///////////////////////////////////////////////
//:: Draconic Aura Enter - Secondary Auras
//:: prc_dracaura_in2.nss
//::///////////////////////////////////////////////
/*
    Handles PCs entering the Aura AoE for secondary
    draconic auras.
*/
//:://////////////////////////////////////////////
//:: Created By: xwarren
//:: Created On: Apr 2, 2011
//:://////////////////////////////////////////////

#include "prc_alterations"

void main()
{
    object oAura = OBJECT_SELF;
    object oTarget = GetEnteringObject();
    object oShaman = GetAreaOfEffectCreator();

    // Only apply to allies
    if(!GetIsFriend(oTarget, oShaman))
        return;

    int nAuraID     = GetLocalInt(oAura, "SpellID");
    int nAuraBonus  = GetLocalInt(oAura, "AuraBonus");
    int nDamageType = GetLocalInt(oAura, "DamageType");

    effect eBonus = EffectVisualEffect(VFX_DUR_CESSATE_POSITIVE);
    effect eVis   = EffectVisualEffect(VFX_IMP_HEAD_HOLY);

    switch(nAuraID)
    {
        case SPELL_SECOND_AURA_TOUGHNESS:
            eBonus = EffectLinkEffects(eBonus, EffectDamageReduction(nAuraBonus, DAMAGE_POWER_PLUS_TWENTY));
            break;
        case SPELL_SECOND_AURA_VIGOR:
            eBonus = EffectLinkEffects(eBonus, EffectRegenerate(nAuraBonus, 6.0));
            break;
        case SPELL_SECOND_AURA_PRESENCE:
            eBonus = EffectLinkEffects(eBonus, EffectSkillIncrease(SKILL_PERSUADE, nAuraBonus));
            eBonus = EffectLinkEffects(eBonus, EffectSkillIncrease(SKILL_BLUFF, nAuraBonus));
            eBonus = EffectLinkEffects(eBonus, EffectSkillIncrease(SKILL_INTIMIDATE, nAuraBonus));
            break;
        case SPELL_SECOND_AURA_SENSES:
            eBonus = EffectLinkEffects(eBonus, EffectSkillIncrease(SKILL_LISTEN, nAuraBonus));
            eBonus = EffectLinkEffects(eBonus, EffectSkillIncrease(SKILL_SPOT, nAuraBonus));
            break;
        case SPELL_SECOND_AURA_ENERGY_SHIELD:
            eBonus = EffectLinkEffects(eBonus, EffectDamageShield(2 * nAuraBonus, 0, nDamageType));
            break;
        case SPELL_SECOND_AURA_RESISTANCE:
            eBonus = EffectLinkEffects(eBonus, EffectDamageResistance(nDamageType, 5 * nAuraBonus));
            break;
        case SPELL_SECOND_AURA_RESISTACID:
            eBonus = EffectLinkEffects(eBonus, EffectDamageResistance(DAMAGE_TYPE_ACID, 5 * nAuraBonus));
            break;
        case SPELL_SECOND_AURA_RESISTCOLD:
            eBonus = EffectLinkEffects(eBonus, EffectDamageResistance(DAMAGE_TYPE_COLD, 5 * nAuraBonus));
            break;
        case SPELL_SECOND_AURA_RESISTELEC:
            eBonus = EffectLinkEffects(eBonus, EffectDamageResistance(DAMAGE_TYPE_ELECTRICAL, 5 * nAuraBonus));
            break;
        case SPELL_SECOND_AURA_RESISTFIRE:
            eBonus = EffectLinkEffects(eBonus, EffectDamageResistance(DAMAGE_TYPE_FIRE, 5 * nAuraBonus));
            break;
        case SPELL_SECOND_AURA_POWER:
            eBonus = EffectLinkEffects(eBonus, EffectDamageIncrease(nAuraBonus, DAMAGE_TYPE_MAGICAL));
            break;
        case SPELL_SECOND_AURA_INSIGHT:
            eBonus = EffectLinkEffects(eBonus, EffectSkillIncrease(SKILL_LORE, nAuraBonus));
            eBonus = EffectLinkEffects(eBonus, EffectSkillIncrease(SKILL_SPELLCRAFT, nAuraBonus));
            break;
        case SPELL_SECOND_AURA_RESOLVE:
            eBonus = EffectLinkEffects(eBonus, EffectSavingThrowIncrease(SAVING_THROW_WILL, nAuraBonus, SAVING_THROW_TYPE_FEAR));
            eBonus = EffectLinkEffects(eBonus, EffectSkillIncrease(SKILL_CONCENTRATION, nAuraBonus));
            break;
        case SPELL_SECOND_AURA_STAMINA:
            eBonus = EffectLinkEffects(eBonus, EffectSavingThrowIncrease(SAVING_THROW_FORT, nAuraBonus));
            break;
        case SPELL_SECOND_AURA_SWIFTNESS:
            eBonus = EffectLinkEffects(eBonus, EffectMovementSpeedIncrease(nAuraBonus * 5));
            eBonus = EffectLinkEffects(eBonus, EffectSkillIncrease(SKILL_JUMP, nAuraBonus));
            break;
        case SPELL_SECOND_AURA_MAGICPOWER:
            if(nAuraBonus > GetLocalInt(oTarget, "MagicPowerAura"))
                SetLocalInt(oTarget, "MagicPowerAura", nAuraBonus);
            break;
        case SPELL_SECOND_AURA_ENERGY:
            if(nDamageType == DAMAGE_TYPE_ACID && nAuraBonus > GetLocalInt(oTarget, "AcidEnergyAura"))
                SetLocalInt(oTarget, "AcidEnergyAura", nAuraBonus);
            else if(nDamageType == DAMAGE_TYPE_COLD && nAuraBonus > GetLocalInt(oTarget, "ColdEnergyAura"))
                SetLocalInt(oTarget, "ColdEnergyAura", nAuraBonus);
            else if(nDamageType == DAMAGE_TYPE_ELECTRICAL && nAuraBonus > GetLocalInt(oTarget, "ElecEnergyAura"))
                SetLocalInt(oTarget, "ElecEnergyAura", nAuraBonus);
            else if(nDamageType == DAMAGE_TYPE_FIRE && nAuraBonus > GetLocalInt(oTarget, "FireEnergyAura"))
                SetLocalInt(oTarget, "FireEnergyAura", nAuraBonus);
            break;
        case SPELL_SECOND_AURA_ENERGYACID:
            if(nAuraBonus > GetLocalInt(oTarget, "AcidEnergyAura"))
                SetLocalInt(oTarget, "AcidEnergyAura", nAuraBonus);
            break;
        case SPELL_SECOND_AURA_ENERGYCOLD:
            if(nAuraBonus > GetLocalInt(oTarget, "ColdEnergyAura"))
                SetLocalInt(oTarget, "ColdEnergyAura", nAuraBonus);
            break;
        case SPELL_SECOND_AURA_ENERGYELEC:
            if(nAuraBonus > GetLocalInt(oTarget, "ElecEnergyAura"))
                SetLocalInt(oTarget, "ElecEnergyAura", nAuraBonus);
            break;
        case SPELL_SECOND_AURA_ENERGYFIRE:
            if(nAuraBonus > GetLocalInt(oTarget, "FireEnergyAura"))
                SetLocalInt(oTarget, "FireEnergyAura", nAuraBonus);
            break;
    }

    eBonus = ExtraordinaryEffect(eBonus);
    ApplyEffectToObject(DURATION_TYPE_PERMANENT, eBonus, oTarget);
    ApplyEffectToObject(DURATION_TYPE_INSTANT, eVis, oTarget);
}