/*
    Healer's Stone to Flesh
*/

#include "prc_alterations"

void main()
{
    // This is all it does.
    DoRacialSLA(SPELL_STONE_TO_FLESH, GetHitDice(OBJECT_SELF));
}