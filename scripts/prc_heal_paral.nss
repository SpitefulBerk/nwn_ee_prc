/*
    Healer's Remove Paralysis
*/

#include "prc_alterations"

void main()
{
    // This is all it does.
    DoRacialSLA(SPELL_REMOVE_PARALYSIS, GetHitDice(OBJECT_SELF));
}