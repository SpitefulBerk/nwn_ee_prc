//::///////////////////////////////////////////////
//:: Summon Anti-paladin Mount
//:: apal_mount.nss
//:: Copyright (c) 2008 Bioware Corp.
//:://////////////////////////////////////////////
/*
     This script handles the summoning of the anti-paladin mount.
*/
//:://////////////////////////////////////////////
//:: Created By: Deva B. Winblood
//:: Created On: 2007-18-12
//:: Last Update: March 29th, 2008
//:://////////////////////////////////////////////

/*
    On the module object set X3_HORSE_PALADIN_USE_PHB to 1 as an integer
    variable if you want the duration to match that found in the 3.5 edition
    version of the Player's Handbook.

*/

#include "x3_inc_horse"
#include "prc_inc_template"

object HorseSummonAntiPaladinMount(int bPHBDuration=FALSE);

void main()
{
    object oPC=OBJECT_SELF;
    object oMount;
    int bPHBDuration = GetLocalInt(GetModule(),"X3_HORSE_PALADIN_USE_PHB");
    int bNoMounts=FALSE;
    string sSummonScript;
    object oAreaTarget=GetArea(oPC); // used for mount restriction checking

    if (!GetLocalInt(oAreaTarget,"X3_MOUNT_OK_EXCEPTION"))
    { // check for global restrictions
        if (GetLocalInt(GetModule(),"X3_MOUNTS_EXTERNAL_ONLY")&&GetIsAreaInterior(oAreaTarget)) bNoMounts=TRUE;
        else if (GetLocalInt(GetModule(),"X3_MOUNTS_NO_UNDERGROUND")&&!GetIsAreaAboveGround(oAreaTarget)) bNoMounts=TRUE;
    } // check for global restrictions

    if (GetLocalInt(GetArea(oPC),"X3_NO_HORSES")||bNoMounts)
    { // no horses allowed in the area
        DelayCommand(1.0,IncrementRemainingFeatUses(oPC,FEAT_APAL_MOUNT));
        FloatingTextStrRefOnCreature(111986,oPC,FALSE);
        return;
    } // no horses allowed in the area

    if (GetSpellId()==SPELL_APAL_MOUNT)
    { // Paladin Mount Summon
        oMount=HorseGetPaladinMount(oPC);
        if (!GetIsObjectValid(oMount)) oMount=GetLocalObject(oPC,"oX3PaladinMount");
        if (GetIsObjectValid(oMount))
        { // mount already exists
            if (GetIsPC(oPC))
            { // send messages
                if (oMount==oPC) FloatingTextStrRefOnCreature(111987,oPC,FALSE);
                else { FloatingTextStrRefOnCreature(111988,oPC,FALSE); }
            } // send messages
            DelayCommand(1.0,IncrementRemainingFeatUses(oPC,FEAT_APAL_MOUNT));
            return;
        } // mount already exists
        sSummonScript=GetLocalString(GetModule(),"X3_PALMOUNT_SUMMONOVR");
        if (GetStringLength(GetLocalString(oPC,"X3_PALMOUNT_SUMMONOVR"))>0) sSummonScript=GetLocalString(oPC,"X3_PALMOUNT_SUMMONOVR");
        if (GetStringLength(sSummonScript)<1)
        { // no summon paladin mount override
            oMount=HorseSummonAntiPaladinMount(bPHBDuration);
        } // no summon paladin mount override
        else
        { // execute summon script
            ExecuteScript(sSummonScript,oPC);
        } // execute summon script
    } // Paladin Mount Summon
}


object HorseSummonAntiPaladinMount(int bPHBDuration = FALSE)
{ // PURPOSE: Summon Paladin Mount
    object oSummoner=OBJECT_SELF;
    object oMount;
    location lLoc;
    int nLevel=GetLevelByClass(CLASS_TYPE_ANTI_PALADIN,oSummoner);
    int nDespawnTime;
    int nCurrentTime;
    int nMountNum=11;
    string sResRef=HORSE_PALADIN_PREFIX;
    effect eVFX;
    oMount=HorseGetPaladinMount(oSummoner);
    if (!GetIsObjectValid(oMount)&&nLevel>4&&GetObjectType(oSummoner)==OBJECT_TYPE_CREATURE)
    { // okay to summon - only one paladin mount at a time
        if ((GetIsPC(oSummoner)||GetIsDM(oSummoner))&&!GetHasFeat(FEAT_HORSE_MENU,oSummoner)) HorseAddHorseMenu(oSummoner);
        if (nLevel>7&&nLevel<11) nMountNum=12;
        else if (nLevel>10&&nLevel<15) nMountNum=13;
        else if (nLevel>14&&nLevel<25) nMountNum=14;
        else if (nLevel>24&&nLevel<30) nMountNum=15;
        else if (nLevel>29&&nLevel<35) nMountNum=16;
        else if (nLevel>34&&nLevel<40) nMountNum=17;
        else if (nLevel>39) nMountNum=18;
        lLoc=HORSE_SupportGetMountLocation(oSummoner,oSummoner);
        oMount=HorseCreateHorse(sResRef+IntToString(nMountNum),lLoc,oSummoner);
        if (!GetIsObjectValid(oMount)) oMount=HorseCreateHorse(sResRef+IntToString(nMountNum),GetLocation(oSummoner),oSummoner);
        if (GetIsObjectValid(oMount))
        { // oMount created
            eVFX=EffectVisualEffect(VFX_DUR_CUTSCENE_INVISIBILITY);
            ApplyEffectToObject(DURATION_TYPE_TEMPORARY,eVFX,oMount,3.0);
            ApplyTemplateToObject(TEMPLATE_FIENDISH, oMount);
            eVFX=EffectVisualEffect(VFX_FNF_SUMMON_MONSTER_2);
            if (nMountNum>3) eVFX=EffectVisualEffect(VFX_FNF_SUMMON_MONSTER_3);
            ApplyEffectAtLocation(DURATION_TYPE_INSTANT,eVFX,GetLocation(oMount));
            if (bPHBDuration)
            { // Players Handbook 3.5 edition durations
                nCurrentTime=HORSE_SupportAbsoluteMinute();
                nDespawnTime=(2*nLevel*60)+nCurrentTime;
                SetLocalInt(oSummoner,"nX3_PALADIN_UNSUMMON",nDespawnTime);
            } // Players Handbook 3.5 edition durations
            else
            { // 24 hour - popular bioware
                nCurrentTime=HORSE_SupportAbsoluteMinute();
                nDespawnTime=nCurrentTime+(60*24);
                SetLocalInt(oSummoner,"nX3_PALADIN_UNSUMMON",nDespawnTime);
            } // 24 hour - popular bioware
            if (GetLocalInt(GetModule(),"X3_ENABLE_MOUNT_DB")&&GetIsPC(oSummoner)) SetLocalInt(oSummoner,"bX3_STORE_MOUNT_INFO",TRUE);
            SetLocalObject(oSummoner,"oX3PaladinMount",oMount);
        } // oMount created
    } // okay to summon - only one paladin mount at a time
    else { oMount=OBJECT_INVALID; }
    return oMount;
}
