/*
    Healer's Regenerate
*/

#include "prc_alterations"

void main()
{
    // This is all it does.
    DoRacialSLA(SPELL_REGENERATE, GetHitDice(OBJECT_SELF));
}