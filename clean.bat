@echo off
del /q epicspellobjs\*.*
del /q objs\*.*
del /q raceobjs\*.*
del /q spellobjs\*.*
del /q psionicsobjs\*.*
del /q newspellbookobjs\*.*
del /q ocfixerfobjs\*.*
del /q *.temp
del /q CompiledResources\*.hak
del /q CompiledResources\*.erf
del /q xml_temp\*.*
