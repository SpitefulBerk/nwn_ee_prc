/*
    generic powercalling function
*/
#include "psi_inc_psifunc"

void main()
{
    UsePower(GetPowerFromSpellID(PRCGetSpellId()), CLASS_TYPE_PSION);
}