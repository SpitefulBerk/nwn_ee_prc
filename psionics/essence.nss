void main()
{
    SpeakString("Here lies your very essence, held where only you can see. This, buried in the deepest recesses of your mind, holds everything that you are.");
}
