/*:://////////////////////////////////////////////
//:: Spell Name Feather Fall
//:: Spell FileName PHS_S_FeatherFal
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Transmutation
    Level: Brd 1, Sor/Wiz 1
    Components: V
    Casting Time: 1 free action
    Range: Close (8M)
    Targets: One Medium or smaller freefalling object or creature/level within a 3.33-M radius sphere
    Duration: Until landing or 1 round/level
    Saving Throw: Will negates (harmless) or Will negates (object)
    Spell Resistance: Yes (object)

    The affected creatures or objects fall slowly. Feather fall instantly changes
    the rate at which the targets fall to a mere 20 meters per round (equivalent
    to the end of a fall from a few feet), and the subjects take no damage upon
    landing while the spell is in effect. However, when the spell duration expires,
    a normal rate of falling resumes.

    More description...
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Duh, where is there to fall in NWN?!

    Ok, it can maybe be used to fall, I don't know! Needs sorting.

    Probably DM only, placeholder script.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
