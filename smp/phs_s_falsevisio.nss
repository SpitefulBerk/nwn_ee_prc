/*:://////////////////////////////////////////////
//:: Spell Name False Vision
//:: Spell FileName PHS_S_FalseVisio
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Illusion (Glamer)
    Level: Brd 5, Sor/Wiz 5, Trickery 5
    Components: V, S, M
    Casting Time: 1 standard action
    Range: Touch
    Area: 13.33-M.-radius emanation
    Duration: 1 hour/level (D)
    Saving Throw: None
    Spell Resistance: No

    Any divination (scrying) spell used to view anything within the area of
    this spell instead receives a false image (as the major image spell), as
    defined by you at the time of casting. As long as the duration lasts, you
    can concentrate to change the image as desired. While you aren�t
    concentrating, the image remains static.

    Arcane Material Component: The ground dust of a piece of jade worth at least
    250 gp, which is sprinkled into the air when the spell is cast.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Need scrying spells first, that is, if this can even be used.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
