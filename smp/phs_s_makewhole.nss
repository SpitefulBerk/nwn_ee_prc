/*:://////////////////////////////////////////////
//:: Spell Name Make Whole
//:: Spell FileName PHS_S_MakeWhole
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Transmutation
    Level: Clr 2
    Casting Time: 1 standard action
    Range: Close (8M)
    Target: One object of up to 10 cu. ft./ level

    This spell functions like mending, except that make whole completely repairs
    an object made of any substance, even one with multiple breaks, to be as
    strong as new. The spell does not restore the magical abilities of a broken
    magic item made whole, and it cannot mend broken magic rods, staffs, or
    wands. The spell does not repair items that have been warped, burned,
    disintegrated, ground to powder, melted, or vaporized, nor does it affect
    creatures (including constructs).
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Needs mending, and might not be possible.

    Placeholder script.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
