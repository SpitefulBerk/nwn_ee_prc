/*:://////////////////////////////////////////////
//:: Spell Name Elemental Swarm
//:: Spell FileName PHS_S_ElementalS
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Conjuration (Summoning) [see text]
    Level: Air 9, Drd 9, Earth 9, Fire 9, Water 9
    Components: V, S
    Casting Time: 10 minutes
    Range: Medium (20M)
    Effect: Two or more summoned creatures
    Duration: 10 min./level (D)
    Saving Throw: None
    Spell Resistance: No

    This spell opens a portal to an Elemental Plane and summons elementals from
    it. A druid can choose the plane (Air, Earth, Fire, or Water); a cleric
    opens a portal to the plane matching his domain.

    When the spell is complete, 2d4 Large elementals appear. Ten minutes later,
    1d4 Huge elementals appear. Ten minutes after that, one greater elemental
    appears. Each elemental has maximum hit points per HD. Once these creatures
    appear, they serve you for the duration of the spell.

    The elementals obey you explicitly and never attack you, even if someone
    else manages to gain control over them. You do not need to concentrate to
    maintain control over the elementals. You can dismiss them singly or in
    groups at any time.

    When you use a summoning spell to summon an air, earth, fire, or water
    creature, it is a spell of that type.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Will work fine once the summoning spells are done.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
