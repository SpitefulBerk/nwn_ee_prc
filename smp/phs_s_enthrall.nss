/*:://////////////////////////////////////////////
//:: Spell Name Enthrall
//:: Spell FileName PHS_S_Enthrall
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Enchantment (Charm) [Language Dependent, Mind-Affecting, Sonic]
    Level: Brd 2, Clr 2
    Components: V, S
    Casting Time: 1 round
    Range: Medium (20M)
    Targets: Any number of creatures
    Duration: 1 hour or less
    Saving Throw: Will negates; see text
    Spell Resistance: Yes

    If you have the attention of a group of creatures, you can use this spell to
    hold them spellbound. To cast the spell, you must speak or sing without
    interruption for 1 full round. Thereafter, those affected give you their
    undivided attention, ignoring their surroundings. They are considered to
    have an attitude of friendly while under the effect of the spell. Any
    potentially affected creature of a race other then yours gets a +4 bonus on
    the saving throw.

    A creature with 4 or more HD or with a Wisdom score of 16 or higher remains
    aware of its surroundings and if attacked or see allies being attack will
    snap out of Enthrallment.

    The effect lasts as long as you speak or sing, to a maximum of 1 hour. Thusly,
    if you become silenced the spell stops as if interrupted. Those enthralled by
    your words take no action while you speak or sing and for 1d3 rounds thereafter
    while they discuss the topic or performance. Those entering the area during
    the performance must also successfully save or become enthralled. The speech
    ends (but the 1d3-round delay still applies) if you lose concentration or do
    anything other than speak or sing.

    If those not enthralled have unfriendly or hostile attitudes toward you,
    they can collectively make a Charisma check to try to end the spell by
    jeering and heckling. For this check, use the Charisma bonus of the creature
    with the highest Charisma in the group; others may make Charisma checks to
    assist. The heckling ends the spell if this check result beats your Charisma
    check result. Only one such challenge is allowed per use of the spell.

    If any member of the audience is attacked or subjected to some other overtly
    hostile act, the spell ends and the previously enthralled members become
    immediately unfriendly toward you. Each creature with 4 or more HD or with
    a Wisdom score of 16 or higher becomes hostile.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    This might be a harder one, maybe use confusion or similar. Rating: 15+!,
    but possible.

    Placeholder script.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
