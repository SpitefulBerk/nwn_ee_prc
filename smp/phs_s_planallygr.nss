/*:://////////////////////////////////////////////
//:: Spell Name Planar Ally, Greater
//:: Spell FileName PHS_S_PlanAllyGr
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Conjuration (Calling) [see text for lesser planar ally]
    Level: Clr 8
    Effect: Up to three called elementals or outsiders, totaling no more than 18
    HD, no two of which can be more than 30 ft. apart when they appear.

    This spell functions like lesser planar ally, except that you may call a
    single creature of 18 HD or less, or up to three creatures of the same kind
    whose Hit Dice total no more than 18. The creatures agree to help you and
    request your return payment together.

    XP Cost: 500 XP.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Do lesser first
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
