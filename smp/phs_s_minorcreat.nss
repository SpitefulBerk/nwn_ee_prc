/*:://////////////////////////////////////////////
//:: Spell Name Minor Creation
//:: Spell FileName PHS_S_MinorCreat
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Conjuration (Creation)
    Level: Sor/Wiz 4
    Components: V, S, M
    Casting Time: 1 minute
    Range: 0 ft.
    Effect: Unattended, nonmagical object of nonliving plant matter,
               up to 1 cu. ft./level
    Duration: 1 hour/level (D)
    Saving Throw: None
    Spell Resistance: No

    You create a nonmagical, unattended object of nonliving, vegetable matter.
    The volume of the item created cannot exceed 1 cubic foot per caster level.
    You must succeed on an appropriate skill check to make a complex item.

    Attempting to use any created object as a material component causes the spell
    to fail.

    Material Component: A tiny piece of matter of the same sort of item you plan
    to create with minor creation.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Like Major Creation, it is hard, and might not be added (except by a DM)

    Placeholder script.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
