/*:://////////////////////////////////////////////
//:: Spell Name Command, Greater
//:: Spell FileName PHS_S_CommandGre
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Enchantment (Compulsion) [Language-Dependent, Mind-Affecting]
    Level: Clr 5
    Targets: One creature/level, no two of which can be more than 30 ft. apart
    Duration: 1 round/level

    This spell functions like command, except that up to one creature per level
    may be affected, and the activities continue beyond 1 round. At the start of
    each commanded creature�s action after the first, it gets another Will save
    to attempt to break free from the spell. Each creature must receive the same
    command.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Will maybe be confusion..I don't know.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
