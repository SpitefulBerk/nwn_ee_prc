/*:://////////////////////////////////////////////
//:: Spell Name Polymorph Any Object
//:: Spell FileName PHS_S_PolyAnyObj
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Transmutation
    Level: Sor/Wiz 8, Trickery 8
    Components: V, S, M/DF
    Casting Time: 1 standard action
    Range: Close (8M)
    Target: One creature, or one nonmagical object of up to 100 cu. ft./level
    Duration: See text
    Saving Throw: Fortitude negates (object); see text
    Spell Resistance: Yes (object)

    This spell functions like polymorph, except that it changes one object or
    creature into another. The duration of the spell depends on how radical a
    change is made from the original state to its enchanted state. The duration
    is determined by using the following guidelines.

    Changed Subject Is:                             Increase to Duration Facto (1)
    Same kingdom (animal, vegetable, mineral)                   +5
    Same class (mammals, fungi, metals, etc.)                   +2
    Same size                                                   +2
    Related (twig is to tree, wolf fur is to wolf, etc.)        +2
    Same or lower Intelligence                                  +2

    (1) Add all that apply. Look up the total on the next table.

    Duration Factor Duraton     Example
    0               20 minutes  Pebble to human
    2               1 hour      Marionette to human
    4               3 hours     Human to marionette
    5               12 hours    Lizard to manticore
    6               2 days      Sheep to wool coat
    7               1 week      Shrew to manticore
    9+              Permanent   Manticore to shrew

    Unlike polymorph, polymorph any object does grant the creature the
    Intelligence score of its new form. If the original form didn�t have a
    Wisdom or Charisma score, it gains those scores as appropriate for the new
    form.

    Damage taken by the new form can result in the injury or death of the
    polymorphed creature. In general, damage occurs when the new form is changed
    through physical force.

    A nonmagical object cannot be made into a magic item with this spell.
    Magic items aren�t affected by this spell.

    This spell cannot create material of great intrinsic value, such as copper,
    silver, gems, silk, gold, platinum, mithral, or adamantine. It also cannot
    reproduce the special properties of cold iron in order to overcome the
    damage reduction of certain creatures.

    This spell can also be used to duplicate the effects of baleful polymorph,
    polymorph, flesh to stone, stone to flesh, transmute mud to rock, transmute
    metal to wood, or transmute rock to mud.

    Arcane Material Component: Mercury, gum arabic, and smoke.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    The "doing flesh to stone" things are easy enough. This could be basically
    turned into a more powerful Baleful Polymorph spell...it can be used that
    way anyway.

    Ok, so it can transform things into other things. Hell, make this have
    just 5 options maybe:

    - Turn a object into an inaimate placable object (duration of 20 minutes
    to 3 hours depending on size?)
    - Turn something into a really tiny creature (much longer duration?)
    - Animate a item into a creature for a while
    -
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
