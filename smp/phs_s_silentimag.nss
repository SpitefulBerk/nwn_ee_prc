/*:://////////////////////////////////////////////
//:: Spell Name Silent Image
//:: Spell FileName PHS_S_SilentImag
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Illusion (Figment)
    Level: Brd 1, Sor/Wiz 1
    Components: V, S, F
    Casting Time: 1 standard action
    Range: Long (40M)
    Effect: Visual figment that cannot extend beyond four 10-ft. cubes + one 10-ft. cube/level (S)
    Duration: Concentration
    Saving Throw: Will disbelief (if interacted with)
    Spell Resistance: No

    This spell creates the visual illusion of an object, creature, or force, as
    visualized by you. The illusion does not create sound, smell, texture, or
    temperature. You can move the image within the limits of the size of the effect.

    Focus: A bit of fleece.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Other ones similar to do this done first, or even wait until I look at the
    others and do this one first.

    Placeholder script.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}

