/*:://////////////////////////////////////////////
//:: Spell Name Arcane Eye
//:: Spell FileName PHS_S_ArcaneEye
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Divination (Scrying)
    Level: Sor/Wiz 4
    Components: V, S, M
    Casting Time: 10 minutes
    Range: Unlimited
    Effect: Magical sensor
    Duration: 1 min./level (D)
    Saving Throw: None
    Spell Resistance: No

    Description.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Rating: 9: Requires cutseens to see what the eye sees, but it is
    possible to do I guess.

    Placeholder script.

    As in:
    - Create a new object (and cutscene dominate or whatever it), which is the eye.
    - MAke it very easy to kill
    - "possess" it via. using a power or something
    - Move the caster (and make a copy of the caster) and turn them into the
      eye.
    - If they (or thier body) is attacked, end the spell.

    Note: Note in description the eye isn't too small, and will trigger traps
    and if attacked will go away.

    Note 2: Make sure that the PC knows that any invisiblity effects they have will go
    away when they use the spell to look at something (the spell makes the
    creature cutscene invisible to mean we don't see lots of visuals, but it
    still can be attacked by things. Or maybe make it so it can't?)

    Maybe make the PC not cutscene invisible, but actually make a quick eye
    for this, which yes, will have all effects (but will never be invisible etc.)
    visible, but thats more a hinderence then anything.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
