/*:://////////////////////////////////////////////
//:: Spell Name Hold Portal
//:: Spell FileName PHS_S_HoldPortal
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Abjuration
    Level: Sor/Wiz 1
    Component: V
    Casting Time: 1 standard action
    Range: Medium (20M)
    Target: One portal, up to 6.67 sq. M./level
    Duration: 1 min./level (D)
    Saving Throw: None
    Spell Resistance: No

    This spell magically holds shut a door, gate, window, or shutter of wood,
    metal, or stone. The magic affects the portal just as if it were securely
    closed and normally locked. A knock spell or a successful dispel magic spell
    can negate a hold portal spell.

    For a portal affected by this spell, add 5 to the normal DC for forcing open
    the portal.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Need to edit door scripts probably.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
