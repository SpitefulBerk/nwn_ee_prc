/*:://////////////////////////////////////////////
//:: Spell Name Phantom Steed
//:: Spell FileName PHS_S_PhantomSte
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Conjuration (Creation)
    Level: Brd 3, Sor/Wiz 3
    Components: V, S
    Casting Time: 10 minutes
    Range: 0 meters.
    Effect: One quasi-real, horselike creature
    Duration: 1 hour/level (D)
    Saving Throw: None
    Spell Resistance: No

    You conjure a Large, quasi-real, horselike creature. The steed can be
    ridden only by you or by the one person for whom you specifically created
    the mount. A phantom steed has a black head and body, gray mane and tail,
    and smoke-colored, insubstantial hooves that make no sound. It has what
    seems to be a saddle, bit, and bridle. It does not fight, but animals shun
    it and refuse to attack it.

    The mount has an AC of 18 (-1 size, +4 natural armor, +5 Dex) and 7 hit
    points +1 hit point per caster level. If it loses all its hit points, the
    phantom steed disappears. A phantom steed has a speed of 20 feet per caster
    level, to a maximum of 240 feet. It can bear its rider�s weight plus up to
    10 pounds per caster level.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    No horses, duh. Placeholder only, too. Might re-add the powers that were
    assigned (although some would be pointless) and in NwN, this would be
    hardly better then the other horse spell.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
