/*:://////////////////////////////////////////////
//:: Spell Name Magic Jar
//:: Spell FileName PHS_S_MagicJar
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Necromancy
    Level: Sor/Wiz 5
    Components: V, S, F
    Casting Time: 1 standard action
    Range: Medium (20M)
    Target: One creature
    Duration: 1 hour/level or until you return to your body
    Saving Throw: Will negates; see text
    Spell Resistance: Yes

    By casting magic jar, you place your soul in a gem or large crystal (known
    as the magic jar), leaving your body lifeless. Then you can attempt to take
    control of a nearby body, forcing its soul into the magic jar. You may move
    back to the jar (thereby returning the trapped soul to its body) and attempt
    to possess another body. The spell ends when you send your soul back to your
    own body, leaving the receptacle empty.

    More description.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Rating: Traps or something, I don't know! Rating: HARD

    Placeholder script.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
