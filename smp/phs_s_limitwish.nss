/*:://////////////////////////////////////////////
//:: Spell Name Limited Wish
//:: Spell FileName PHS_S_LimitWish
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Universal
    Level: Sor/Wiz 7
    Components: V, S, XP
    Casting Time: 1 standard action
    Range: See text
    Target, Effect, or Area: See text
    Duration: See text
    Saving Throw: None; see text
    Spell Resistance: Yes

    A limited wish lets you create nearly any type of effect. For example, a limited wish can do any of the following things.

     Duplicate any sorcerer/wizard spell of 6th level or lower, provided the
      spell is not of a school prohibited to you.
     Duplicate any other spell of 5th level or lower, provided the spell is not
      of a school prohibited to you.
     Duplicate any sorcerer/wizard spell of 5th level or lower, even if its of
      a prohibited school.
     Duplicate any other spell of 4th level or lower, even if its of a
      prohibited school.
     Undo the harmful effects of many spells, such as geas/quest or insanity.

     Produce any other effect whose power level is in line with the above
      effects, such as a single creature automatically hitting on its next
      attack or taking a -7 penalty on its next saving throw.

    A duplicated spell allows saving throws and spell resistance as normal (but
    the save DC is for a 7th-level spell). When a limited wish duplicates a
    spell that has an XP cost, you must pay that cost or 300 XP, whichever is
    more. When a limited wish spell duplicates a spell with a material component
    that costs more than 1,000 gp, you must provide that component.

    XP Cost: 300 XP or more (see above).
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Need to, well, script this harshly - it'd take a while.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
