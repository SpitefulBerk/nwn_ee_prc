/*:://////////////////////////////////////////////
//:: Spell Name Jump
//:: Spell FileName PHS_S_Jump
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Transmutation
    Level: Drd 1, Rgr 1, Sor/Wiz 1
    Components: V, S, M
    Casting Time: 1 standard action
    Range: Touch
    Target: Creature touched
    Duration: 1 min./level (D)
    Saving Throw: Will negates (harmless)
    Spell Resistance: Yes

    The subject gets a +10 enhancement bonus on Jump checks. The enhancement
    bonus increases to +20 at caster level 5th, and to +30 (the maximum) at
    caster level 9th.

    Material Component: A grasshopper�s hind leg, which you break when the spell
    is cast.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    No jump skill. Rating: -1
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
