/*:://////////////////////////////////////////////
//:: Spell Name Wall of Fire
//:: Spell FileName PHS_S_WallofFire
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Evocation [Fire]
    Level: Drd 5, Fire 4, Sor/Wiz 4
    Components: V, S, M/DF
    Casting Time: 1 standard action
    Range: Medium (20M)
    Effect: Opaque sheet of flame 5M.long/4 levels or a ring of fire, 3M-donut,
            5M-radius.
    Duration: Concentration + 1 round/level
    Saving Throw: Reflex half
    Spell Resistance: Yes

    An immobile, blazing curtain of shimmering violet fire springs into
    existence. The wall can be a 3M-wide donut, or up to 20M long, and 2M wide.

    The wall of fire will damage creatures, dealing 2d6 points of fire damage +1
    point of fire damage per caster level (maximum +20) to any creature inside
    it. The wall deals double damage to undead creatures.

    If a cold damaging spell is aimed over the spot the Wall of Fire was
    envoked, it will usually make the flames go out.

    Arcane Material Component: A small piece of phosphorus.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    As above.

    The choice of what to use will be set on the user, probably. SR
    applies, and there is a reflex save.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
