/*:://////////////////////////////////////////////
//:: Spell Name Guards and Wards
//:: Spell FileName PHS_S_GuardsandW
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Abjuration
    Level: Sor/Wiz 6
    Components: V, S, M, F
    Casting Time: 30 minutes
    Range: Anywhere within the area to be warded
    Area: Up to 200 sq. ft./level (S)
    Duration: 2 hours/level (D)
    Saving Throw: See text
    Spell Resistance: See text

    This powerful spell is primarily used to defend your stronghold. The ward
    protects 200 square feet per caster level. The warded area can be as much
    as 20 feet high, and shaped as you desire. You can ward several stories of
    a stronghold by dividing the area among them; you must be somewhere within
    the area to be warded to cast the spell. The spell creates the following
    magical effects within the warded area.

    More description.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Rating: 10: and it requires symbol work.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
