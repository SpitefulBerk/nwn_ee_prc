/*:://////////////////////////////////////////////
//:: Spell Name Discern Location
//:: Spell FileName PHS_S_DiscernLoc
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Divination
    Level: Clr 8, Knowledge 8, Sor/Wiz 8
    Components: V, S, DF
    Casting Time: 10 minutes
    Range: Unlimited
    Target: One creature or object
    Duration: Instantaneous
    Saving Throw: None
    Spell Resistance: No

    A discern location spell is among the most powerful means of locating
    creatures or objects. Nothing short of a mind blank spell or the direct
    intervention of a deity keeps you from learning the exact location of a
    single individual or object. Discern location circumvents normal means of
    protection from scrying or location. The spell reveals the name of the
    creature or object�s location (place, name, business name, building name, or
    the like), community, county (or similar political division), country,
    continent, and the plane of existence where the target lies.

    To find a creature with the spell, you must have seen the creature or have
    some item that once belonged to it. To find an object, you must have touched
    it at least once.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Not sure how to do this one. Rating 10.

    Maybe we just can find PC characters, via. giving the area name of them.

    It'd be simple and effective.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
