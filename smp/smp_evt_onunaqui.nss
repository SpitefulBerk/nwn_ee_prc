/*:://////////////////////////////////////////////
//:: Name On Unaquire Item default PHS event
//:: FileName SMP_EVT_OnUnaqui
//:://////////////////////////////////////////////
//:: Notes
//:://////////////////////////////////////////////
    This will remove the effects of these spells from any items dropped. Note
    that of course this will not affect NPC's - they do not drop things.

    The same spells will be removed On Item Aquire too - to stop bartering, or
    NPC's casting it and then a PC using henchmen inventory to take it.


//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "SMP_INC_SPELLS"

void main()
{

}
