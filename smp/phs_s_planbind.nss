/*:://////////////////////////////////////////////
//:: Spell Name Planar Binding
//:: Spell FileName PHS_S_PlanBind
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Conjuration (Calling) [see text for lesser planar binding]
    Level: Sor/Wiz 6
    Components: V, S
    Targets: Up to three elementals or outsiders, totaling no more than 12 HD, no two of which can be more than 30 ft. apart when they appear

    This spell functions like lesser planar binding, except that you may call a
    single creature of 12 HD or less, or up to three creatures of the same kind
    whose Hit Dice total no more than 12. Each creature gets a save, makes an
    independent attempt to escape, and must be individually persuaded to aid you.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Lesser needed first.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
