/*:://////////////////////////////////////////////
//:: Spell Name Permanent Image
//:: Spell FileName PHS_S_PermanentI
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Illusion (Figment)
    Level: Brd 6, Sor/Wiz 6
    Effect: Figment that cannot extend beyond a 20-ft. cube + one 10-ft. cube/
            level (S)
    Duration: Permanent (D)

    This spell functions like silent image, except that the figment includes
    visual, auditory, olfactory, and thermal elements, and the spell is
    permanent. By concentrating, you can move the image within the limits of
    the range, but it is static while you are not concentrating.

    Material Component: A bit of fleece plus powdered jade worth 100 gp.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Need silent image.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
