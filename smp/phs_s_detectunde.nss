/*:://////////////////////////////////////////////
//:: Spell Name Detect Undead
//:: Spell FileName PHS_S_DetectUnde
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Divination
    Level: Clr 1, Pal 1, Sor/Wiz 1
    Components: V, S, M/DF
    Casting Time: 1 standard action
    Range: 20M.
    Area: Cone-shaped emanation
    Duration: Concentration, up to 1 minute/ level (D)
    Saving Throw: None
    Spell Resistance: No

    You can detect the aura that surrounds undead creatures. The amount of
    information revealed depends on how long you study a particular area.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Not added yet, requires concentration as all detect spells.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
