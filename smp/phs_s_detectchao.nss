/*:://////////////////////////////////////////////
//:: Spell Name Detect Chaos
//:: Spell FileName PHS_S_DetectChao
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Detect Chaos
    Divination
    Level: Clr 1

    This spell functions like detect evil, except that it detects the auras of
    chaotic creatures, clerics of chaotic deities, chaotic spells, and chaotic
    magic items, and you are vulnerable to an overwhelming chaotic aura if you
    are lawful.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Need detect evil first.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}

