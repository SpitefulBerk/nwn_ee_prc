/*:://////////////////////////////////////////////
//:: Spell Name Glyph of Warding
//:: Spell FileName PHS_S_GlyphWard
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Abjuration
    Level: Clr 3
    Components: V, S, M
    Casting Time: 10 minutes
    Range: Touch
    Target or Area: Object touched or up to 1.67 sq. M./level
    Duration: Permanent until discharged (D)
    Saving Throw: See text
    Spell Resistance: No (object) and Yes; see text

    This powerful inscription harms those who enter, pass, or open the warded
    area or object. A glyph of warding can guard a bridge or passage, ward a
    portal, trap a chest or box, and so on.

    More description.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Rating: 4: Need to finish symbols and similar spells.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
