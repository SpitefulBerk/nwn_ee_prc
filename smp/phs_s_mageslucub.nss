/*:://////////////////////////////////////////////
//:: Spell Name Mage�s Lucubration
//:: Spell FileName
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Transmutation
    Level: Wiz 6
    Components: V, S
    Casting Time: 1 standard action
    Range: Personal
    Target: You
    Duration: Instantaneous

    You instantly recall any one spell of 5th level or lower that you have used
    during the past 24 hours. The spell must have been actually cast during that
    period. The recalled spell is stored in your mind as through prepared in the
    normal fashion.

    If the recalled spell requires material components, you must provide them.
    The recovered spell is not usable until the material components are available.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Currently impossible without IncrementRemainingSpellUses(), urg...of course,
    it could be feasable, kinda..somehow, deadly complicatedly.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
