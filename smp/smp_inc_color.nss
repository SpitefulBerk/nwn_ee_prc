/*:://////////////////////////////////////////////
//:: Name Text "Colors" Include
//:: FileName SMP_INC_COLOR
//:://////////////////////////////////////////////
//:: Notes
//:://////////////////////////////////////////////
    Ok, using American spelling of "Colour"...damn stupid world...

    I mean, "Color" said phenetically would be "Cul-law", or "Cul-oor", you need
    a "U" to have the "cul...ur" effect.

    This is from the forums, a big thanks for it.

    Nailog - http://nwn.bioware.com/forums/viewcodepost.html?post=2972536
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

//Color used by saving throws.
const string COLOR_BLUE         = "<cf��>";

//Color used for electric damage.
const string COLOR_DARK_BLUE    = "<c f�>";

//Color used for negative damage.
const string COLOR_GRAY         = "<c&#8482;&#8482;&#8482;>";

//Color used for acid damage.
const string COLOR_GREEN        = "<c � >";

//Color used for the player's name, and cold damage.
const string COLOR_LIGHT_BLUE   = "<c&#8482;��>";

//Color used for system messages.
const string COLOR_LIGHT_GRAY   = "<c���>";

//Color used for sonic damage.
const string COLOR_LIGHT_ORANGE = "<c�&#8482; >";

//Color used for a target's name.
const string COLOR_LIGHT_PURPLE = "<c�&#8482;�>";

//Color used for attack rolls and physical damage.
const string COLOR_ORANGE       = "<c�f >";

//Color used for spell casts, as well as magic damage.
const string COLOR_PURPLE       = "<c�w�>";

//Color used for fire damage.
const string COLOR_RED          = "<c�  >";

//Color used for positive damage.
const string COLOR_WHITE        = "<c���>";

//Color used for healing, and sent messages.
const string COLOR_YELLOW       = "<c�� >";

// End of file Debug lines. Uncomment below "/*" with "//" and compile.
/*
void main()
{
    return;
}
//*/
