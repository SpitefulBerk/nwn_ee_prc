/*:://////////////////////////////////////////////
//:: Spell Name Disguise Self
//:: Spell FileName PHS_S_DisguiseSe
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Disguise Self
    Illusion (Glamer)
    Level: Brd 1, Sor/Wiz 1, Trickery 1
    Components: V, S
    Casting Time: 1 standard action
    Range: Personal
    Target: You
    Duration: 10 min./level (D)

    You make yourself-including clothing, armor, weapons, and equipment-look
    different. You can seem 1 foot shorter or taller, thin, fat, or in between.
    You cannot change your body type. Otherwise, the extent of the apparent
    change is up to you. You could add or obscure a minor feature or look like
    an entirely different person.

    The spell does not provide the abilities or mannerisms of the chosen form,
    nor does it alter the perceived tactile (touch) or audible (sound)
    properties of you or your equipment.

    If you use this spell to create a disguise, you get a +10 bonus on the
    Disguise check.

    A creature that interacts with the glamer gets a Will save to recognize it
    as an illusion.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Without phenotype changing, this is pratically impossible. Rating: 10.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
