/*:://////////////////////////////////////////////
//:: Spell Name Locate Object
//:: Spell FileName PHS_S_LocateObje
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Divination
    Level: Brd 2, Clr 3, Sor/Wiz 2, Travel 2
    Components: V, S, F/DF
    Casting Time: 1 standard action
    Range: Long (40M)
    Area: Circle, centered on you, with a radius of 40M
    Duration: 1 min./level
    Saving Throw: None
    Spell Resistance: No

    You sense the direction of a well-known or clearly visualized object. You
    can search for general items, in which case you locate the nearest one of
    its kind if more than one is within range. Attempting to find a certain item
    requires a specific and accurate mental image; if the image is not close
    enough to the actual object, the spell fails. You cannot specify a unique
    item unless you have observed that particular item firsthand (not through
    divination).

    The spell is blocked by even a thin sheet of lead. Creatures cannot be found
    by this spell. Polymorph any object fools it.

    Arcane Focus: A forked twig.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Um, what should this spell do exactly?
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
