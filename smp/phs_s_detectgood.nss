/*:://////////////////////////////////////////////
//:: Spell Name Detect Good
//:: Spell FileName PHS_S_DetectGood
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Divination
    Level: Clr 1

    This spell functions like detect evil, except that it detects the auras of
    good creatures, clerics or paladins of good deities, good spells, and good
    magic items, and you are vulnerable to an overwhelming good aura if you are
    evil. Healing potions, antidotes, and similar beneficial items are not good.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Needs detect evil.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
