/*:://////////////////////////////////////////////
//:: Spell Name Detect Thoughts
//:: Spell FileName PHS_S_DetectThou
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Divination [Mind-Affecting]
    Level: Brd 2, Knowledge 2, Sor/Wiz 2
    Components: V, S, F/DF
    Casting Time: 1 standard action
    Range: 20M.
    Area: Cone-shaped emanation
    Duration: Concentration, up to 1 min./level (D)
    Saving Throw: Will negates; see text
    Spell Resistance: No

    You detect surface thoughts. The amount of information revealed depends on
    how long you study a particular area or subject.

    1st Round: Presence or absence of thoughts (from conscious creatures with
    Intelligence scores of 1 or higher).

    2nd Round: Number of thinking minds and the Intelligence score of each. If
    the highest Intelligence is 26 or higher (and at least 10 points higher than
    your own Intelligence score), you are stunned for 1 round and the spell ends.
    This spell does not let you determine the location of the thinking minds if
    you can�t see the creatures whose thoughts you are detecting.

    3rd Round: Surface thoughts of any mind in the area. A target�s Will save
    prevents you from reading its thoughts, and you must cast detect thoughts
    again to have another chance. Creatures of animal intelligence (Int 1 or 2)
    have simple, instinctual thoughts that you can pick up.

    Each round, you can turn to detect thoughts in a new area. The spell can
    penetrate barriers, but 1 foot of stone, 1 inch of common metal, a thin sheet
    of lead, or 3 feet of wood or dirt blocks it.

    Arcane Focus: A copper piece.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    We can't really detect thoughts. DM's might need it, or use it.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
