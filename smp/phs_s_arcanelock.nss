/*:://////////////////////////////////////////////
//:: Spell Name Arcane Lock
//:: Spell FileName PHS_S_ArcaneLock
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Abjuration
    Level: Sor/Wiz 2
    Components: V, S, M
    Casting Time: 1 standard action
    Range: Touch
    Target: The door, chest, or portal touched, up to 10 sq. M./level in size
    Duration: Permanent
    Saving Throw: None
    Spell Resistance: No

    Description.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Rating: Depends. If simply put as "Locks a door", as long as it doesn't
    require a key and isn't plot anyway, it'd work great.

    Placeholder script.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}

