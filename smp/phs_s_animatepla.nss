/*:://////////////////////////////////////////////
//:: Spell Name Animate Plants
//:: Spell FileName PHS_S_AnimatePla
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Transmutation
    Level: Drd 7, Plant 7
    Components: V
    Casting Time: 1 standard action
    Range: Close (8M)
    Targets: One Large plant per three caster levels or all plants within range;
             see text
    Duration: 1 round/level or 1 hour/level; see text
    Saving Throw: None
    Spell Resistance: No

    Description.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Rating: As above (IE: Animate Objects): Need plant appearance things.
    If cast in an area of plants, maybe just do an high-powered-entangle

    Placeholder script.

    See Animate Objects. We'll just use "plant" appearances instead of "object"
    ones.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
