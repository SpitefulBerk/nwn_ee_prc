/*:://////////////////////////////////////////////
//:: Spell Name Gentle Repose
//:: Spell FileName PHS_S_GentleRepo
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Necromancy
    Level: Clr 2, Sor/Wiz 3
    Components: V, S, M/DF
    Casting Time: 1 standard action
    Range: Touch
    Target: Corpse touched
    Duration: One day/level
    Saving Throw: Will negates (object)
    Spell Resistance: Yes (object)

    You preserve the remains of a dead creature so that they do not decay. Doing
    so effectively extends the time limit on raising that creature from the dead
    (see raise dead). Days spent under the influence of this spell don�t count
    against the time limit. Additionally, this spell makes transporting a fallen
    comrade more pleasant.

    The spell also works on severed body parts and the like.

    Arcane Material Component: A pinch of salt, and a copper piece for each eye
    the corpse has (or had).
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Rating: 7: Need corpse system, it'd be a 1 then.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
