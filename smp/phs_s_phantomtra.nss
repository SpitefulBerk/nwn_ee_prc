/*:://////////////////////////////////////////////
//:: Spell Name Phantom Trap
//:: Spell FileName PHS_S_PhantomTra
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Illusion (Glamer)
    Level: Sor/Wiz 2
    Components: V, S, M
    Casting Time: 1 standard action
    Range: Touch
    Target: Door or Placable touched
    Duration: Permanent (D)
    Saving Throw: None
    Spell Resistance: No

    This spell makes a lock or other small mechanism seem to be trapped to
    anyone who can detect traps. You place the spell upon any small mechanism or
    device, such as a lock, hinge, hasp, cork, cap, or ratchet. Any character
    able to detect traps, or who uses any spell or device enabling trap
    detection, is 100% certain a real trap exists. Of course, the effect is
    illusory and nothing happens if the trap is �sprung�; its primary purpose
    is to frighten away thieves or make them waste precious time.

    If another phantom trap is active within 13.33 meters when the spell is
    cast, the casting fails.

    Material Component: A piece of iron pyrite touched to the object to be
    trapped while the object is sprinkled with a special dust requiring 50 gp
    to prepare.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Maybe a trap with 0 skill to set. This would be easy to disarm (as it is an
    illusion) but appear to be a normal trap.

    Maybe ActionUseSkill() will work for PC's?
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
