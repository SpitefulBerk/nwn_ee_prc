/*:://////////////////////////////////////////////
//:: Spell Name Water Breathing
//:: Spell FileName PHS_S_WaterBreat
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Transmutation
    Level: Clr 3, Drd 3, Sor/Wiz 3, Water 3
    Components: V, S, M/DF
    Casting Time: 1 standard action
    Range: Touch
    Target: Living creatures touched
    Duration: 2 hours/level; see text
    Saving Throw: Will negates (harmless)
    Spell Resistance: Yes (harmless)

    The transmuted creatures can breathe water freely. Divide the duration
    evenly among all the creatures you touch.

    The spell does not make creatures unable to breathe air.

    Arcane Material Component: A short reed or piece of straw.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Probably DM only...Water? Where?
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
