/*:://////////////////////////////////////////////
//:: Name Appearance Constants.
//:: FileName SMP_INC_APPCONST
//:://////////////////////////////////////////////
    Simply a list of missing (from HotU) or new appearance constants, like the
    polymorph ones.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

const int SMP_APPEARANCE_TYPE_GASEOUS_FORM     = 1000;
const int SMP_APPEARANCE_TYPE_WIND_WALK_CLOUD  = 1001;



// End of file Debug lines. Uncomment below "/*" with "//" and compile.
/*
void main()
{
    return;
}
//*/
