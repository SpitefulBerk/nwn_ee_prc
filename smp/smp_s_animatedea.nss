/*:://////////////////////////////////////////////
//:: Spell Name Animate Dead
//:: Spell FileName SMP_S_AnimateDea
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Animate Dead
    Necromancy [Evil]
    Level: Clr 3, Death 3, Sor/Wiz 4
    Components: V, S, M
    Casting Time: 1 standard action
    Range: Touch
    Targets: One or more corpses touched
    Duration: Instantaneous
    Saving Throw: None
    Spell Resistance: No

    Description.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Rating: 10: Requires AI editing, changing to turning scripts etc. It will
    use, however, EffectTurned() to make them follow the "master", as it is a
    simple thing.

    Placeholder script.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "SMP_INC_SPELLS"

void main()
{

}

