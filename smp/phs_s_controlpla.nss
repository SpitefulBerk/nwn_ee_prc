/*:://////////////////////////////////////////////
//:: Spell Name Control Plants
//:: Spell FileName PHS_S_ControlPla
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Transmutation
    Level: Drd 8, Plant 8
    Components: V, S, DF
    Casting Time: 1 standard action
    Range: Close (8M)
    Targets: Up to 2 HD/level of enemy plant creatures within a 5.0M-radius sphere
    Duration: 1 min./level
    Saving Throw: Will negates
    Spell Resistance: No

    This spell enables you to control the actions of one or more plant
    creatures for a short period of time. You command the creatures by voice
    and they understand you, no matter what language you speak. Even if vocal
    communication is impossible  the controlled plants do not attack you. At
    the end of the spell, the subjects revert to their normal behavior.

    Suicidal or self-destructive commands are simply ignored.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Dominates plants.

    Not complete until test domination things.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
