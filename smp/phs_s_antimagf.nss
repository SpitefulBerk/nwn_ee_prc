/*:://////////////////////////////////////////////
//:: Spell Name Antimagic Field
//:: Spell FileName PHS_S_AntimagF
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Antimagic Field
    Abjuration
    Level: Clr 8, Magic 6, Protection 6, Sor/Wiz 6
    Components: V, S, M/DF
    Casting Time: 1 standard action
    Range: 3.33 M.
    Area: 3.33-M.-radius emanation, centered on you
    Duration: 10 min./level (D)
    Saving Throw: None
    Spell Resistance: See text

    Description.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Rating: 10: This is very hard: Will it remove effects OnEnter, or just
    not allow spells to be cast in it, or what?

    Placeholder script.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
