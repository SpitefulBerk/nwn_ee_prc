/*:://////////////////////////////////////////////
//:: Spell Name Planar Ally
//:: Spell FileName PHS_S_PlanAlly
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Conjuration (Calling) [see text for lesser planar ally]
    Level: Clr 6
    Effect: One or two called elementals or outsiders, totaling no more than 12
    HD, which cannot be more than 30 ft. apart when they appear

    This spell functions like lesser planar ally, except you may call a single
    creature of 12 HD or less, or two creatures of the same kind whose Hit Dice
    total no more than 12. The creatures agree to help you and request your
    return payment together.

    XP Cost: 250 XP.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Do lesser first
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
