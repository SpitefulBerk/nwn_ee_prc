/*:://////////////////////////////////////////////
//:: Spell Name Illusory Wall
//:: Spell FileName PHS_S_IllusoryWa
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Illusion (Figment)
    Level: Sor/Wiz 4
    Components: V, S
    Casting Time: 1 standard action
    Range: Close (8M)
    Effect: Illusionary Image
    Duration: Permanent
    Saving Throw: Will disbelief (if interacted with)
    Spell Resistance: No

    This spell creates the illusion of a wall, floor, ceiling, or similar
    surface. It appears absolutely real when viewed, but physical objects can
    pass through it without difficulty. When the spell is used to hide pits,
    traps, or normal doors, any detection abilities that do not require sight
    work normally. Touch or a probing search reveals the true nature of the
    surface, though such measures do not cause the illusion to disappear.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Need to sort out some of the illision things like this - there is one set
    up to work, to get done first. Hallucaionary or something.

    Note on this: Might be good to create an illusionary "placeable" like stone
    walls or wooden fences.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
