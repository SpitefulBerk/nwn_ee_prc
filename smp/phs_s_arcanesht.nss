/*:://////////////////////////////////////////////
//:: Spell Name Arcane Sight
//:: Spell FileName PHS_S_ArcaneSht
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Divination
    Level: Sor/Wiz 3
    Components: V, S
    Casting Time: 1 standard action
    Range: Personal
    Target: You
    Duration: 1 min./level (D)

    This spell makes your eyes glow blue and allows you to see magical auras
    within 120 feet of you. The effect is similar to that of a detect magic
    spell, but arcane sight does not require concentration and discerns aura
    location and power more quickly.

    You know the location and power of all magical auras within your sight.
    An aura�s power depends on a spell�s functioning level or an item�s caster
    level, as noted in the description of the detect magic spell. If the items
    or creatures bearing the auras are in line of sight, you can make Spellcraft
    skill checks to determine the school of magic involved in each. (Make one
    check per aura; DC 15 + spell level, or 15 + one-half caster level for a
    nonspell effect.)

    If you concentrate on a specific creature within 120 feet of you as a
    standard action, you can determine whether it has any spellcasting or
    spell-like abilities, whether these are arcane or divine (spell-like
    abilities register as arcane), and the strength of the most powerful
    spell or spell-like ability the creature currently has available for use.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Rating: 6: Need to have some DB accessing and stuff...hmmm.

    Scratch the DB accessing stuff, I think...

    Placeholder script.

    Ok, what it can do:

    - Look at things in a heartbeat, and tell the player what he see's glowing
      (visible creatures, objects etc.)
    - Can specifically target something with a power to access more information
      about it.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
