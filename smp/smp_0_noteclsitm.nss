/*
    Notes on special class items.

    These are required to call down spell effects after they have been conjured
    - eg: Call Lightning, with correct caster levels and stuff.

    Note that there are a limit of 8 spells on a class item. They could be added and
    removed manually, I guess, but quickslots go wrong.

    The spell conversation will probably have more info, or something.

    Current ones needed/Added (all under PHS_SPECIAL and in the constants file)

    Powers:
    Call Lightning Bolt: Caster levels for 5 to 40.
    Description:

    This spell power calls down one lightning bolt for either Call Lightning
    Storm or Call Lightning, as appropriate. This will do nothing if you have
    neither of the spells active upon you, or you have run out of bolts to fire.



*/

void main()
{

}
