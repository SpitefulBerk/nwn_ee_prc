/*:://////////////////////////////////////////////
//:: Spell Name Summon Swarm
//:: Spell FileName PHS_S_SummSwarm
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Conjuration (Summoning)
    Level: Brd 2, Drd 2, Sor/Wiz 2
    Components: V, S, M/DF
    Casting Time: 1 round
    Range: Close (8M)
    Effect: One swarm of bats, rats, or spiders
    Duration: Concentration + 2 rounds
    Saving Throw: None
    Spell Resistance: No

    You summon a swarm of bats, rats, or spiders (your choice), which attacks
    all other creatures within its area. (You may summon the swarm so that it
    shares the area of other creatures.) If no living creatures are within its
    area, the swarm attacks or pursues the nearest creature as best it can. The
    caster has no control over its target or direction of travel.

    Arcane Material Component: A square of red cloth.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    This creates the swarm, which works until the duration expires...

    Its harder to do this, but the concentration will mearly do similar things
    as other spells.

    Sub-dials:
    Summon Swarm: Rats
    Summon Swarm: Bats
    Summon Swarm: Spiders

    Animals are considered one being, that can pass through other things. Need
    this modeling.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"
#include "PHS_INC_CONCENTR"

void main()
{

}
