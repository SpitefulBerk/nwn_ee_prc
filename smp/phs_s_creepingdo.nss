/*:://////////////////////////////////////////////
//:: Spell Name Creeping Doom
//:: Spell FileName PHS_S_CreepingDo
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Conjuration (Summoning)
    Level: Drd 7
    Components: V, S
    Casting Time: 1 round
    Range: Close (8M); see text
    Effect: One swarm of centipedes per two levels
    Duration: 1 min./level
    Saving Throw: None
    Spell Resistance: No

    When you utter the spell of creeping doom, you call forth a mass of
    centipede swarms (one per two caster levels, to a maximum of ten swarms at
    20th level), which need not appear adjacent to one another.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////



//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}

