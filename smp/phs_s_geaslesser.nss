/*:://////////////////////////////////////////////
//:: Spell Name Geas, Lesser
//:: Spell FileName PHS_S_GeasLesser
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Enchantment (Compulsion) [Language-Dependent, Mind-Affecting]
    Level: Brd 3, Sor/Wiz 4
    Components: V
    Casting Time: 1 round
    Range: Close (8M)
    Target: One living creature with 7 HD or less
    Duration: One day/level or until discharged (D)
    Saving Throw: Will negates
    Spell Resistance: Yes

    A lesser geas places a magical command on a creature to carry out some
    service or to refrain from some action or course of activity, as desired by
    you. The creature must have 7 or fewer Hit Dice and be able to understand
    you. While a geas cannot compel a creature to kill itself or perform acts
    that would result in certain death, it can cause almost any other course of
    activity.

    The geased creature must follow the given instructions until the geas is
    completed, no matter how long it takes.

    If the instructions involve some open-ended task that the recipient cannot
    complete through his own actions the spell remains in effect for a maximum
    of one day per caster level. A clever recipient can subvert some instructions:

    If the subject is prevented from obeying the lesser geas for 24 hours, it
    takes a -2 penalty to each of its ability scores. Each day, another -2
    penalty accumulates, up to a total of -8. No ability score can be reduced
    to less than 3 by this effect. The ability score penalties are removed 24
    hours after the subject resumes obeying the lesser geas.

    A lesser geas (and all ability score penalties) can be ended by break
    enchantment, limited wish, remove curse, miracle, or wish. Dispel magic does
    not affect a lesser geas.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Not in yet because, frankly, it might never be added, apart from a DM
    spell! ;-) . It requires quite a bit of scripting
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
