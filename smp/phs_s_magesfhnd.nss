/*:://////////////////////////////////////////////
//:: Spell Name Mage�s Faithful Hound
//:: Spell FileName PHS_S_MagesFHnd
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Mage�s Faithful Hound
    Conjuration (Creation)
    Level: Sor/Wiz 5
    Components: V, S, M
    Casting Time: 1 standard action
    Range: Close (8M)
    Effect: Phantom watchdog
    Duration: 1 hour/caster level or until discharged, then 1 round/caster
              level; see text
    Saving Throw: None
    Spell Resistance: No

    You conjure up a phantom watchdog that is invisible, although you can talk
    to it. It then guards the area where it was conjured (it does not move).
    The hound immediately starts barking loudly if any creature approaches
    within 10 meters of it. (Those within 10 meters of the hound when it is
    conjured may move about in the area, but if they leave and return, they
    activate the barking.) The hound sees invisible and ethereal creatures. It
    does not react to figments, but it does react to shadow illusions.

    If an intruder approaches to within 1.67 feet of the hound, the dog stops
    barking and delivers a vicious bite (+10 attack bonus, 2d6+3 points of
    piercing damage) once per round. The dog also gets the bonuses appropriate
    to an invisible creature. Its bite is the equivalent of a magic weapon for
    the purpose of damage reduction. The hound cannot be attacked, but it can
    be dispelled.

    The spell lasts for 1 hour per caster level, but once the hound begins
    barking, it lasts only 1 round per caster level. If you are ever more than
    33.33M distant from the hound, the spell ends.

    Material Component: A tiny silver whistle, a piece of bone, and a thread.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Needs AOE's and stuff. Should be fine to do, rating 3 or so.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
