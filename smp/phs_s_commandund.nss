/*:://////////////////////////////////////////////
//:: Spell Name Command Undead
//:: Spell FileName PHS_S_CommandUnd
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Necromancy
    Level: Sor/Wiz 2
    Components: V, S, M
    Casting Time: 1 standard action
    Range: Close (8M)
    Targets: One undead creature
    Duration: One day/level
    Saving Throw: Will negates; see text
    Spell Resistance: Yes

    This spell allows you some degree of control over an undead creature.
    Assuming the subject is intelligent, it perceives your words and actions in
    the most favorable way (treat its attitude as friendly). It will not attack
    you while the spell lasts. You can try to give the subject orders, but you
    must win an opposed Charisma check to convince it to do anything it wouldn�t
    ordinarily do. (Retries are not allowed.) An intelligent commanded undead
    never obeys suicidal or obviously harmful orders, but it might be convinced
    that something very dangerous is worth doing.

    A nonintelligent undead creature gets no saving throw against this spell.
    When you control a mindless being, you can communicate only basic commands,
    such as �come here,� �go there,� �fight,� �stand still,� and so on.
    Nonintelligent undead won�t resist suicidal or obviously harmful orders.

    Any act by you or your apparent allies that threatens the commanded undead
    (regardless of its Intelligence) breaks the spell.

    Your commands are not telepathic. The undead creature must be able to hear
    you.

    Material Component: A shred of raw meat and a splinter of bone.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Not sure what to use.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
