/*:://////////////////////////////////////////////
//:: Spell Name Discern Lies
//:: Spell FileName PHS_S_DiscernLie
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Divination
    Level: Clr 4, Pal 3
    Components: V, S, DF
    Casting Time: 1 standard action
    Range: Close (8M)
    Target: One creature
    Duration: Concentration, up to 1 round/level
    Saving Throw: Will negates
    Spell Resistance: No

    Each round, you concentrate on one subject, who must be within range.
    You know if the subject deliberately and knowingly speaks a lie by
    discerning disturbances in its aura caused by lying. The spell does not
    reveal the truth, uncover unintentional inaccuracies, or necessarily reveal
    evasions.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    DM spell only. Can only target one person, however. (canged from "can
    concetrate on different ones each round)
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
