/*:://////////////////////////////////////////////
//:: Spell Name Fabricate
//:: Spell FileName PHS_S_Fabricate
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Transmutation
    Level: Sor/Wiz 5
    Components: V, S, M
    Casting Time: See text
    Range: Close (8M)
    Target: Up to 3.33 cu. M./level; see text
    Duration: Instantaneous
    Saving Throw: None
    Spell Resistance: No

    You convert material of one sort into a product that is of the same material.
    Creatures or magic items cannot be created or transmuted by the fabricate
    spell. The quality of items made by this spell is commensurate with the
    quality of material used as the basis for the new fabrication. If you work
    with a mineral, the target is reduced to 0.33 cubic meters per level instead of
    3.33 cubic meters.

    You must make an appropriate Craft check to fabricate articles requiring a
    high degree of craftsmanship.

    Casting requires 1 round per 3.33 cubic meters (or 0.33 cubic meters) of
    material to  be affected by the spell.

    Material Component: The original material, which costs the same amount as
    the raw materials required to craft the item to be created.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Rating: 6: Needs a, well, system...maybe transforming weaponsinto
    other weapons...no idea...

    Placeholder script.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
