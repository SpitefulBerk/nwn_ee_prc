/*:://////////////////////////////////////////////
//:: Spell Name Comprehend Languages
//:: Spell FileName PHS_S_CompreLang
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Divination
    Level: Brd 1, Clr 1, Sor/Wiz 1
    Components: V, S, M/DF
    Casting Time: 1 standard action
    Range: Personal
    Target: You
    Duration: 10 min./level

    You can understand the spoken words of creatures or read otherwise
    incomprehensible written messages. In either case, you must touch the
    creature or the writing. The ability to read does not necessarily impart
    insight into the material, merely its literal meaning. The spell enables
    you to understand or read an unknown language, not speak or write it.

    Written material can be read at the rate of one page (250 words) per minute.
    Magical writing cannot be read, though the spell reveals that it is magical.
    This spell can be foiled by certain warding magic (such as the secret page
    and illusory script spells). It does not decipher codes or reveal messages
    concealed in otherwise normal text.

    Comprehend languages can be made permanent with a permanency spell.

    Arcane Material Component: A pinch of soot and a few grains of salt.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Spell placeholder script.

    DMFI only?
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
