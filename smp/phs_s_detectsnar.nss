/*:://////////////////////////////////////////////
//:: Spell Name Detect Snares and Pits
//:: Spell FileName PHS_S_DetectSnar
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Divination
    Level: Drd 1, Rgr 1
    Components: V, S
    Casting Time: 1 standard action
    Range: 20M.
    Area: Cone-shaped emanation
    Duration: Concentration, up to 10 min./level (D)
    Saving Throw: None
    Spell Resistance: No

    You can detect simple pits, deadfalls, and snares as well as mechanical
    traps constructed of natural materials. The spell does not detect complex
    traps, including trapdoor traps.

    Detect snares and pits does detect certain natural hazards-quicksand
    (a snare), a sinkhole (a pit), or unsafe walls of natural rock (a deadfall).
    However, it does not reveal other potentially dangerous conditions. The
    spell does not detect magic traps (except those that operate by pit,
    deadfall, or snaring; see the spell snare), nor mechanically complex ones,
    nor those that have been rendered safe or inactive.

    The amount of information revealed depends on how long you study a particular
    area.

    1st Round: Presence or absence of hazards.

    2nd Round: Number of hazards and the location of each. If a hazard is outside
    your line of sight, then you discern its direction but not its exact location.

    Each Additional Round: The general type and trigger for one particular hazard
    closely examined by you.

    Each round, you can turn to detect snares and pits in a new area. The spell
    can penetrate barriers, but 1 foot of stone, 1 inch of common metal, a thin
    sheet of lead, or 3 feet of wood or dirt blocks it.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Might be possible, but need pits and snares first!
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
