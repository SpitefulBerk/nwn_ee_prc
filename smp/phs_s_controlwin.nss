/*:://////////////////////////////////////////////
//:: Spell Name Control Winds
//:: Spell FileName PHS_S_ControlWin
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Transmutation [Air]
    Level: Air 5, Drd 5
    Components: V, S
    Casting Time: 1 standard action
    Range: 13.33M./level
    Area: 13.33M./level radius cylinder 13.33M. high
    Duration: 10 min./level
    Saving Throw: Fortitude negates
    Spell Resistance: No

    You alter wind force in the area surrounding you. You can make the wind
    blow in a certain direction or manner, increase its strength, or decrease
    its strength.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Rating: What is this useful for? Blowing away AOE's and knockdown is
    about it. Do later.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
