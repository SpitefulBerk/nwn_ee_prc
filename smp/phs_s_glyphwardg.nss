/*:://////////////////////////////////////////////
//:: Spell Name Glyph of Warding, Greater
//:: Spell FileName PHS_S_GlyphWardG
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Abjuration
    Level: Clr 6

    This spell functions like glyph of warding, except that a greater blast glyph
    deals up to 10d8 points of damage, and a greater spell glyph can store a spell
    of 6th level or lower.

    Material Component: You trace the glyph with incense, which must first be
    sprinkled with powdered diamond worth at least 400 gp.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Rating: 4: Need to finish symbols and similar spells.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
