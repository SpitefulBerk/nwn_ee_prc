/*:://////////////////////////////////////////////
//:: Spell Name Contagion
//:: Spell FileName PHS_S_Contagion
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Necromancy [Evil]
    Level: Clr 3, Destruction 3, Drd 3, Sor/Wiz 4
    Components: V, S
    Casting Time: 1 standard action
    Range: Touch
    Target: Living creature touched
    Duration: Instantaneous
    Saving Throw: Fortitude negates
    Spell Resistance: Yes

    The subject contracts a disease selected from the table below, which
    strikes immediately (no incubation period). The DC noted is for the
    subsequent saves (use contagionís normal save DC for the initial saving
    throw).
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    The problem with this is the initial save, we can't edit it.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
