/*:://////////////////////////////////////////////
//:: Spell Name Major Creation
//:: Spell FileName PHS_S_MajorCreat
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Conjuration (Creation)
    Level: Sor/Wiz 5
    Casting Time: 10 minutes
    Range: Close (8M)
    Duration: See text

    This spell functions like minor creation, except that you can also create
    an object of mineral nature: stone, crystal, metal, or the like. The
    duration of the created item varies with its relative hardness and rarity,
    as indicated on the following table.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Not done. Creates gems and fake stuff, however, it might be hard to make
    this workable without a DM's permission. Rating to create; not much,
    but needs conversation work! Rating: 4 at least.

    Also needs minor creation.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
