/*:://////////////////////////////////////////////
//:: Spell Name Persistent Image
//:: Spell FileName PHS_S_Persistent
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Illusion (Figment)
    Level: Brd 5, Sor/Wiz 5
    Duration: 1 min./level (D)

    This spell functions like silent image, except that the figment includes
    visual, auditory, olfactory, and thermal components, and the figment follows
    a script determined by you. The figment follows that script without your
    having to concentrate on it. The illusion can include intelligible speech if
    you wish.

    Material Component: A bit of fleece and several grains of sand.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Need silent image.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
