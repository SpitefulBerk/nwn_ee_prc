/*:://////////////////////////////////////////////
//:: Spell Name Nondetection
//:: Spell FileName PHS_S_Nondetecti
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Abjuration
    Level: Rgr 4, Sor/Wiz 3, Trickery 3
    Components: V, S, M
    Casting Time: 1 standard action
    Range: Touch
    Target: Creature or object touched
    Duration: 1 hour/level
    Saving Throw: Will negates (harmless, object)
    Spell Resistance: Yes (harmless, object)

    The warded creature or object becomes difficult to detect by divination
    spells such as clairaudience/clairvoyance, locate object, and detect spells.
    Nondetection also prevents location by such magic items as crystal balls. If
    a divination is attempted against the warded creature or item, the caster of
    the divination must succeed on a caster level check (1d20 + caster level)
    against a DC of 11 + the caster level of the spellcaster who cast
    nondetection. If you cast nondetection on yourself or on an item currently
    in your possession, the DC is 15 + your caster level.

    If cast on a creature, nondetection wards the creature�s gear as well as
    the creature itself.

    Material Component: A pinch of diamond dust worth 50 gp.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Probably easy enough, I don't know, no scyring/detecting yet.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
