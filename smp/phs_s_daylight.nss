/*:://////////////////////////////////////////////
//:: Spell Name Daylight
//:: Spell FileName PHS_S_Daylight
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Evocation [Light]
    Level: Brd 3, Clr 3, Drd 3, Pal 3, Sor/Wiz 3
    Components: V, S
    Casting Time: 1 standard action
    Range: Touch
    Target: Object touched
    Duration: 10 min./level (D)
    Saving Throw: None
    Spell Resistance: No

    The object touched sheds light as bright as full daylight in a 20.0-M radius,
    and dim light for an additional 20.0M beyond that. Creatures that take
    penalties in bright light also take them while within the radius of this
    magical light. Despite its name, this spell is not the equivalent of daylight
    for the purposes of creatures that are damaged or destroyed by bright light
    (such as vampires).

    Daylight brought into an area of magical darkness (or vice versa) is
    temporarily negated, so that the otherwise prevailing light conditions exist
    in the overlapping areas of effect.

    Daylight counters or dispels any darkness or deeper darkness spells.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script:

    Rating: 6: Need to have a special custom VFX probably for this, else
    use DUR_LIGHT or sometihng it dispels darkness mainly.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
