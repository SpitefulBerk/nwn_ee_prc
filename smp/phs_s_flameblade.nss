/*:://////////////////////////////////////////////
//:: Spell Name Flame Blade
//:: Spell FileName
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Range: 0M.
    Effect: Sword-like beam
    Duration: 1 min./level (D)
    Saving Throw: None
    Spell Resistance: Yes

    A 3-foot-long, blazing beam of red-hot fire springs forth from your hand.
    You wield this bladelike beam as if it were a scimitar. The blade deals 1d8
    points of fire damage +1 point per two caster levels (maximum +10). Since
    the blade is immaterial, your Strength modifier does not apply to the
    damage.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Creates a sword to use.

    Scimitar - no combat damage, -10 damage (For the strength penalty) and
    does 1d8 + 1/2 caster levels to a max of +10, added in the script.

    It is cursed, it is avalible to equip normally.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
