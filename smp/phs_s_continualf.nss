/*:://////////////////////////////////////////////
//:: Spell Name Continual Flame
//:: Spell FileName PHS_S_ContinualF
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Evocation [Light]
    Level: Clr 3, Sor/Wiz 2
    Components: V, S, M
    Casting Time: 1 standard action
    Range: Touch
    Target: Object touched
    Effect: Magical, heatless flame
    Duration: Permanent
    Saving Throw: None
    Spell Resistance: No

    A flame, equivalent in brightness to a torch, springs forth from an object
    that you touch. The effect looks like a regular flame, but it creates no
    heat and doesn�t use oxygen. A continual flame can be covered and hidden
    but not smothered or quenched.

    Light spells counter and dispel darkness spells of an equal or lower level.

    Material Component: You sprinkle ruby dust (worth 50 gp) on the item that is
    to carry the flame.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    PLaceholder script.

    This should be possible.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
