/*:://////////////////////////////////////////////
//:: Spell Name Create Food and Water
//:: Spell FileName PHS_S_CreateFood
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Conjuration (Creation)
    Level: Clr 3
    Components: V, S
    Casting Time: 10 minutes
    Range: Close (8M)
    Effect: Food and water to sustain three humans or one horse/level for 24 hours
    Duration: 24 hours; see text
    Saving Throw: None
    Spell Resistance: No

    The food that this spell creates is simple fare of your choice-highly
    nourishing, if rather bland. Food so created decays and becomes inedible
    within 24 hours, although it can be kept fresh for another 24 hours by
    casting a purify food and water spell on it. The water created by this
    spell is just like clean rain water, and it doesn�t go bad as the food does.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.


//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
