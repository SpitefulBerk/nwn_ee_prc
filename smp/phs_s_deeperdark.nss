/*:://////////////////////////////////////////////
//:: Spell Name Deeper Darkness
//:: Spell FileName PHS_S_DeeperDark
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Evocation [Darkness]
    Level: Clr 3
    Components: V, M/DF
    Casting Time: 1 standard action
    Range: Touch
    Target: Object touched
    Duration: One day/level (D)
    Saving Throw: None
    Spell Resistance: No

    This spell functions like darkness, except that the object radiates shadowy
    illumination in a 20-M radius and the darkness lasts longer. This spell
    causes an object to radiate shadowy illumination out to a 20-M radius. All
    creatures in the area gain concealment (20% miss chance). Even creatures
    that can normally see in such conditions (such as with darkvision or low-light
    vision) have the miss chance in an area shrouded in magical darkness.

    Normal lights (torches, candles, lanterns, and so forth) are incapable of
    brightening the area. Higher level light spells can dispel or counter darkness.

    If darkness is cast on a small object that is then placed inside or under a
    lightproof covering, the spell�s effect is blocked until the covering is
    removed.

    Daylight brought into an area of deeper darkness (or vice versa) is
    temporarily negated, so that the otherwise prevailing light conditions exist
    in the overlapping areas of effect.

    Darkness counters or dispels daylight and Continual Flame. To dispel such
    spell, target an affected creature.

    Arcane Material Component: A bit of bat fur and either a drop of pitch or
    a piece of coal.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Will work exactly the same as Darkness.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
