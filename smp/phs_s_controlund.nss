/*:://////////////////////////////////////////////
//:: Spell Name Control Undead
//:: Spell FileName PHS_S_ControlUnd
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Necromancy
    Level: Sor/Wiz 7
    Components: V, S, M
    Casting Time: 1 standard action
    Range: Close (8M)
    Targets: Up to 2 HD/level of enemy undead creatures within a 5.0M-radius sphere
    Duration: 1 min./level
    Saving Throw: Will negates
    Spell Resistance: Yes

    This spell enables you to command undead creatures for a short period of time.
    You command them by voice and they understand you, no matter what language
    you speak. Even if vocal communication is impossible the controlled undead
    do not attack you. At the end of the spell, the subjects revert to their
    normal behavior.

    Intelligent undead creatures remember that you controlled them.

    Material Component: A small piece of bone and a small piece of raw meat.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    See control undead. Might use EffectCutseenDominated if it works.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
