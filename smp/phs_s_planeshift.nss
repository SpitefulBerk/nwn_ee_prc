/*:://////////////////////////////////////////////
//:: Spell Name Plane Shift
//:: Spell FileName PHS_S_PlaneShift
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Conjuration (Teleportation)
    Level: Clr 5, Sor/Wiz 7
    Components: V, S, F
    Casting Time: 1 standard action
    Range: Touch
    Target: Creature touched, or up to eight willing creatures joining hands
    Duration: Instantaneous
    Saving Throw: Will negates
    Spell Resistance: Yes

    You move yourself or some other creature to another plane of existence or
    alternate dimension. If several willing persons link hands in a circle, as
    many as eight can be affected by the plane shift at the same time. Precise
    accuracy as to a particular arrival location on the intended plane is nigh
    impossible. From the Material Plane, you can reach any other plane, though
    you appear 5 to 500 miles (5d%) from your intended destination.

    Note: Plane shift transports creatures instantaneously and then ends. The
    creatures need to find other means if they are to travel back.

    Focus: A small, forked metal rod. The size and metal type dictates to which
    plane of existence or alternate dimension the spell sends the affected
    creatures.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    This will be possible when planes are eventually in. Maybe an escape
    method, if they can outrun the appropriate monsters...
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
