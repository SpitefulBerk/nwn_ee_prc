/*:://////////////////////////////////////////////
//:: Spell Name Detect Secret Doors
//:: Spell FileName PHS_S_DetectSecr
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Divination
    Level: Brd 1, Knowledge 1, Sor/Wiz 1
    Components: V, S
    Casting Time: 1 standard action
    Range: 20M
    Area: Cone-shaped emanation
    Duration: Concentration, up to 1 min./level (D)
    Saving Throw: None
    Spell Resistance: No

    You can detect secret doors, compartments, caches, and so forth. Only
    passages, doors, or openings that have been specifically constructed to
    escape detection are detected by this spell. The amount of information
    revealed depends on how long you study a particular area or subject.

    1st Round: Presence or absence of secret doors.

    2nd Round: Number of secret doors and the location of each. If an aura is
    outside your line of sight, then you discern its direction but not its exact
    location.

    Each Additional Round: The mechanism or trigger for one particular secret
    portal closely examined by you. Each round, you can turn to detect secret
    doors in a new area. The spell can penetrate barriers, but 1 foot of stone,
    1 inch of common metal, a thin sheet of lead, or 3 feet of wood or dirt
    blocks it.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Need to investigate Bioware's own secret doors to work at all.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
