/*:://////////////////////////////////////////////
//:: Spell Name Clairaudience/Clairvoyance
//:: Spell FileName PHS_S_Clairvoyan
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Divination (Scrying)
    Level: Brd 3, Knowledge 3, Sor/Wiz 3
    Components: V, S, F/DF
    Casting Time: 10 minutes
    Range: Long (40M)
    Effect: Magical sensor
    Duration: 1 min./level (D)
    Saving Throw: None
    Spell Resistance: No

    Clairaudience/clairvoyance creates an invisible magical sensor at a specific
    location that enables you to hear or see (your choice) almost as if you were
    there. You don�t need line of sight or line of effect, but the locale must
    be known-a place familiar to you or an obvious one. Once you have selected
    the locale, the sensor doesn�t move, but you can rotate it in all directions
    to view the area as desired. Unlike other scrying spells, this spell does
    not allow magically or supernaturally enhanced senses to work through it.
    If the chosen locale is magically dark, you see nothing. If it is naturally
    pitch black, you can see in a 10- foot radius around the center of the
    spell�s effect. Clairaudience/clairvoyance functions only on the plane of
    existence you are currently occupying.

    Arcane Focus: A small horn (for hearing) or a glass eye (for seeing).
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Only here as a placerholder script.

    Should be able to do it, probably...
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
