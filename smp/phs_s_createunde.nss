/*:://////////////////////////////////////////////
//:: Spell Name Create Undead
//:: Spell FileName PHS_S_CreateUnde
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Necromancy [Evil]
    Level: Clr 6, Death 6, Evil 6, Sor/Wiz 6
    Components: V, S, M
    Casting Time: 1 hour
    Range: Close (8M)
    Target: One corpse
    Duration: Instantaneous
    Saving Throw: None
    Spell Resistance: No

    A much more potent spell than animate dead, this evil spell allows you to
    create more powerful sorts of undead: ghouls, ghasts, mummies, and mohrgs.
    The type or types of undead you can create is based on your caster level,
    as shown on the table below.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
