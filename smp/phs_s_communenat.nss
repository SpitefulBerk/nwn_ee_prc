/*:://////////////////////////////////////////////
//:: Spell Name Commune with Nature
//:: Spell FileName PHS_S_CommuneNat
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Divination
    Level: Animal 5, Drd 5, Rgr 4
    Components: V, S
    Casting Time: 10 minutes
    Range: Personal
    Target: You
    Duration: Instantaneous

    You become one with nature, attaining knowledge of the surrounding territory.
    You instantly gain knowledge of as many as three facts from among the
    following subjects: the ground or terrain, plants, minerals, bodies of water,
    people, general animal population, presence of woodland creatures, presence
    of powerful unnatural creatures, or even the general state of the natural
    setting.

    In outdoor settings, the spell operates in a radius of 1 mile per caster
    level. In natural underground settings-caves, caverns, and the like-the
    radius is limited to 100 feet per caster level. The spell does not function
    where nature has been replaced by construction or settlement, such as in
    dungeons and towns.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    DM or area-set information only?
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
