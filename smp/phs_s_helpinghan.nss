/*:://////////////////////////////////////////////
//:: Spell Name Helping Hand
//:: Spell FileName PHS_S_HelpingHan
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Evocation
    Level: Clr 3
    Components: V, S, DF
    Casting Time: 1 standard action
    Range: 5 miles
    Effect: Ghostly hand
    Duration: 1 hour/level
    Saving Throw: None
    Spell Resistance: No

    You create the ghostly image of a hand, which you can send to find a
    creature within 5 miles. The hand then beckons to that creature and leads
    it to you if the creature is willing to follow.

    More description.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Rating: 5: Need to work our what this really has a use for. Use: Creates
    "hand" which goes off to a specific creature, and beckons them to the caster.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
