/*:://////////////////////////////////////////////
//:: Spell Name Invisibility, Mass
//:: Spell FileName PHS_S_InvisMass
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Illusion (Glamer)
    Level: Sor/Wiz 7
    Components: V, S, M
    Range: Long (40M)
    Targets: Any number of creatures, no two of which can be more than 180 ft. apart

    This spell functions like invisibility, except that the effect is mobile with
    the group and is broken when anyone in the group attacks. Individuals in the
    group cannot see each other. The spell is broken for any individual who moves
    more than 180 feet from the nearest member of the group. (If only two
    individuals are affected, the one moving away from the other one loses its
    invisibility. If both are moving away from each other, they both become
    visible when the distance between them exceeds 180 feet.)

    Material Component: An eyelash encased in a bit of gum arabic.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Entirely possible, Rating: 3, but need a good set of AOE scripts.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
