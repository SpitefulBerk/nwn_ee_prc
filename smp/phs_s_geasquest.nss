/*:://////////////////////////////////////////////
//:: Spell Name Geas/Quest
//:: Spell FileName PHS_S_GeasQuest
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Enchantment (Compulsion) [Language-Dependent, Mind-Affecting]
    Level: Brd 6, Clr 6, Sor/Wiz 6
    Casting Time: 10 minutes
    Target: One living creature
    Saving Throw: None

    This spell functions similarly to lesser geas, except that it affects a
    creature of any HD and allows no saving throw.

    Instead of taking penalties to ability scores (as with lesser geas), the
    subject takes 3d6 points of damage each day it does not attempt to follow
    the geas/quest. Additionally, each day it must make a Fortitude saving
    throw or become sickened. These effects end 24 hours after the creature
    attempts to resume the geas/ quest.

    A remove curse spell ends a geas/quest spell only if its caster level is at
    least two higher than your caster level. Break enchantment does not end a
    geas/quest, but limited wish, miracle, and wish do.

    Bards, sorcerers, and wizards usually refer to this spell as geas, while
    clerics call the same spell quest.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Not in yet because, frankly, it might never be added, apart from a DM
    spell! ;-) . It requires quite a bit of scripting.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
