/*:://////////////////////////////////////////////
//:: Spell Name Detect Evil
//:: Spell FileName PHS_S_DetectEvil
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Divination
    Level: Clr 1
    Components: V, S, DF
    Casting Time: 1 standard action
    Range: 20M.
    Area: Cone-shaped emanation
    Duration: Concentration, up to 10 min./ level (D)
    Saving Throw: None
    Spell Resistance: No

    You can sense the presence of evil. The amount of information revealed
    depends on how long you study a particular area or subject.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Possible, really, and should be easy enough, if limited to not including
    magical items.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
