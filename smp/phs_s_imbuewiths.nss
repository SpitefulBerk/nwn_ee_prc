/*:://////////////////////////////////////////////
//:: Spell Name Imbue with Spell Ability
//:: Spell FileName PHS_S_ImbuewithS
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Evocation
    Level: Clr 4, Magic 4
    Components: V, S, DF
    Casting Time: 10 minutes
    Range: Touch
    Target: Creature touched; see text
    Duration: Permanent until discharged (D)
    Saving Throw: Will negates (harmless)
    Spell Resistance: Yes (harmless)

    You transfer some of your currently prepared spells, and the ability to cast
    them, to another creature. Only a creature with an Intelligence score of at
    least 5 and a Wisdom score of at least 9 can receive this bestowal. Only
    cleric spells from the schools of abjuration, divination, and conjuration
    (healing) can be transferred. The number and level of spells that the
    subject can be granted depends on its Hit Dice; even multiple castings of
    imbue with spell ability can�t exceed this limit.

    More description.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Very hard.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
