/*:://////////////////////////////////////////////
//:: Spell Name Enlarge Person
//:: Spell FileName PHS_S_EnlrgPer
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Transmutation
    Level: Sor/Wiz 1, Strength 1
    Components: V, S, M
    Casting Time: 1 round
    Range: Close (8M)
    Target: One humanoid creature
    Duration: 1 min./level (D)
    Saving Throw: Fortitude negates
    Spell Resistance: Yes

    This spell causes instant growth of a humanoid creature, doubling its height
    and multiplying its weight by 8. This increase changes the creature�s size
    category to the next larger one. The target gains a +2 size bonus to Strength,
    a -2 size penalty to Dexterity (to a minimum of 1), and a -1 penalty on
    attack rolls and AC due to its increased size.

    More description...
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    This is definaltly hard to do visually.

    The actual effects might not be too hard though :-)

    Placeholder script
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
