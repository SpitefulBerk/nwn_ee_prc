/*:://////////////////////////////////////////////
//:: Spell Name Water Walk
//:: Spell FileName PHS_S_WaterWalk
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Transmutation [Water]
    Level: Clr 3, Rgr 3
    Components: V, S, DF
    Casting Time: 1 standard action
    Range: Touch
    Targets: One touched creature/level
    Duration: 10 min./level (D)
    Saving Throw: Will negates (harmless)
    Spell Resistance: Yes (harmless)

    The transmuted creatures can tread on any liquid as if it were firm ground.
    Mud, oil, snow, quicksand, running water, ice, and even lava can be
    traversed easily, since the subjects� feet hover an inch or two above the
    surface. (Creatures crossing molten lava still take damage from the heat
    because they are near it.) The subjects can walk, run, charge, or otherwise
    move across the surface as if it were normal ground.

    If the spell is cast underwater (or while the subjects are partially or
    wholly submerged in whatever liquid they are in), the subjects are borne
    toward the surface at 60 feet per round until they can stand on it.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Might remove. Quite impossible without tileset changes.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
