/*:://////////////////////////////////////////////
//:: Spell Name Demand
//:: Spell FileName PHS_S_Demand
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Demand
    Enchantment (Compulsion) [Mind-Affecting]
    Level: Sor/Wiz 8
    Saving Throw: Will partial
    Spell Resistance: Yes

    This spell functions like sending, but the message can also contain a
    suggestion (see the suggestion spell), which the subject does its best to
    carry out. A successful Will save negates the suggestion effect but not the
    contact itself. The demand, if received, is understood even if the subjectís
    Intelligence score is as low as 1. If the message is impossible or
    meaningless according to the circumstances that exist for the subject at the
    time the demand is issued, the message is understood but the suggestion is
    ineffective.

    The demandís message to the creature must be twenty-five words or less,
    including the suggestion. The creature can also give a short reply immediately.

    Material Component: A short piece of copper wire and some small part of the
    subject-a hair, a bit of nail, or the like.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Need suggestion.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
