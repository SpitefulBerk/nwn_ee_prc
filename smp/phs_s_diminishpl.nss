/*:://////////////////////////////////////////////
//:: Spell Name Diminish Plants
//:: Spell FileName PHS_S_DiminishPl
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Diminish Plants
    Transmutation
    Level: Drd 3, Rgr 3
    Components: V, S, DF
    Casting Time: 1 standard action
    Range: See text
    Target or Area: See text
    Duration: Instantaneous
    Saving Throw: None
    Spell Resistance: No

    This spell has two versions.

    Prune Growth: This version causes normal vegetation within long range (40M)
    to shrink to about one-third of their normal size, becoming untangled and
    less bushy. The affected vegetation appears to have been carefully pruned
    and trimmed.

    You may also designate portions of the area that are not affected.

    Stunt Growth: This version targets normal plants within a your current area,
    reducing their potential productivity over the course of the following year
    to one third below normal.

    Diminish plants counters plant growth.

    This spell has no effect on plant creatures.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    About the only current use is to counter plant growth.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
