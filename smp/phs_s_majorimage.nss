/*:://////////////////////////////////////////////
//:: Spell Name Major Image
//:: Spell FileName PHS_S_MajorImage
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Illusion (Figment)
    Level: Brd 3, Sor/Wiz 3
    Duration: Concentration + 3 rounds

    This spell functions like silent image, except that sound, smell, and thermal
    illusions are included in the spell effect. While concentrating, you can move
    the image within the range.

    The image disappears when struck by an opponent unless you cause the illusion
    to react appropriately.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Need Silent Image.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
