/*:://////////////////////////////////////////////
//:: Spell Name Detect Law
//:: Spell FileName PHS_S_DetectLaw
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Divination
    Level: Clr 1

    This spell functions like detect evil, except that it detects the auras of
    lawful creatures, clerics of lawful deities, lawful spells, and lawful magic
    items, and you are vulnerable to an overwhelming lawful aura if you are
    chaotic.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    Needs detect evil.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
