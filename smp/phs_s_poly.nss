/*:://////////////////////////////////////////////
//:: Spell Name Polymorph
//:: Spell FileName PHS_S_Poly
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Transmutation
    Level: Sor/Wiz 4
    Components: V, S, M
    Casting Time: 1 standard action
    Range: Touch
    Target: Willing living creature touched
    Duration: 1 min./level (D)
    Saving Throw: None
    Spell Resistance: No

    This spell functions like alter self, except that you change the willing
    subject into another form of living creature. The new form may be of the
    same type as the subject or any of the following types: aberration, animal,
    dragon, fey, giant, humanoid, magical beast, monstrous humanoid, ooze,
    plant, or vermin. The assumed form can�t have more Hit Dice than your
    caster level (or the subject�s HD, whichever is lower), to a maximum of 15
    HD at 15th level. You can�t cause a subject to assume a form smaller than
    Fine, nor can you cause a subject to assume an incorporeal or gaseous form.
    The subject�s creature type and subtype (if any) change to match the new
    form.

    Upon changing, the subject regains lost hit points as if it had rested for
    a night (though this healing does not restore temporary ability damage and
    provide other benefits of resting; and changing back does not heal the
    subject further). If slain, the subject reverts to its original form, though
    it remains dead.

    The subject gains the Strength, Dexterity, and Constitution scores of the
    new form but retains its own Intelligence, Wisdom, and Charisma scores. It
    also gains all extraordinary special attacks possessed by the form but does
    not gain the extraordinary special qualities possessed by the new form or
    any supernatural or spell-like abilities.

    Incorporeal or gaseous creatures are immune to being polymorphed, and a
    creature with the shapechanger subtype can revert to its natural form as a
    standard action.

    Material Component: An empty cocoon.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Need to add the relivant polymorph entries before this is even attempted,
    I'm afraid.

    The choice will likely be immense!

    Placeholder script.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
