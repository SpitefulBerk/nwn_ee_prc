/*:://////////////////////////////////////////////
//:: Spell Name Mage�s Magnificent Mansion
//:: Spell FileName PHS_S_MagesMgMan
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Conjuration (Creation)
    Level: Sor/Wiz 7
    Components: V, S, F
    Casting Time: 1 standard action
    Range: Close (8M)
    Effect: Extradimensional mansion (S)
    Duration: 2 hours/level (D)
    Saving Throw: None
    Spell Resistance: No

    You conjure up an extradimensional dwelling that has a single entrance on
    the plane from which the spell was cast. The entry point looks like a faint
    shimmering in the air. Only those in your party may enter the mansion, and
    the portal is shut and made invisible behind you when you enter. You may
    open it again by simply opening the exit door that leads from the mansion.
    Once observers have passed beyond the entrance, they are in a magnificent
    foyer with numerous chambers beyond. The atmosphere is clean, fresh, and
    warm.

    The plan of the Mansion is 50x50M up to level 14, 70x70 from levels 15 to
    18 and 100x100 from levels 19 and up. Choices of many interiors you choose
    before casting the spell are avalible and are furnished approprately. The
    Mansion has enough food for an entire banquet and is a safe place to rest.
    A staff of near-transparent servants (as many as two per caster level),
    liveried and obedient, wait upon all who enter. The servants function as
    unseen servant spells except that they are visible and can go anywhere in
    the mansion. They can provide drinks and food.

    Since the place can be entered only through its special portal, outside
    conditions do not affect the mansion, nor do conditions inside it pass to
    the plane beyond.

    Focus: A miniature portal carved from ivory, a small piece of polished
    marble, and a tiny silver spoon (each item worth 5 gp).
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    This spell creates a doorway which will lead to an approprately sized
    mansion, if it isn't empty.

    The caster can choose beforehand, or at the time of casting (if not set or
    already someone in thier mansion size and type) what type of mansion to use.

    Tags are as in PHS_MAGEMAN11, for type 1 (castle) size 1 (smallest, 5x5)
    Size - Tiles
    1    - 5x5 (50x50M). Includes some bedrooms, and a grandish banquet.
    2    - 7x7 (70x70M). Includes additional battle-orentated place.
    3    - 10x10 (100x100M). Includes 2 floors (in the same area), thus more space.

    Number - Tileset Type
    1      - Castle Interior
    2      - City Interior (House/Inn style)
    3      - Underdark Interior
    4      -

    etc.

    Not complete.
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
