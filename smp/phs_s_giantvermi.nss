/*:://////////////////////////////////////////////
//:: Spell Name Giant Vermin
//:: Spell FileName PHS_S_GiantVermi
//:://////////////////////////////////////////////
//:: In Game Spell desctiption
//:://////////////////////////////////////////////
    Transmutation
    Level: Clr 4, Drd 4
    Components: V, S, DF
    Casting Time: 1 standard action
    Range: Close (8M)
    Targets: Up to three vermin, captured in your inventory
    Duration: 1 min./level
    Saving Throw: None
    Spell Resistance: Yes

    You turn three normal-sized centipedes, two normal-sized spiders, or a single
    normal-sized scorpion into larger forms. Only one type of vermin can be
    transmuted (so a single casting cannot affect both a centipede and a spider),
    and all must be grown to the same size. The size to which the vermin can be
    grown depends on your level; see the table below.

    More description.
//:://////////////////////////////////////////////
//:: Spell Effects Applied / Notes
//:://////////////////////////////////////////////
    Placeholder script.

    "Summoning" spell. It'll use some items as a material
    component to grow the things...
//:://////////////////////////////////////////////
//:: Created By: Jasperre
//::////////////////////////////////////////////*/

#include "PHS_INC_SPELLS"

void main()
{

}
