//:://////////////////////////////////////////////
//:: FileName: "at_contreu_*****"
/*   Purpose: These scripts set the variable for use in the Contingent Reunion
        spell. The variable is the condition that will trigger the teleportation
*/
//:://////////////////////////////////////////////
//:: Created By: Boneshank
//:: Last Updated On:
//:://////////////////////////////////////////////
void main()
{
    SetLocalInt(GetPCSpeaker(), "nReunionTrigger", 13);
}
