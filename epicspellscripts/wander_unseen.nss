//:://////////////////////////////////////////////
//:: FileName: "wander_unseen"
/*   Purpose: Wander Unseen - this is the ability that is granted to a player
        as the result of an Unseen Wanderer epic spell. Using this feat will
        either turn the player invisible, or if already in that state, visible
        again.
*/
//:://////////////////////////////////////////////
//:: Created By: Boneshank
//:: Last Updated On: March 13, 2004
//:://////////////////////////////////////////////
#include "prc_alterations"
#include "inc_dispel"
#include "inc_epicspells"

void main()
{
    PRCSetSchool(SPELL_SCHOOL_ILLUSION);

    object oPC = OBJECT_SELF;

    if(GetHasSpellEffect(SPELL_WANDER_UNSEEN))
    {
        PRCRemoveEffectsFromSpell(oPC, SPELL_WANDER_UNSEEN);
    }
    else
    {
        effect eInv = SupernaturalEffect(EffectInvisibility(INVISIBILITY_TYPE_NORMAL));
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, eInv, oPC);
    }
    PRCSetSchool();
}