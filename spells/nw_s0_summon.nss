//::///////////////////////////////////////////////
//:: Summon Creature Series
//:: NW_S0_Summon
//:: Copyright (c) 2001 Bioware Corp.
//:://////////////////////////////////////////////
/*
    Carries out the summoning of the appropriate
    creature for the Summon Monster Series of spells
    1 to 9
*/
//:://////////////////////////////////////////////
//:: Created By: Preston Watamaniuk
//:: Created On: Jan 8, 2002
//:://////////////////////////////////////////////
#include "prc_inc_spells"

void main()
{
    if(!X2PreSpellCastCode()) return;

    PRCSetSchool(SPELL_SCHOOL_CONJURATION);

    //Declare major variables
    object oCaster = OBJECT_SELF;
    int bAnimalDomain = GetHasFeat(FEAT_ANIMAL_DOMAIN_POWER, oCaster);
    int nMetaMagic = PRCGetMetaMagicFeat();
    int nSwitch = GetPRCSwitch(PRC_SUMMON_ROUND_PER_LEVEL);
    float fDuration = nSwitch == 0 ? HoursToSeconds(24) :
                                     RoundsToSeconds(PRCGetCasterLevel(oCaster) * nSwitch);
    if(nMetaMagic & METAMAGIC_EXTEND)
        fDuration *= 2;

    string sSummon;
    int nVFX;
    switch(GetSpellId())
    {
        case SPELL_SUMMON_CREATURE_I:
            sSummon = bAnimalDomain ? "boardire" : "badgerdire";
            nVFX = VFX_FNF_SUMMON_MONSTER_1;
            break;
        case SPELL_SUMMON_CREATURE_II:
            sSummon = bAnimalDomain ? "wolfdire" : "boardire";
            nVFX = VFX_FNF_SUMMON_MONSTER_1;
            break;
        case SPELL_SUMMON_CREATURE_III:
            if(bAnimalDomain)
            {
                sSummon = "spiddire";
                nVFX = VFX_FNF_SUMMON_MONSTER_2;
            }
            else
            {
                sSummon = "wolfdire";
                nVFX = VFX_FNF_SUMMON_MONSTER_1;
            }
            break;
        case SPELL_SUMMON_CREATURE_IV:
            sSummon = bAnimalDomain ? "beardire" : "spiddire";
            nVFX = VFX_FNF_SUMMON_MONSTER_2;
            break;
        case SPELL_SUMMON_CREATURE_V:
            sSummon = bAnimalDomain ? "diretiger" : "beardire";
            nVFX = VFX_FNF_SUMMON_MONSTER_2;
            break;
        case SPELL_SUMMON_CREATURE_VI:
            if(bAnimalDomain)
            {
                nVFX = VFX_FNF_SUMMON_MONSTER_3;
                int nRoll = d4();
                sSummon = nRoll == 1 ? "airhuge" :
                          nRoll == 2 ? "earthhuge" :
                          nRoll == 3 ? "firehuge" :
                          "waterhuge";
            }
            else
            {
                sSummon = "diretiger";
                nVFX = VFX_FNF_SUMMON_MONSTER_2;
            }
            break;
       case SPELL_SUMMON_CREATURE_VII_AIR:
            sSummon = bAnimalDomain ? "airgreat" : "airhuge";
            nVFX = VFX_FNF_SUMMON_MONSTER_3;
            break;
       case SPELL_SUMMON_CREATURE_VII_EARTH:
            sSummon = bAnimalDomain ? "earthgreat" : "earthhuge";
            nVFX = VFX_FNF_SUMMON_MONSTER_3;
            break;
       case SPELL_SUMMON_CREATURE_VII_FIRE:
            sSummon = bAnimalDomain ? "firegreat" : "firehuge";
            nVFX = VFX_FNF_SUMMON_MONSTER_3;
            break;
       case SPELL_SUMMON_CREATURE_VII_WATER:
            sSummon = bAnimalDomain ? "watergreat" : "waterhuge";
            nVFX = VFX_FNF_SUMMON_MONSTER_3;
            break;
       case SPELL_SUMMON_CREATURE_VIII_AIR:
            sSummon = bAnimalDomain ? "airelder" : "airgreat";
            nVFX = VFX_FNF_SUMMON_MONSTER_3;
            break;
       case SPELL_SUMMON_CREATURE_VIII_EARTH:
            sSummon = bAnimalDomain ? "earthelder" : "earthgreat";
            nVFX = VFX_FNF_SUMMON_MONSTER_3;
            break;
       case SPELL_SUMMON_CREATURE_VIII_FIRE:
            sSummon = bAnimalDomain ? "fireelder" : "firegreat";
            nVFX = VFX_FNF_SUMMON_MONSTER_3;
            break;
       case SPELL_SUMMON_CREATURE_VIII_WATER:
            sSummon = bAnimalDomain ? "waterelder" : "watergreat";
            nVFX = VFX_FNF_SUMMON_MONSTER_3;
            break;
       case SPELL_SUMMON_CREATURE_IX_AIR:
            sSummon = "airelder";
            nVFX = VFX_FNF_SUMMON_MONSTER_3;
            break;
       case SPELL_SUMMON_CREATURE_IX_EARTH:
            sSummon = "earthelder";
            nVFX = VFX_FNF_SUMMON_MONSTER_3;
            break;
       case SPELL_SUMMON_CREATURE_IX_FIRE:
            sSummon = "fireelder";
            nVFX = VFX_FNF_SUMMON_MONSTER_3;
            break;
       case SPELL_SUMMON_CREATURE_IX_WATER:
            sSummon = "waterelder";
            nVFX = VFX_FNF_SUMMON_MONSTER_3;
            break;
    }

    sSummon = GetHasFeat(FEAT_SUMMON_ALIEN, oCaster) ? "pseudo"+sSummon : "nw_s_"+sSummon;
    effect eSummon = EffectSummonCreature(sSummon, nVFX);

    //Apply the VFX impact and summon effect
    MultisummonPreSummon();
    ApplyEffectAtLocation(DURATION_TYPE_TEMPORARY, eSummon, PRCGetSpellTargetLocation(), fDuration);

    DelayCommand(0.5, AugmentSummonedCreature(sSummon, oCaster));

    PRCSetSchool();
}