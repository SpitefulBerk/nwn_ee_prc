//::///////////////////////////////////////////////
//:: Name      Plague of Undead
//:: FileName  sp_plaguedead.nss
//:://////////////////////////////////////////////
/**@file Plague of Undead
Necromancy [Evil]
Level: Clr 9, Dn 9, Wiz 9
Components: V, S
Casting Time: 1 standard action
Range: Close
Effect: Raise Undead
Duration: Permanent

You summon 4 Bone Warriors. These last until they are killed.

Author:    Stratovarius
Created:   5/17/2009
*/
//:://////////////////////////////////////////////
//:://////////////////////////////////////////////

#include "prc_alterations"

void main()
{
    if(!X2PreSpellCastCode()) return;

    PRCSetSchool(SPELL_SCHOOL_NECROMANCY);

    //Declare major variables
    object oCaster = OBJECT_SELF;
    int nCasterLevel = PRCGetCasterLevel(oCaster);
    int nMaxHD = GetLevelByClass(CLASS_TYPE_DREAD_NECROMANCER, oCaster) >= 8 ?
                  nCasterLevel * (4 + GetAbilityModifier(ABILITY_CHARISMA, oCaster)) : nCasterLevel * 4;
    int nTotalHD = GetControlledUndeadTotalHD();
    int nHD = 20;
    //effect eVis = EffectVisualEffect(VFX_FNF_SUMMON_UNDEAD);
    effect eSummon = SupernaturalEffect(EffectSummonCreature("prc_sum_bonewar"));

    MultisummonPreSummon();
    int i;
    for(i = 1; i < 5; i++) // 4 monsters
    {
        nTotalHD += nHD;
        if(nTotalHD <= nMaxHD)
        {
            ApplyEffectAtLocation(DURATION_TYPE_PERMANENT, eSummon, PRCGetSpellTargetLocation());
        }
        else
            FloatingTextStringOnCreature("You cannot create more undead at this time.", oCaster);
        FloatingTextStringOnCreature("Currently have "+IntToString(nTotalHD)+"HD out of "+IntToString(nMaxHD)+"HD.", oCaster);
    }

    PRCSetSchool();
}