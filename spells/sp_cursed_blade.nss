//::///////////////////////////////////////////////
//:: Name      Cursed Blade
//:: FileName  sp_cursed_blade.nss
//:://////////////////////////////////////////////
/**@file Cursed Blade
Necromancy
Level: Assassin 4, Hexblade 4
Components: V
Casting Time: 1 action
Range: Touch
Target: 1 melee weapon
Duration: 1 minute/level
Saving Throw: None
Spell Resistance: No

A weapon affected by this spell deals damage that
cannot be healed in the usual fashion.  Any damage
dealt by the weapon (not including damage from
special weapon properties such as flaming, holy,
wounding, and so on) cannot be cured by any means
until the damaged individual has received a remove
curse spell (or some other effect that neutralizes
a curse).

If a creature is slain by a weapon that is under
the effect of this spell, it can't ve raised from
the dead unless a remove curse spell (or similar
effect) is cast on the body or a true resurrection
spell is used.

               Waiting on new effects system
**/

//////////////////////////////////////////////////
// Author: Tenjac
// Date:   8.9.2006
//////////////////////////////////////////////////