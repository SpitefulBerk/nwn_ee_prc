/////////////////////////////////////////////////////////////////////
//
// Benign Transposition - Swap the caster and target's positions,
// the target must be a member of the caster's party.
//
/////////////////////////////////////////////////////////////////////

#include "prc_inc_spells"
#include "spinc_trans"

void main()
{
	DoTransposition(FALSE);
}
