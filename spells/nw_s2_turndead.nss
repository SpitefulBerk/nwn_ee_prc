//::///////////////////////////////////////////////
//:: Turn Undead
//:: NW_S2_TurnDead
//:: Copyright (c) 2001 Bioware Corp.
//:://////////////////////////////////////////////
/*
    Checks domain powers and class to determine
    the proper turning abilities of the casting
    character.
*/
//:://////////////////////////////////////////////
//:: Created By: Preston Watamaniuk
//:: Created On: Nov 2, 2001
//:: Updated On: Jul 15, 2003 - Georg Zoeller
//:://////////////////////////////////////////////

/*
308  - turn undead
1726 - Turn_Scaleykind
1727 - Turn_Slime
1728 - Turn_Spider
1729 - Turn_Plant
1730 - Turn_Air
1731 - Turn_Earth
1732 - Turn_Fire
1733 - Turn_Water
1734 - Turn_Blightspawned
2259 - turn outsider
*/

#include "prc_inc_domain"
#include "prc_inc_turning"
#include "prc_inc_clsfunc"

void main()
{
    object oCaster = OBJECT_SELF;
    int nTurnType = GetSpellId();

    // Because Turn Undead/Outsider isn't from a domain, skip this check
    if(nTurnType != SPELL_TURN_UNDEAD && nTurnType != SPELL_TURN_OUTSIDER)
    {
        // Used by the uses per day check code for bonus domains
        if(!DecrementDomainUses(GetTurningDomain(nTurnType), oCaster))
            return;
    }

    //casters level for turning
    int nLevel = GetTurningClassLevel(oCaster);

    //check alignment - we will need that later
    int nAlign = GetAlignmentGoodEvil(oCaster);

    //casters charisma modifier
    int nChaMod = GetTurningCharismaMod(oCaster, nTurnType);

    //key values for turning
    int nTurningTotalHD = d6(2) + nLevel + nChaMod;
    int nTurningCheck = d20() + nChaMod;
    int nTurningMaxHD = GetTurningCheckResult(nLevel, nTurningCheck);

    //these stack but dont multiply
    //so use a bonus amount and then add that on later

    //apply maximize turning
    //+100% if good
    if((GetHasFeat(FEAT_MAXIMIZE_TURNING, oCaster)
        && nAlign == ALIGNMENT_GOOD)
        || GetLocalInt(oCaster,"vMaximizeNextTurn"))
    {
        nTurningTotalHD += nTurningTotalHD;
        DeleteLocalInt(oCaster,"vMaximizeNextTurn");
        DecrementRemainingFeatUses(oCaster, FEAT_ML_MAXIMIZE_TURNING);
    }

    //apply empowered turning
    //+50%
    //only if maximize doesnt apply
    else if(GetHasFeat(FEAT_EMPOWER_TURNING, oCaster))
        nTurningTotalHD += nTurningTotalHD/2;

    FloatingTextStringOnCreature("You are turning "+IntToString(nTurningTotalHD)+"HD of creatures whose HD is equal or less than "+IntToString(nTurningMaxHD), oCaster, FALSE);

    // Evil clerics rebuke, not turn, and have a different VFX
    effect eImpactVis = nAlign == ALIGNMENT_EVIL ? EffectVisualEffect(VFX_FNF_LOS_EVIL_30):
                                                   EffectVisualEffect(VFX_FNF_LOS_HOLY_30);

    ApplyEffectAtLocation(DURATION_TYPE_INSTANT, eImpactVis, GetLocation(oCaster));

    //zone of animation effect
    //"You can use a rebuke or command undead attempt to animate
    //corpses within range of your rebuke or command attempt.
    //You animate a total number of HD of undead equal to the
    //number of undead that would be commanded by your result
    //(though you can�t animate more undead than there are available
    //corpses within range). You can�t animate more undead with
    //any single attempt than the maximum number you can command
    //(including any undead already under your command). These
    //undead are automatically under your command, though your
    //normal limit of commanded undead still applies. If the
    //corpses are relatively fresh, the animated undead are zombies.
    //Otherwise, they are skeletons."
    //currently implemented ignoring corpses & what the corpses are of.
    if(GetLocalInt(oCaster, "UsingZoneOfAnimation"))
    {
        //Undead Mastery multiplies total HD by 10
        //non-undead have their HD score multiplied by 10 to compensate
        int bUndeadMastery = GetHasFeat(FEAT_UNDEAD_MASTERY, oCaster);
        if(bUndeadMastery)
            nLevel *= 10;
        //create the effect
        effect eCommand2 = EffectDominated();
        effect eCommand = EffectCutsceneDominated();
        effect eLink = EffectLinkEffects(eCommand, eCommand2);
        eLink = SupernaturalEffect(eLink);
        effect eVFXCom = EffectVisualEffect(VFX_IMP_DOMINATE_S);
        effect eVFX = EffectVisualEffect(VFX_FNF_SUMMON_UNDEAD);
        //keep creating stuff
        int nCommandedTotalHD = GetCommandedTotalHD(nTurnType, bUndeadMastery);
        while(nCommandedTotalHD < nTurningTotalHD)
        {
            location lLoc = GetLocation(oCaster);
            //skeletal blackguard
            string sResRef = "x2_s_bguard_18"; 
            object oCreated = CreateObject(OBJECT_TYPE_CREATURE, sResRef, lLoc);

            int nTargetHD = GetHitDiceForTurning(oCreated, nTurnType);
            //undead mastery only applies to undead
            //so non-undead have thier HD multiplied by 10
            if(MyPRCGetRacialType(oCreated) != RACIAL_TYPE_UNDEAD
                && bUndeadMastery)
                nTargetHD *= 10;

            if(nCommandedTotalHD + nTargetHD <= nLevel)
            {
                ApplyEffectAtLocation(DURATION_TYPE_INSTANT, eVFX, GetLocation(oCreated));
                ApplyEffectToObject(DURATION_TYPE_PERMANENT, eLink, oCreated);
                ApplyEffectToObject(DURATION_TYPE_INSTANT, eVFXCom, oCreated);
                CorpseCrafter(oCaster, oCreated);
                nCommandedTotalHD += nTargetHD;
            }
            //cant be commanded, clean it up
            else
                DestroyObject(oCreated);
        }
        //send feedback
        FloatingTextStringOnCreature("Currently commanding "
            +IntToString(nCommandedTotalHD)
            +"HD out of "+IntToString(nLevel)
            +"HD.", oCaster);
        return;
    }

    //assemble the list of targets to try to turn
    MakeTurningTargetList(nTurningMaxHD, nTurningTotalHD, nTurnType);

    //cycle through target list
    object oTarget = GetTargetListHead(oCaster);
    while(GetIsObjectValid(oTarget))
    {
        DoTurnAttempt(oTarget, nTurningMaxHD, nLevel, nTurnType);
        oTarget = GetTargetListHead(oCaster);
    }
}
