//::///////////////////////////////////////////////
//:: Barbarian Rage
//:: NW_S1_BarbRage
//:: Copyright (c) 2001 Bioware Corp.
//:://////////////////////////////////////////////
/*
    The Str and Con of the Barbarian increases,
    Will Save are +2, AC -2.
    Greater Rage starts at level 15.

    Updated: 2004-01-18 mr_bumpkin: Added bonuses for exceeding +12 stat cap
    Updated: 2004-2-24 by Oni5115: Added Intimidating Rage
*/
//:://////////////////////////////////////////////
//:: Created By: Preston Watamaniuk
//:: Created On: Aug 13, 2001
//:://////////////////////////////////////////////

#include "prc_inc_combat"
#include "inc_addragebonus"
#include "inc_npc"

void main()
{
    object oPC = OBJECT_SELF;

    if(GetHasSpellEffect(SPELL_SPELL_RAGE, oPC))
    {
        IncrementRemainingFeatUses(oPC, FEAT_BARBARIAN_RAGE);
        FloatingTextStringOnCreature("You can't use Barbarian Rage and Spell Rage at the same time.", oPC, FALSE);
        return;
    }

    if(!GetHasFeatEffect(FEAT_BARBARIAN_RAGE, oPC) && !GetHasSpellEffect(GetSpellId(), oPC))
    {
        //Declare major variables
        int nLevel = GetLevelByClass(CLASS_TYPE_BARBARIAN, oPC)
                   + GetLevelByClass(CLASS_TYPE_RUNESCARRED, oPC)
                   + GetLevelByClass(CLASS_TYPE_BATTLERAGER, oPC)
                   + GetLevelByClass(CLASS_TYPE_PRC_EYE_OF_GRUUMSH, oPC)
                   + GetLevelByClass(CLASS_TYPE_RAGE_MAGE, oPC);
        int iStr, iCon, iAC;
        int nSave;

        iAC = 2;

        //Lock: Added compatibility for PRC Mighty Rage ability
        if(nLevel > 14 && (GetHasFeat(FEAT_PRC_EPIC_MIGHT_RAGE, oPC)))
        {
            iStr = 8;
            iCon = 8;
            nSave = 4;
        }
        else if(nLevel > 14)
        {
            iStr = 6;
            iCon = 6;
            nSave = 3;
        }
        else
        {
            iStr = 4;
            iCon = 4;
            nSave = 2;
        }

        // Eye of Gruumsh ability. Additional  +4 Str and -2 AC.
        if(GetHasFeat(FEAT_SWING_BLINDLY, oPC))
        {
            iStr += 4;
            iAC += 2;
        }

        // +2 Str/Con -2 AC
        if(GetHasFeat(FEAT_RECKLESS_RAGE, oPC))
        {
            iStr += 2;
            iCon += 2;
            iAC += 2;
        }

        // play a random voice chat instead of just VOICE_CHAT_BATTLECRY1
        int iVoice = d3(1);
        switch(iVoice)
        {
             case 1: iVoice = VOICE_CHAT_BATTLECRY1; break;
             case 2: iVoice = VOICE_CHAT_BATTLECRY2; break;
             case 3: iVoice = VOICE_CHAT_BATTLECRY3; break;
        }
        PlayVoiceChat(iVoice);

        //Determine the duration by getting the con modifier after being modified
        int nCon = 3 + GetAbilityModifier(ABILITY_CONSTITUTION) + iCon;
        effect eLink = EffectVisualEffect(VFX_DUR_CESSATE_POSITIVE);
               eLink = EffectLinkEffects(eLink, EffectAbilityIncrease(ABILITY_CONSTITUTION, iCon));
               eLink = EffectLinkEffects(eLink, EffectAbilityIncrease(ABILITY_STRENGTH, iStr));
               eLink = EffectLinkEffects(eLink, EffectSavingThrowIncrease(SAVING_THROW_WILL, nSave));
               eLink = EffectLinkEffects(eLink, EffectACDecrease(iAC, AC_DODGE_BONUS));

        // Blazing Berserker
        if(GetHasFeat(FEAT_BLAZING_BERSERKER, oPC))
        {
               eLink = EffectLinkEffects(eLink, EffectVisualEffect(VFX_DUR_ELEMENTAL_SHIELD));
               eLink = EffectLinkEffects(eLink, EffectDamageImmunityDecrease(DAMAGE_TYPE_COLD, 50));
               eLink = EffectLinkEffects(eLink, EffectDamageImmunityIncrease(DAMAGE_TYPE_FIRE, 100));
        }
        // Frozen Berserker
        if(GetHasFeat(FEAT_FROZEN_BERSERKER, oPC))
        {
               eLink = EffectLinkEffects(eLink, EffectVisualEffect(VFX_DUR_ELEMENTAL_SHIELD));
               eLink = EffectLinkEffects(eLink, EffectDamageImmunityDecrease(DAMAGE_TYPE_FIRE, 50));
               eLink = EffectLinkEffects(eLink, EffectDamageImmunityIncrease(DAMAGE_TYPE_COLD, 100));
        }

        //Make effect extraordinary
        eLink = ExtraordinaryEffect(eLink);
        effect eVis = EffectVisualEffect(VFX_IMP_IMPROVE_ABILITY_SCORE); //Change to the Rage VFX

        SignalEvent(oPC, EventSpellCastAt(oPC, SPELLABILITY_BARBARIAN_RAGE, FALSE));

        if(nCon > 0)
        {
            // 2004-01-18 mr_bumpkin: determine the ability scores before adding bonuses, so the values
            // can be read in by the GiveExtraRageBonuses() function below.
            int StrBeforeBonuses = GetAbilityScore(oPC, ABILITY_STRENGTH);
            int ConBeforeBonuses = GetAbilityScore(oPC, ABILITY_CONSTITUTION);
            nCon += GetHasFeat(FEAT_EXTENDED_RAGE, oPC) ? 5 : 0;
            float fDuration = RoundsToSeconds(nCon);

            //Apply the VFX impact and effects
            ApplyEffectToObject(DURATION_TYPE_TEMPORARY, eLink, oPC, fDuration);
            ApplyEffectToObject(DURATION_TYPE_INSTANT, eVis, oPC);

            // Shared Fury
            if(GetHasFeat(FEAT_SHARED_FURY, oPC))
            {
                object oComp = GetFirstObjectInShape(SHAPE_SPHERE, RADIUS_SIZE_MEDIUM, GetLocation(oPC), TRUE, OBJECT_TYPE_CREATURE);
                while(GetIsObjectValid(oComp))
                {
                    if(GetMasterNPC(oComp) == oPC && GetAssociateTypeNPC(oComp) == ASSOCIATE_TYPE_ANIMALCOMPANION)
                    {
                        ApplyEffectToObject(DURATION_TYPE_TEMPORARY, eLink, oComp, fDuration);
                        ApplyEffectToObject(DURATION_TYPE_INSTANT, eVis, oComp);
                    }
                    oComp = GetNextObjectInShape(SHAPE_SPHERE, RADIUS_SIZE_MEDIUM, GetLocation(oPC), TRUE, OBJECT_TYPE_CREATURE);
                }
            }

            // 2004-2-24 Oni5115: Intimidating Rage
            if(GetHasFeat(FEAT_INTIMIDATING_RAGE, oPC))
            {
                //Finds nearest visible enemy within 30 ft.
                object oTarget = GetNearestCreature(CREATURE_TYPE_REPUTATION, REPUTATION_TYPE_ENEMY, oPC, 1, CREATURE_TYPE_PERCEPTION, PERCEPTION_SEEN);
                if(GetDistanceBetween(oPC, oTarget) < FeetToMeters(30.0))
                {
                    // Will save DC 10 + 1/2 Char level + Cha mod
                    int saveDC = 10 + (GetHitDice(oPC)/2) + GetAbilityModifier(ABILITY_CHARISMA, oPC);
                    int nResult = WillSave(oTarget, saveDC, SAVING_THROW_TYPE_NONE);

                    if(!nResult)
                    {
                        // Same effect as Doom Spell
                        ApplyEffectToObject(DURATION_TYPE_TEMPORARY, EffectShaken(), oTarget, TurnsToSeconds(nCon));
                        ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_DOOM), oTarget);
                    }
                }
            }

            // Terrifying Rage
            if(GetHasFeat(FEAT_EPIC_TERRIFYING_RAGE, oPC))
            {
                effect eAOE = EffectAreaOfEffect(AOE_MOB_FEAR, "x2_s2_terrage_A");
                ApplyEffectToObject(DURATION_TYPE_TEMPORARY, eAOE, oPC, fDuration);
            }

            // Thundering Rage
            if(GetHasFeat(FEAT_EPIC_THUNDERING_RAGE, oPC))
            {
                object oWeapon = GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oPC);
                if(GetIsObjectValid(oWeapon))
                {
                    IPSafeAddItemProperty(oWeapon, ItemPropertyMassiveCritical(IP_CONST_DAMAGEBONUS_2d6), fDuration, X2_IP_ADDPROP_POLICY_KEEP_EXISTING,TRUE,TRUE);
                    IPSafeAddItemProperty(oWeapon, ItemPropertyVisualEffect(ITEM_VISUAL_SONIC), fDuration, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING,FALSE,TRUE);
                    IPSafeAddItemProperty(oWeapon, ItemPropertyOnHitProps(IP_CONST_ONHIT_DEAFNESS,IP_CONST_ONHIT_SAVEDC_20,IP_CONST_ONHIT_DURATION_25_PERCENT_3_ROUNDS), fDuration, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING,FALSE,TRUE);
                }

                oWeapon = GetItemInSlot(INVENTORY_SLOT_LEFTHAND, oPC);
                if(GetIsObjectValid(oWeapon))
                {
                    IPSafeAddItemProperty(oWeapon, ItemPropertyMassiveCritical(IP_CONST_DAMAGEBONUS_2d6), fDuration, X2_IP_ADDPROP_POLICY_KEEP_EXISTING,TRUE,TRUE);
                    IPSafeAddItemProperty(oWeapon,ItemPropertyVisualEffect(ITEM_VISUAL_SONIC), fDuration, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING,FALSE,TRUE);
                }
            }

            // 2004-01-18 mr_bumpkin: Adds special bonuses to those barbarians who are restricted by the
            // +12 attribute bonus cap, to make up for them. :)
            // The delay is because you have to delay the command if you want the function to be able
            // to determine what the ability scores become after adding the bonuses to them.
            DelayCommand(0.1, GiveExtraRageBonuses(nCon, StrBeforeBonuses, ConBeforeBonuses, iStr, iCon, nSave, DAMAGE_TYPE_BASE_WEAPON, oPC));

        }
    }
}
