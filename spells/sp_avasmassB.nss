//::///////////////////////////////////////////////
//:: Avascular Mass B: On Exit
//:: sp_avasmassB.nss
//:://////////////////////////////////////////////
/*
    Removes the entangle effect after the AOE dies.
*/
//:://////////////////////////////////////////////
//:: Created By: Tenjac
//:: Created On: 11/15/05
//:://////////////////////////////////////////////

#include "prc_inc_spells"

#include "prc_add_spell_dc"

void main()
{
SetLocalInt(OBJECT_SELF, "X2_L_LAST_SPELLSCHOOL_VAR", SPELL_SCHOOL_NECROMANCY);
    SetAllAoEInts(SPELL_AVASCULAR_MASS,OBJECT_SELF, GetSpellSaveDC());

    //Declare major variables
    //Get the object that is exiting the AOE
    object oTarget = GetExitingObject();
    effect eAOE;
    if(GetHasSpellEffect(SPELL_AVASCULAR_MASS, oTarget))
    {
        //Search through the valid effects on the target.
        eAOE = GetFirstEffect(oTarget);
        while (GetIsEffectValid(eAOE))
        {
            if (GetEffectCreator(eAOE) == GetAreaOfEffectCreator())
            {
                if(GetEffectType(eAOE) == EFFECT_TYPE_ENTANGLE)
                {
                    //If the effect was created by the Entagle then remove it
                    if(GetEffectSpellId(eAOE) == SPELL_AVASCULAR_MASS)
                    {
                        RemoveEffect(oTarget, eAOE);
                    }
                }
            }
            //Get next effect on the target
            eAOE = GetNextEffect(oTarget);
        }
    }


DeleteLocalInt(OBJECT_SELF, "X2_L_LAST_SPELLSCHOOL_VAR");
// Getting rid of the local integer storing the spellschool name
}

