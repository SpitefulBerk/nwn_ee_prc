/////////////////////////////////////////////////////////////////////
//
// Baleful Transposition - Swap the caster and target's posistions,
// hostile targets get SR and a will save to negate.
//
/////////////////////////////////////////////////////////////////////

#include "prc_inc_spells"
#include "spinc_trans"

void main()
{
	DoTransposition(TRUE);
}
