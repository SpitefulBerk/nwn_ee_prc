//::///////////////////////////////////////////////
//:: NW_S3_Alcohol.nss
//:: Copyright (c) 2001 Bioware Corp.
//:://////////////////////////////////////////////
/*
  Makes beverages fun.
  May 2002: Removed fortitude saves. Just instant intelligence loss
*/
//:://////////////////////////////////////////////
//:: Created By:   Brent
//:: Created On:   February 2002
//:://////////////////////////////////////////////

//:://////////////////////////////////////////////
//:: Modified by Jeremiah Teague for
//:: Drunken Master Prestige Class
//:://////////////////////////////////////////////

#include "prc_inc_clsfunc"

void main()
{
    object oTarget = PRCGetSpellTargetObject();
    int nDrunkenMaster = GetLevelByClass(CLASS_TYPE_DRUNKEN_MASTER, oTarget);
    string sScript;

    switch(nDrunkenMaster)
    {
        case 0:
        {
            MakeDrunk(oTarget, 3);
            break;
        }
        case 1:
        case 2:
        case 3:
        case 4:
        {
            sScript = "prc_dm_drnkdemon";
            break;
        }
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        {
            sScript = "prc_dm_drnknrage";
            break;
        }
    }

    if(sScript != "")
    {
        ExecuteScript(sScript, oTarget);
        DrunkenMasterSpeakString(oTarget);
        DrunkenMasterCreateEmptyBottle(oTarget, GetSpellId());
    }
}
