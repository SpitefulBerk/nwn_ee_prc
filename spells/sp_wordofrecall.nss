//::///////////////////////////////////////////////
//:: Word of Recall
//:: sp_wordofrecall.nss
//:://////////////////////////////////////////////
/*
Conjuration (Teleportation)
Level:            Clr 6, Drd 8
Components:       V
Casting Time:     1 standard action
Range:            Unlimited
Target:           You and touched objects or other
                  willing creatures
Duration:         Instantaneous
Saving Throw:     None or Will negates (harmless, object)
Spell Resistance: No or Yes (harmless, object)

Word of recall teleports you instantly back to
your sanctuary when the word is uttered. You must
designate the sanctuary when you prepare the spell,
and it must be a very familiar place. The actual
point of arrival is a designated area no larger
than 10 feet by 10 feet. You can be transported any
distance within a plane but cannot travel between
planes. You can transport, in addition to yourself,
any objects you carry, as long as their weight
doesn�t exceed your maximum load. You may also
bring one additional willing Medium or smaller
creature (carrying gear or objects up to its
maximum load) or its equivalent per three caster
levels. A Large creature counts as two Medium
creatures, a Huge creature counts as two Large
creatures, and so forth. All creatures to be
transported must be in contact with one another,
and at least one of those creatures must be in
contact with you. Exceeding this limit causes the
spell to fail.

An unwilling creature can�t be teleported by word
of recall. Likewise, a creature�s Will save (or
spell resistance) prevents items in its possession
from being teleported. Unattended, nonmagical
objects receive no saving throw.
 */
//:://////////////////////////////////////////////

#include "spinc_teleport"

void main()
{
    if(!X2PreSpellCastCode()) return;

    PRCSetSchool(SPELL_SCHOOL_CONJURATION);

    object oCaster = OBJECT_SELF;
    object oTarget = PRCGetSpellTargetObject();

    if(oTarget == oCaster)
    {
        int nCasterLvl = PRCGetCasterLevel(oCaster);
        int nSpellID = GetSpellId();

        Teleport(oCaster, nCasterLvl, nSpellID == SPELL_WORD_OF_RECALL_PARTY, TRUE, "");
    }
    else
    {
        location lTarget = PRCGetSpellTargetLocation();
        SetLocalMetalocation(oCaster, "PRC_WordOfRecall", LocationToMetalocation(lTarget));
        FloatingTextStrRefOnCreature(16789978, oCaster, FALSE);
    }

    PRCSetSchool();
}