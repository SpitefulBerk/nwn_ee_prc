//:://////////////////////////////////////////////////
//:: NW_C2_DEFAULT7
/*
  Default OnDeath event handler for NPCs.

  Adjusts killer's alignment if appropriate and
  alerts allies to our death.
 */
//:://////////////////////////////////////////////////
//:: Copyright (c) 2002 Floodgate Entertainment
//:: Created By: Naomi Novik
//:: Created On: 12/22/2002
//:://////////////////////////////////////////////////
//:://////////////////////////////////////////////////
//:: Modified By: Deva Winblood
//:: Modified On: April 1st, 2008
//:: Added Support for Dying Wile Mounted
//:://///////////////////////////////////////////////

const string sHenchSummonedFamiliar = "HenchSummonedFamiliar";
const string sHenchSummonedAniComp  = "HenchSummonedAniComp";
const string sHenchLastHeardOrSeen  = "LastSeenOrHeard";

#include "x2_inc_compon"
#include "x0_i0_spawncond"

// Clears the last unheard, unseen enemy location
void ClearEnemyLocation(object oSelf);

void main()
{
    object oSelf = OBJECT_SELF;
    object oKiller = GetLastKiller();
    object oMaster = GetMaster(oSelf);

    int nAlign = GetAlignmentGoodEvil(oSelf);

    if(GetLocalInt(GetModule(), "X3_ENABLE_MOUNT_DB") && GetIsObjectValid(oMaster))
        SetLocalInt(oMaster, "bX3_STORE_MOUNT_INFO", TRUE);

    // If we're a good/neutral commoner,
    // adjust the killer's alignment evil
    if(GetLevelByClass(CLASS_TYPE_COMMONER, oSelf)
    && (nAlign == ALIGNMENT_GOOD || nAlign == ALIGNMENT_NEUTRAL))
    {
        AdjustAlignment(oKiller, ALIGNMENT_EVIL, 5);
    }

//Start Hench AI
    if(GetLocalInt(oSelf, "GaveHealing"))
    {
        // Pausanias: destroy potions of healing
        object oItem = GetFirstItemInInventory();
        while(GetIsObjectValid(oItem))
        {
            if(GetTag(oItem) == "NW_IT_MPOTION003")
                DestroyObject(oItem);
            oItem = GetNextItemInInventory();
        }
    }

    if(GetLocalInt(oSelf, sHenchSummonedFamiliar))
    {
        object oFam = GetLocalObject(oSelf, sHenchSummonedFamiliar);
        if(GetIsObjectValid(oFam))
        {
            //if(DEBUG) DoDebug(GetName(oSelf) + " destroy familiar");
            DestroyObject(oFam, 0.1);
            ApplyEffectAtLocation(DURATION_TYPE_TEMPORARY, EffectVisualEffect(VFX_IMP_UNSUMMON), GetLocation(oFam));
        }
    }
    if(GetLocalInt(oSelf, sHenchSummonedAniComp))
    {
        object oAni = GetLocalObject(oSelf, sHenchSummonedAniComp);
        if (GetIsObjectValid(oAni))
        {
            //if(DEBUG) DoDebug(GetName(oSelf) + " destroy ani comp");
            DestroyObject(oAni, 0.1);
            ApplyEffectAtLocation(DURATION_TYPE_TEMPORARY, EffectVisualEffect(VFX_IMP_UNSUMMON), GetLocation(oAni));
        }
    }

    ClearEnemyLocation(oSelf);
//End Hench AI

    // Call to allies to let them know we're dead
    SpeakString("NW_I_AM_DEAD", TALKVOLUME_SILENT_TALK);

    //Shout Attack my target, only works with the On Spawn In setup
    SpeakString("NW_ATTACK_MY_TARGET", TALKVOLUME_SILENT_TALK);

    // NOTE: the OnDeath user-defined event does not
    // trigger reliably and should probably be removed
    if(GetSpawnInCondition(NW_FLAG_DEATH_EVENT))
         SignalEvent(oSelf, EventUserDefined(1007));

    craft_drop_items(oKiller);

    ExecuteScript("prc_npc_death", oSelf);
    ExecuteScript("prc_pwondeath", oSelf);
}

void ClearEnemyLocation(object oSelf)
{
    DeleteLocalInt(oSelf, sHenchLastHeardOrSeen);
    DeleteLocalLocation(oSelf, sHenchLastHeardOrSeen);

    object oInvisTarget = GetLocalObject(oSelf, sHenchLastHeardOrSeen);
    if (GetIsObjectValid(oInvisTarget))
    {
        DestroyObject(oInvisTarget);
        DeleteLocalObject(oSelf, sHenchLastHeardOrSeen);
    }
}