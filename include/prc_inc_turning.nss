//::///////////////////////////////////////////////
//:: Turn undead include
//:: prc_inc_turning
//::///////////////////////////////////////////////
/** @file
    Defines functions that seem to have something
    to do with Turn Undead (and various other
    stuff).
*/
//:://////////////////////////////////////////////
//:://////////////////////////////////////////////

//////////////////////////////////////////////////
/*             Function prototypes              */
//////////////////////////////////////////////////

//gets the number of class levels that count for turning
int GetTurningClassLevel(object oCaster, int bUndeadOnly = FALSE);

//Returns the turning charisma modifier for this caster
int GetTurningCharismaMod(object oCaster, int nTurnType);

//this adjusts the highest HD of undead turned
int GetTurningCheckResult(int nLevel, int nTurningCheck);

//this creates the list of targets that are affected
//use the inc_target_list functions
void MakeTurningTargetList(int nTurningMaxHD, int nTurningTotalHD, int nTurnType);

//tests if the target can be turned by self
//includes race, domains, etc.
int GetCanTurn(object oTarget, int nTurnType);

//gets the equivalent HD total for turning purposes
//includes turn resistance and SR for outsiders
int GetHitDiceForTurning(object oTarget, int nTurnType);

//the main turning function once targets have been listed
//routs to turn/destroy/rebuke/command as appropaite
void DoTurnAttempt(object oTarget, int nTurningMaxHD, int nLevel, int nTurnType);

int GetCommandedTotalHD(int nTurnType = SPELL_TURN_UNDEAD, int bUndeadMastery = FALSE);

//various sub-turning effect funcions

void DoTurn(object oTarget);
void DoDestroy(object oTarget);
void DoRebuke(object oTarget);
void DoCommand(object oTarget, int nLevel, int nTurnType, int bUndeadMastery);

// This uses the turn type to determine whether the target should be turned or rebuked
// Used by Reptile, Air, Earth, Fire, Water, Spider domains
// Called from GetIsTurnNotRebuke
// TRUE = Turn
// FALSE = Rebuke
int GetTurnTargets(object oTarget, int nTurnType);

// What this does is check to see if there is an discrete word
// That matches the input, and if there is returns TRUE
int CheckTargetName(object oTarget, string sName);

int GetIsAirCreature(object oCreature, int nAppearance);
int GetIsEarthCreature(object oCreature, int nAppearance);
int GetIsFireCreature(object oCreature, int nAppearance);
int GetIsWaterCreature(object oCreature, int nAppearance);
int GetIsReptile(object oCreature, int nAppearance);
int GetIsSpider(object oCreature, int nAppearance);

//////////////////////////////////////////////////
/*                  Includes                    */
//////////////////////////////////////////////////

#include "prc_class_const"
#include "prc_feat_const"
#include "inc_utility"
#include "prc_inc_racial"

//////////////////////////////////////////////////
/*             Function definitions             */
//////////////////////////////////////////////////

//private function
//only used by DoTurnAttempt
//TRUE  == Turn
//FALSE == Rebuke
int GetIsTurnNotRebuke(object oTarget, int nTurnType)
{
    object oPC = OBJECT_SELF;
    int nTargetRace = MyPRCGetRacialType(oTarget);
    int nTargetAppearance = GetAppearanceType(oTarget);

    if(nTurnType == SPELL_TURN_UNDEAD)
    {
        if(nTargetRace == RACIAL_TYPE_UNDEAD)
        {
            // Evil clerics rebuke undead, otherwise turn
            // Dread Necro, True Necro and Master of Shrouds always rebuke or command
            if(GetAlignmentGoodEvil(oPC) == ALIGNMENT_EVIL
            || GetLevelByClass(CLASS_TYPE_DREAD_NECROMANCER, oPC)
            || GetLevelByClass(CLASS_TYPE_TRUENECRO, oPC)
            || GetLevelByClass(CLASS_TYPE_MASTER_OF_SHROUDS, oPC))
                return FALSE;

            return TRUE;
        }
        else if(nTargetRace == RACIAL_TYPE_OUTSIDER && GetHasFeat(FEAT_EPIC_PLANAR_TURNING))
        {
            // Evil clerics turn non-evil outsiders, and rebuke evil outsiders
            if(GetAlignmentGoodEvil(oPC) == ALIGNMENT_EVIL)
            {
                if(GetAlignmentGoodEvil(oTarget) == ALIGNMENT_EVIL)
                    return FALSE;
                return TRUE;
            }
            // Good clerics turn non-good outsiders, and rebuke good outsiders
            if(GetAlignmentGoodEvil(oTarget) == ALIGNMENT_GOOD)
                return FALSE;
            return TRUE;
        }
    }
    else if(nTurnType == SPELL_TURN_OUTSIDER && nTargetRace == RACIAL_TYPE_OUTSIDER)
    {
        // Evil clerics turn non-evil outsiders, and rebuke evil outsiders
        if(GetAlignmentGoodEvil(oPC) == ALIGNMENT_EVIL)
        {
            if(GetAlignmentGoodEvil(oTarget) == ALIGNMENT_EVIL)
                return FALSE;
            return TRUE;
        }
        // Good clerics turn non-good outsiders, and rebuke good outsiders
        if(GetAlignmentGoodEvil(oTarget) == ALIGNMENT_GOOD)
            return FALSE;
        return TRUE;
    }
    else if(nTurnType == SPELL_TURN_BLIGHTSPAWNED)
    {
        // Rebuke/Command evil animals
        if(nTargetRace == RACIAL_TYPE_ANIMAL && GetAlignmentGoodEvil(oTarget) == ALIGNMENT_EVIL)
        {
            return FALSE;
        }
        // Rebuke/Command blightspawned, either by tag from Blight touch
        // Or from having a Blightlord master
        if (GetTag(oTarget) == "prc_blightspawn" || GetLevelByClass(CLASS_TYPE_BLIGHTLORD, GetMaster(oTarget)) > 0)
        {
            return FALSE;
        }
        // Rebuke/Command evil plants
        if(nTargetRace == RACIAL_TYPE_PLANT && GetAlignmentGoodEvil(oTarget) == ALIGNMENT_EVIL)
        {
            return FALSE;
        }
        // This should never trigger, but oh well
        return TRUE;
    }
    // Slime domain rebukes or commands oozes
    else if(nTurnType == SPELL_TURN_OOZE && nTargetRace == RACIAL_TYPE_OOZE)
    {
        return FALSE;
    }
    // Plant domain rebukes or commands plants
    /*else if(nTargetRace == RACIAL_TYPE_PLANT && nTurnType == SPELL_TURN_PLANT)
    {
        return FALSE;
    }*/
    else if(nTurnType == SPELL_TURN_AIR)
    {
        if(GetIsAirCreature(oTarget, nTargetAppearance))
            return FALSE;

        if(GetIsEarthCreature(oTarget, nTargetAppearance))
            return TRUE;

        return TRUE;
    }
    else if(nTurnType == SPELL_TURN_EARTH)
    {
        if(GetIsAirCreature(oTarget, nTargetAppearance))
            return TRUE;

        if(GetIsEarthCreature(oTarget, nTargetAppearance))
            return FALSE;

        return TRUE;
    }
    else if(nTurnType == SPELL_TURN_FIRE)
    {
        if(GetIsFireCreature(oTarget, nTargetAppearance))
            return FALSE;

        if(GetIsWaterCreature(oTarget, nTargetAppearance))
            return TRUE;

        return TRUE;
    }
    else if(nTurnType == SPELL_TURN_WATER)
    {
        if(GetIsFireCreature(oTarget, nTargetAppearance))
            return TRUE;

        if(GetIsWaterCreature(oTarget, nTargetAppearance))
            return FALSE;

        return TRUE;
    }
    else if(nTurnType == SPELL_TURN_REPTILE)
    {
        if(GetIsReptile(oTarget, nTargetAppearance))
            return FALSE;
    }
    else if(nTurnType == SPELL_TURN_SPIDER)
    {
        if(GetIsSpider(oTarget, nTargetAppearance))
            return FALSE;
    }

    return TRUE;
}

void DoTurnAttempt(object oTarget, int nTurningMaxHD, int nLevel, int nTurnType)
{
    object oPC = OBJECT_SELF;

    //signal the event
    SignalEvent(oTarget, EventSpellCastAt(oPC, nTurnType));

    //sun domain
    //destroys instead of turn/rebuke/command
    //useable once per day only
    if(GetLocalInt(oPC, "UsingSunDomain"))
    {
        DoDestroy(oTarget);
    }
    else
    {
        int nTargetHD = GetHitDiceForTurning(oTarget, nTurnType);
        if(GetIsTurnNotRebuke(oTarget, nTurnType)
        || GetLevelByClass(CLASS_TYPE_BAELNORN, oTarget))//always turn baelnorns
        {
            //if half of level, destroy
            //otherwise turn
            if(nTargetHD < nLevel/2)    DoDestroy(oTarget);
            else                        DoTurn(oTarget);
        }
        else
        {
            //if half of level, command
            //otherwise rebuke
            int bUndeadMastery = GetHasFeat(FEAT_UNDEAD_MASTERY);
            if(nTargetHD < nLevel/2)    DoCommand(oTarget, nLevel, nTurnType, bUndeadMastery);
            else                        DoRebuke(oTarget);
            int nCommandLevel = nLevel;
            if(bUndeadMastery)
                nCommandLevel *= 10;
            FloatingTextStringOnCreature("Currently commanding "
                +IntToString(GetCommandedTotalHD())
                +"HD out of "+IntToString(nCommandLevel)
                +"HD.", oPC);
        }
    }

    // Check for Exalted Turning
    // take 3d6 damage if they do
    // Only works on undead
    if (GetHasFeat(FEAT_EXALTED_TURNING) && 
        GetAlignmentGoodEvil(oPC) == ALIGNMENT_GOOD &&
        MyPRCGetRacialType(oTarget) == RACIAL_TYPE_UNDEAD)
    {
        effect eDamage = EffectDamage(d6(3), DAMAGE_TYPE_DIVINE);
        ApplyEffectToObject(DURATION_TYPE_INSTANT, eDamage, oTarget);
    }
}

void DoTurn(object oTarget)
{
    //create the effect
    effect eTurn = EffectTurned();
    eTurn = EffectLinkEffects(eTurn, EffectVisualEffect(VFX_DUR_MIND_AFFECTING_FEAR));
    eTurn = EffectLinkEffects(eTurn, EffectVisualEffect(VFX_DUR_CESSATE_NEGATIVE));
    //apply the effect for 60 seconds
    ApplyEffectToObject(DURATION_TYPE_TEMPORARY, eTurn, oTarget, 60.0);
}

void DoDestroy(object oTarget)
{
    //create the effect
    //supernatural so it penetrates immunity to death
    effect eDestroy = SupernaturalEffect(EffectDeath());
    //apply the effect
    ApplyEffectToObject(DURATION_TYPE_INSTANT, eDestroy, oTarget);
}

void DoRebuke(object oTarget)
{
    //rebuke effect
    //The character is frozen in fear and can take no actions.
    //A cowering character takes a -2 penalty to Armor Class and loses
    //her Dexterity bonus (if any).
    //create the effect
    effect eRebuke = EffectEntangle(); //this removes dex bonus
    eRebuke = EffectLinkEffects(eRebuke, EffectACDecrease(2));
    eRebuke = EffectLinkEffects(eRebuke, EffectVisualEffect(VFX_DUR_MIND_AFFECTING_FEAR));
    eRebuke = EffectLinkEffects(eRebuke, EffectVisualEffect(VFX_DUR_CESSATE_NEGATIVE));
    //apply the effect for 60 seconds
    ApplyEffectToObject(DURATION_TYPE_TEMPORARY, eRebuke, oTarget, 60.0);
    //handle unable to take actions
    AssignCommand(oTarget, ClearAllActions());
    AssignCommand(oTarget, DelayCommand(60.0, SetCommandable(TRUE)));
    AssignCommand(oTarget, SetCommandable(FALSE));
}

void DoCommand(object oTarget, int nLevel, int nTurnType, int bUndeadMastery)
{
    //Undead Mastery multiplies total HD by 10
    //non-undead have their HD score multiplied by 10 to compensate
    if(bUndeadMastery)
        nLevel *= 10;

    int nCommandedTotalHD = GetCommandedTotalHD();

    int nTargetHD = GetHitDiceForTurning(oTarget, nTurnType);
    //undead mastery only applies to undead
    //so non-undead have thier HD multiplied by 10
    if(MyPRCGetRacialType(oTarget) != RACIAL_TYPE_UNDEAD
        && bUndeadMastery)
        nTargetHD *= 10;

    if(nCommandedTotalHD + nTargetHD <= nLevel)
    {
        //create the effect
        //supernatural dominated vs cutscenedominated
        //supernatural will last over resting
        //Why not use both?
        effect eCommand2 = EffectDominated();
        effect eCommand = EffectCutsceneDominated();
        effect eLink = EffectLinkEffects(eCommand, eCommand2);
        eLink = SupernaturalEffect(eLink);
        effect eVFXCom = EffectVisualEffect(VFX_IMP_DOMINATE_S);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, eLink, oTarget);
        ApplyEffectToObject(DURATION_TYPE_INSTANT, eVFXCom, oTarget);
    }
    //not enough commanding power left
    //rebuke instead
    else
        DoRebuke(oTarget);
}


void MakeTurningTargetList(int nTurningMaxHD, int nTurningTotalHD, int nTurnType)
{
    int nHDCount;
    int i = 1;
    object oCaster = OBJECT_SELF;
    object oTest = GetNearestObject(OBJECT_TYPE_CREATURE, oCaster, i);
    while(GetIsObjectValid(oTest)
        && GetDistanceToObject(oTest) < FeetToMeters(60.0)
        && nHDCount <= nTurningTotalHD)
    {
        int nTargetHD = GetHitDiceForTurning(oTest, nTurnType);
        if(GetCanTurn(oTest, nTurnType)
            && nTargetHD <= nTurningMaxHD
            && nHDCount+nTargetHD <= nTurningTotalHD)
        {
            AddToTargetList(oTest, oCaster, INSERTION_BIAS_DISTANCE);
            nHDCount += nTargetHD;
        }

        //move to next test
        i++;
        oTest = GetNearestObject(OBJECT_TYPE_CREATURE, oCaster, i);
    }
}

int GetCanTurn(object oTarget, int nTurnType)
{
    object oPC = OBJECT_SELF;

    //is not an enemy
    if(!spellsIsTarget(oTarget, SPELL_TARGET_SELECTIVEHOSTILE, oPC))
        return FALSE;

    //already turned
    //NOTE: At the moment this breaks down in "turning conflicts" where clerics try to
    //turn each others undead. Fix later.
    //if(GetHasSpellEffect(nTurnType, oTarget))
    //    return FALSE;
    if(PRCGetHasEffect(EFFECT_TYPE_TURNED, oTarget))
        return FALSE;

    //check turning/rebuking immunity
    if(GetIsTurnNotRebuke(oTarget, nTurnType))
    {
        if(GetHasFeat(FEAT_TURNING_IMMUNITY, oTarget))
            return FALSE;
    }
    else if(GetHasFeat(FEAT_IMMUNITY_TO_REBUKING, oTarget))
        return FALSE;

    int nTargetRace = MyPRCGetRacialType(oTarget);
    int nTargetAppearance = GetAppearanceType(oTarget);

    if(nTurnType == SPELL_TURN_UNDEAD)
    {
        if(nTargetRace == RACIAL_TYPE_UNDEAD
        || (nTargetRace == RACIAL_TYPE_OUTSIDER && GetHasFeat(FEAT_EPIC_PLANAR_TURNING, oPC)))
            return TRUE;
    }
    else if(nTurnType == SPELL_TURN_OUTSIDER)
        return nTargetRace == RACIAL_TYPE_OUTSIDER;
    else if(nTurnType == SPELL_TURN_BLIGHTSPAWNED)
    {
        // Rebuke/Command evil animals
        if(nTargetRace == RACIAL_TYPE_ANIMAL && GetAlignmentGoodEvil(oTarget) == ALIGNMENT_EVIL)
        {
            return TRUE;
        }
        // Rebuke/Command blightspawned, either by tag from Blight touch
        // Or from having a Blightlord master
        if (GetTag(oTarget) == "prc_blightspawn" || GetLevelByClass(CLASS_TYPE_BLIGHTLORD, GetMaster(oTarget)) > 0)
        {
            return TRUE;
        }
        // Rebuke/Command evil plants
        /*if(nTargetRace == RACIAL_TYPE_PLANT && GetAlignmentGoodEvil(oTarget) == ALIGNMENT_EVIL)
        {
            return TRUE;
        }*/
    }
    // Slime domain rebukes or commands oozes
    else if(nTurnType == SPELL_TURN_OOZE)
        return nTargetRace == RACIAL_TYPE_OOZE;
    // Plant domain rebukes or commands plants
    else if(nTurnType == SPELL_TURN_PLANT)
        return nTargetRace == RACIAL_TYPE_PLANT;
    else if(nTurnType == SPELL_TURN_AIR || nTurnType == SPELL_TURN_EARTH)
        return GetIsAirCreature(oTarget, nTargetAppearance) || GetIsEarthCreature(oTarget, nTargetAppearance);
    else if(nTurnType == SPELL_TURN_FIRE || nTurnType == SPELL_TURN_WATER)
        return GetIsFireCreature(oTarget, nTargetAppearance) || GetIsWaterCreature(oTarget, nTargetAppearance);
    else if(nTurnType == SPELL_TURN_REPTILE)
        return GetIsReptile(oTarget, nTargetAppearance);
    else if(nTurnType == SPELL_TURN_SPIDER)
        return GetIsSpider(oTarget, nTargetAppearance);

    return FALSE;
}

int GetTurningCheckResult(int nLevel, int nTurningCheck)
{
    switch(nTurningCheck)
    {
        case  0:
            return nLevel-4;
        case  1:
        case  2:
        case  3:
            return nLevel-3;
        case  4:
        case  5:
        case  6:
            return nLevel-2;
        case  7:
        case  8:
        case  9:
            return nLevel-1;
        case 10:
        case 11:
        case 12:
            return nLevel;
        case 13:
        case 14:
        case 15:
            return nLevel+1;
        case 16:
        case 17:
        case 18:
            return nLevel+2;
        case 19:
        case 20:
        case 21:
            return nLevel+3;
        default:
            if(nTurningCheck < 0)
                return nLevel-4;
            else
                return nLevel+4;
    }
    //somethings gone wrong here
    return 0;
}

int GetTurningClassLevel(object oCaster, int bUndeadOnly = FALSE)
{
    int nLevel, nTemp;

    //Baelnorn adds all class levels.
    if(GetLevelByClass(CLASS_TYPE_BAELNORN, oCaster))
        nLevel = GetHitDice(oCaster);
    else
    {
        //full classes
        nLevel += GetLevelByClass(CLASS_TYPE_CLERIC, oCaster);
        nLevel += GetLevelByClass(CLASS_TYPE_DREAD_NECROMANCER, oCaster);
        nLevel += GetLevelByClass(CLASS_TYPE_SOLDIER_OF_LIGHT, oCaster);
        nLevel += GetLevelByClass(CLASS_TYPE_MASTER_OF_SHROUDS, oCaster);
        nLevel += GetLevelByClass(CLASS_TYPE_MORNINGLORD, oCaster);
        nLevel += GetLevelByClass(CLASS_TYPE_ELDRITCH_DISCIPLE, oCaster);
        //Mystics with sun domain can turn undead
        if(GetHasFeat(FEAT_BONUS_DOMAIN_SUN, oCaster))
            nLevel += GetLevelByClass(CLASS_TYPE_MYSTIC, oCaster);

        //True Necromancer's bonus from Necromantic Prowess
        nTemp = GetLevelByClass(CLASS_TYPE_TRUENECRO, oCaster);
        if(nTemp) nLevel += nTemp + (nTemp / 3);

        //offset classes
        nTemp = GetLevelByClass(CLASS_TYPE_PALADIN, oCaster)-2;
        if(nTemp > 0) nLevel += nTemp;
        nTemp = GetLevelByClass(CLASS_TYPE_BLACKGUARD, oCaster)-2;
        if(nTemp > 0) nLevel += nTemp;
        nTemp = GetLevelByClass(CLASS_TYPE_HOSPITALER, oCaster)-2;
        if(nTemp > 0) nLevel += nTemp;
        nTemp = GetLevelByClass(CLASS_TYPE_TEMPLAR, oCaster)-3;
        if(nTemp > 0) nLevel += nTemp;

        //not undead turning classes
        if(!bUndeadOnly)
        {
            nLevel += GetLevelByClass(CLASS_TYPE_JUDICATOR, oCaster);
            nLevel += GetLevelByClass(CLASS_TYPE_BLIGHTLORD, oCaster);
            nTemp = GetLevelByClass(CLASS_TYPE_ANTI_PALADIN, oCaster)-3;
            if(nTemp > 0) nLevel += nTemp;
        }
    }

    //Improved turning feat
    nLevel += GetHasFeat(FEAT_IMPROVED_TURNING, oCaster);

    //Phylactery of Undead Turning
    object oItem = GetItemInSlot(INVENTORY_SLOT_NECK, oCaster);
    if(GetTag(oItem) == "prc_turnphyl")
        nLevel += 4;

    return nLevel;
}

int GetTurningCharismaMod(object oCaster, int nTurnType)
{
    int nChaMod = GetAbilityModifier(ABILITY_CHARISMA, oCaster);

    //Heartwarder adds two to cha checks
    if(GetHasFeat(FEAT_HEART_PASSION, oCaster))
        nChaMod += 2;

    if(nTurnType == SPELL_TURN_UNDEAD)
    {
        //Hierophants Mastery of Energy
        if(GetHasFeat(FEAT_MASTER_OF_ENERGY, oCaster))
            nChaMod += 4;

        //Consecrate spell
        if(GetHasSpellEffect(SPELL_CONCECRATE, oCaster))
            nChaMod += 3;

        //Desecrate spell
        if(GetHasSpellEffect(SPELL_DES_20, oCaster))
            nChaMod -= 3;
    }

    return nChaMod;
}

int GetHitDiceForTurning(object oTarget, int nTurnType)
{
    //Hit Dice
    int nHD = GetHitDice(oTarget);

    //Turn Resistance
    nHD += GetTurnResistanceHD(oTarget);

    if(GetHasFeat(FEAT_TURN_SUBMISSION, oTarget))
        nHD -= 4;

    //Outsiders get SR (halved if turner has planar turning)
    if(MyPRCGetRacialType(oTarget) == RACIAL_TYPE_OUTSIDER)
    {
        int nSR = PRCGetSpellResistance(oTarget, OBJECT_SELF);
        if(GetHasFeat(FEAT_EPIC_PLANAR_TURNING) || nTurnType == SPELL_TURN_OUTSIDER)
            nSR /= 2;
        nHD += nSR;
    }

    //return the total
    return nHD;
}

int GetCommandedTotalHD(int nTurnType = SPELL_TURN_UNDEAD, int bUndeadMastery = FALSE)
{
    int i = 1; // Changed variable declaration since GetAssociate starts indexing at 1.
    int nCommandedTotalHD;
    object oTest = GetAssociate(ASSOCIATE_TYPE_DOMINATED, OBJECT_SELF, i);
    object oOldTest = OBJECT_INVALID;
    while(GetIsObjectValid(oTest) && oTest != oOldTest)
    {
        if(PRCGetHasEffect(EFFECT_TYPE_DOMINATED, oTest)) 
        // Changed from GetHasSpellEffect because it was looking for Turn Undead spell attached to creature
        // instead of looking for the dominated effect.
        {
            int nTestHD = GetHitDiceForTurning(oTest, nTurnType);
            if(MyPRCGetRacialType(oTest) != RACIAL_TYPE_UNDEAD
                && bUndeadMastery)
                nTestHD *= 10;
            nCommandedTotalHD += nTestHD;
        }
        i++;
        oOldTest = oTest;
        oTest = GetAssociate(ASSOCIATE_TYPE_DOMINATED, OBJECT_SELF, i);
    }
    return nCommandedTotalHD;
}

// TRUE = Turn
// FALSE = Rebuke
int GetTurnTargets(object oTarget, int nTurnType)
{
    return TRUE;
}

int CheckTargetName(object oTarget, string sName)
{
    // Make them both lowercase
    sName = GetStringLowerCase(sName);
    string sTargetName = GetStringLowerCase(GetName(oTarget));
    // Get the position first
    int nPos = FindSubString(sTargetName, sName);
    string sTest = GetSubString(GetName(oTarget), nPos, GetStringLength(sName));
    // If this all worked, sTest should equal sName
    if(sTest == sName) return TRUE;

    return FALSE;
}

int GetIsAirCreature(object oCreature, int nAppearance)
{
    if(nAppearance == APPEARANCE_TYPE_ELEMENTAL_AIR
    || nAppearance == APPEARANCE_TYPE_ELEMENTAL_AIR_ELDER
    || nAppearance == APPEARANCE_TYPE_INVISIBLE_STALKER
    || nAppearance == APPEARANCE_TYPE_MEPHIT_AIR
    || nAppearance == APPEARANCE_TYPE_MEPHIT_DUST
    || nAppearance == APPEARANCE_TYPE_MEPHIT_ICE
    || nAppearance == APPEARANCE_TYPE_WILL_O_WISP
    || CheckTargetName(oCreature, "Air")) // This last one is to catch anything named with Air that doesnt use the appearances
    {
        return TRUE;
    }
    return FALSE;
}

int GetIsEarthCreature(object oCreature, int nAppearance)
{
    if(nAppearance == APPEARANCE_TYPE_ELEMENTAL_EARTH
    || nAppearance == APPEARANCE_TYPE_ELEMENTAL_EARTH_ELDER
    || nAppearance == APPEARANCE_TYPE_GARGOYLE
    || nAppearance == APPEARANCE_TYPE_MEPHIT_EARTH
    || nAppearance == APPEARANCE_TYPE_MEPHIT_SALT
    || CheckTargetName(oCreature, "Earth")) // As above
    {
        return TRUE;
    }
    return FALSE;
}

int GetIsFireCreature(object oCreature, int nAppearance)
{
    if(nAppearance == APPEARANCE_TYPE_ELEMENTAL_FIRE
    || nAppearance == APPEARANCE_TYPE_ELEMENTAL_FIRE_ELDER
    || nAppearance == APPEARANCE_TYPE_AZER_MALE
    || nAppearance == APPEARANCE_TYPE_AZER_FEMALE
    || nAppearance == APPEARANCE_TYPE_GIANT_FIRE
    || nAppearance == APPEARANCE_TYPE_GIANT_FIRE_FEMALE
    || nAppearance == APPEARANCE_TYPE_MEPHIT_FIRE
    || nAppearance == APPEARANCE_TYPE_MEPHIT_MAGMA
    || nAppearance == APPEARANCE_TYPE_MEPHIT_STEAM
    || CheckTargetName(oCreature, "Fire")) // This last one is to catch anything named with Fire that doesnt use the appearances
    {
        return TRUE;
    }
    return FALSE;
}

int GetIsWaterCreature(object oCreature, int nAppearance)
{
    if(nAppearance == APPEARANCE_TYPE_ELEMENTAL_WATER
    || nAppearance == APPEARANCE_TYPE_ELEMENTAL_WATER_ELDER
    || nAppearance == APPEARANCE_TYPE_MEPHIT_WATER
    || nAppearance == APPEARANCE_TYPE_MEPHIT_OOZE
    || CheckTargetName(oCreature, "Water")) // As above
    {
        return TRUE;
    }
    return FALSE;
}

int GetIsReptile(object oCreature, int nAppearance)
{
    if(nAppearance == APPEARANCE_TYPE_KOBOLD_A
    || nAppearance == APPEARANCE_TYPE_KOBOLD_B
    || nAppearance == APPEARANCE_TYPE_KOBOLD_CHIEF_A
    || nAppearance == APPEARANCE_TYPE_KOBOLD_CHIEF_B
    || nAppearance == APPEARANCE_TYPE_KOBOLD_SHAMAN_A
    || nAppearance == APPEARANCE_TYPE_KOBOLD_SHAMAN_B
    || nAppearance == APPEARANCE_TYPE_LIZARDFOLK_A
    || nAppearance == APPEARANCE_TYPE_LIZARDFOLK_B
    || nAppearance == APPEARANCE_TYPE_LIZARDFOLK_WARRIOR_A
    || nAppearance == APPEARANCE_TYPE_LIZARDFOLK_WARRIOR_B
    || nAppearance == APPEARANCE_TYPE_LIZARDFOLK_SHAMAN_A
    || nAppearance == APPEARANCE_TYPE_LIZARDFOLK_SHAMAN_B
    || nAppearance == 451 // Trog
    || nAppearance == 452 // Trog Warrior
    || nAppearance == 453 // Trog Cleric
    || CheckTargetName(oCreature, "Scale")
    || CheckTargetName(oCreature, "Snake")
    || CheckTargetName(oCreature, "Reptile")) // Reptilian names
    {
        return TRUE;
    }
    return FALSE;
}

int GetIsSpider(object oCreature, int nAppearance)
{
    if(nAppearance == APPEARANCE_TYPE_SPIDER_DEMON
    || nAppearance == APPEARANCE_TYPE_SPIDER_DIRE
    || nAppearance == APPEARANCE_TYPE_SPIDER_GIANT
    || nAppearance == APPEARANCE_TYPE_SPIDER_SWORD
    || nAppearance == APPEARANCE_TYPE_SPIDER_PHASE
    || nAppearance == APPEARANCE_TYPE_SPIDER_WRAITH
    || CheckTargetName(oCreature, "Spider")) // Duh
    {
        return TRUE;
    }
    return FALSE;
}

// Test main
//void main(){}

