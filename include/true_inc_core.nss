//::///////////////////////////////////////////////
//:: Truenaming include: Core functions
//:: true_inc_core
//::///////////////////////////////////////////////
/** @file
    Defines various functions and other stuff that
    other scripts in the truenaming functions require.

*/
//:://////////////////////////////////////////////
//:://////////////////////////////////////////////


//////////////////////////////////////////////////
/*             Function prototypes              */
//////////////////////////////////////////////////

/*
 * Returns TRUE if it is a Syllable (Bereft class ability).
 * @param nSpellId   Utterance to check
 *
 * @return           TRUE or FALSE
 */
int GetIsSyllable(int nSpellId);

//////////////////////////////////////////////////
/*                  Includes                    */
//////////////////////////////////////////////////

#include "true_utter_const"

//////////////////////////////////////////////////
/*             Function definitions             */
//////////////////////////////////////////////////

int GetIsSyllable(int nSpellId)
{
    switch(nSpellId)
    {
        case SYLLABLE_DETACHMENT:
        case SYLLABLE_AFFLICATION_SIGHT:
        case SYLLABLE_AFFLICATION_SOUND:
        case SYLLABLE_AFFLICATION_TOUCH:
        case SYLLABLE_EXILE:
        case SYLLABLE_DISSOLUTION:
        case SYLLABLE_ENERVATION:
            return TRUE;
    }

    return FALSE;
}