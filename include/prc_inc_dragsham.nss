
// Metabreath Feats - Not implemented yet. 
/** const int FEAT_CLINGING_BREATH         = 5000;
const int FEAT_LINGERING_BREATH        = 5001;
const int FEAT_ENLARGE_BREATH          = 5002;
const int FEAT_HEIGHTEN_BREATH         = 5003;
const int FEAT_MAXIMIZE_BREATH         = 5004;
const int FEAT_QUICKEN_BREATH          = 5005;
const int FEAT_RECOVER_BREATH          = 5006;
const int FEAT_SHAPE_BREATH            = 5007;
const int FEAT_SPLIT_BREATH            = 5008;
const int FEAT_SPREADING_BREATH        = 5009;
const int FEAT_EXTEND_SPREADING_BREATH = 5010;
const int FEAT_TEMPEST_BREATH          = 5011; **/

// Dragon Shaman Aura flag
//const int DRAGON_SHAMAN_AURA_ACTIVE    = 5000;

#include "x2_inc_switches"

// returns the damage type of dragon that the PC has a totem for. Checks for the presence of the
// various feats that are present indicating such. This is necessary for determining the type
// of breath weapon and the type of damage immunity.
int GetDragonDamageType(object oPC);

// Used to create a flag on the caster and store the Aura currently being run.
//int StartDragonShamanAura(object oCaster, int nSpellId);

// Resets the available ToV points; for use after rest or on enter.
void ResetTouchOfVitality(object oPC);

// Applies any metabreath feats that the dragon shaman may have to his breathweapon
//int ApplyMetaBreathFeatMods( int nDuration, object oCaster );

int GetDragonDamageType(object oPC)
{
    int nDamageType = GetHasFeat(FEAT_DRAGONSHAMAN_RED,    oPC) ? DAMAGE_TYPE_FIRE:
                      GetHasFeat(FEAT_DRAGONSHAMAN_BLUE,   oPC) ? DAMAGE_TYPE_ELECTRICAL:
                      GetHasFeat(FEAT_DRAGONSHAMAN_SILVER, oPC) ? DAMAGE_TYPE_COLD:
                      GetHasFeat(FEAT_DRAGONSHAMAN_GREEN,  oPC) ? DAMAGE_TYPE_ACID:
                      GetHasFeat(FEAT_DRAGONSHAMAN_WHITE,  oPC) ? DAMAGE_TYPE_COLD:
                      GetHasFeat(FEAT_DRAGONSHAMAN_BLACK,  oPC) ? DAMAGE_TYPE_ACID:
                      GetHasFeat(FEAT_DRAGONSHAMAN_GOLD,   oPC) ? DAMAGE_TYPE_FIRE:
                      GetHasFeat(FEAT_DRAGONSHAMAN_BRONZE, oPC) ? DAMAGE_TYPE_ELECTRICAL:
                      GetHasFeat(FEAT_DRAGONSHAMAN_BRASS,  oPC) ? DAMAGE_TYPE_FIRE:
                      GetHasFeat(FEAT_DRAGONSHAMAN_COPPER, oPC) ? DAMAGE_TYPE_ACID:
                      -1;

    return nDamageType;
}

void ResetTouchOfVitality(object oPC)
{
    int nDSLevel = GetLevelByClass(CLASS_TYPE_DRAGON_SHAMAN, oPC);
    if(GetHasFeat(FEAT_DRAGONSHAMAN_TOUCHVITALITY, oPC))
    {
        int nChaBonus = GetAbilityModifier(ABILITY_CHARISMA, oPC);
        if(nChaBonus < 0) nChaBonus = 0;
        int nVitPoints = 2 * nDSLevel * nChaBonus;
        SetLocalInt(oPC, "DRAGON_SHAMAN_TOUCH_REMAIN", nVitPoints);
        string sMes = "Healing power: " + IntToString(nVitPoints) + " points.";
        FloatingTextStringOnCreature(sMes, oPC, FALSE);
    }
}

int GetIsAuraActive(int nFirst, int nSecond)
{
    int nAura;
    switch(nFirst)
    {
        case SPELL_DRACONIC_AURA_PRESENCE:      return nSecond == SPELL_SECOND_AURA_PRESENCE;
        case SPELL_DRACONIC_AURA_VIGOR:         return nSecond == SPELL_SECOND_AURA_VIGOR;
        case SPELL_DRACONIC_AURA_TOUGHNESS:     return nSecond == SPELL_SECOND_AURA_TOUGHNESS;
        case SPELL_DRACONIC_AURA_ENERGY_SHIELD: return nSecond == SPELL_SECOND_AURA_ENERGY_SHIELD;
        case SPELL_DRACONIC_AURA_RESISTANCE:    return nSecond == SPELL_SECOND_AURA_RESISTANCE;
        case SPELL_DRACONIC_AURA_POWER:         return nSecond == SPELL_SECOND_AURA_POWER;
        case SPELL_DRACONIC_AURA_SENSES:        return nSecond == SPELL_SECOND_AURA_SENSES;
        case SPELL_DRACONIC_AURA_INSIGHT:       return nSecond == SPELL_SECOND_AURA_INSIGHT;
        case SPELL_DRACONIC_AURA_RESOLVE:       return nSecond == SPELL_SECOND_AURA_RESOLVE;
        case SPELL_DRACONIC_AURA_STAMINA:       return nSecond == SPELL_SECOND_AURA_STAMINA;
        case SPELL_DRACONIC_AURA_SWIFTNESS:     return nSecond == SPELL_SECOND_AURA_SWIFTNESS;
		//TODO: FIXME: HACK: XXX: na na na na na na na na...
		/*
        case SPELL_DRACONIC_AURA_RESISTANCE:    return nSecond == SPELL_SECOND_AURA_RESISTACID;
        case SPELL_DRACONIC_AURA_RESISTANCE:    return nSecond == SPELL_SECOND_AURA_RESISTCOLD;
        case SPELL_DRACONIC_AURA_RESISTANCE:    return nSecond == SPELL_SECOND_AURA_RESISTELEC;
        case SPELL_DRACONIC_AURA_RESISTANCE:    return nSecond == SPELL_SECOND_AURA_RESISTFIRE;
		*/
        case SPELL_DRACONIC_AURA_MAGICPOWER:    return nSecond == SPELL_SECOND_AURA_MAGICPOWER;
		//TODO: FIXME: HACK: XXX: wat-man!
		/*
        case SPELL_DRACONIC_AURA_ENERGY:        return nSecond == SPELL_SECOND_AURA_ENERGY;
        case SPELL_DRACONIC_AURA_ENERGY:        return nSecond == SPELL_SECOND_AURA_ENERGYACID;
        case SPELL_DRACONIC_AURA_ENERGY:        return nSecond == SPELL_SECOND_AURA_ENERGYCOLD;
        case SPELL_DRACONIC_AURA_ENERGY:        return nSecond == SPELL_SECOND_AURA_ENERGYELEC;
        case SPELL_DRACONIC_AURA_ENERGY:        return nSecond == SPELL_SECOND_AURA_ENERGYFIRE;
		*/
    }
    return -1;
}

int GetMarshalAuraBonus(object oPC)
{
    int nBonus;
    int iMarshalLevel = GetLevelByClass(CLASS_TYPE_MARSHAL, oPC);
    if(iMarshalLevel > 19)
        nBonus = iMarshalLevel / 5;
    else if(iMarshalLevel > 13)
        nBonus = 3;
    else if(iMarshalLevel > 6)
        nBonus = 2;
    else if(iMarshalLevel > 1)
        nBonus = 1;

    return nBonus;
}

int GetAuraBonus(object oPC)
{
    int nAuraBonus = (GetLevelByClass(CLASS_TYPE_DRAGON_SHAMAN, oPC) / 5) + 1;
    int nMarshalBonus = GetMarshalAuraBonus(oPC);
    int nExtraBonus;

    if(nMarshalBonus > nAuraBonus)
        nAuraBonus = nMarshalBonus;

    if     (GetHasFeat(FEAT_BONUS_DRACONIC_AURA_LEVEL_4, oPC)) nExtraBonus = 4;
    else if(GetHasFeat(FEAT_BONUS_DRACONIC_AURA_LEVEL_3, oPC)) nExtraBonus = 3;
    else if(GetHasFeat(FEAT_BONUS_DRACONIC_AURA_LEVEL_2, oPC)) nExtraBonus = 2;
    else if(GetHasFeat(FEAT_BONUS_DRACONIC_AURA_LEVEL_1, oPC)) nExtraBonus = 1;

    if(nExtraBonus > nAuraBonus)
        nAuraBonus = nExtraBonus;

    return nAuraBonus;
}

object GetAuraObject(object oShaman, string sTag)
{
    location lTarget = GetLocation(oShaman);
    object oAura = GetFirstObjectInShape(SHAPE_SPHERE, 1.0f, lTarget, FALSE, OBJECT_TYPE_AREA_OF_EFFECT);
    while(GetIsObjectValid(oAura))
    {
        if((GetAreaOfEffectCreator(oAura) == oShaman) //was cast by shaman
        && GetTag(oAura) == sTag                      //it's a draconic aura
        && !GetLocalInt(oShaman, "SpellID"))          //and was not setup before
        {
            return oAura;
        }
        oAura = GetNextObjectInShape(SHAPE_SPHERE, 1.0f, lTarget, FALSE, OBJECT_TYPE_AREA_OF_EFFECT);
    }
    return OBJECT_INVALID;
}
