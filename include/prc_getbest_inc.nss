// Returns the best available-for-casting n-th Level spell from oTarget.
int GetBestL0Spell(object oTarget, int nSpell);
int GetBestL1Spell(object oTarget, int nSpell);
int GetBestL2Spell(object oTarget, int nSpell);
int GetBestL3Spell(object oTarget, int nSpell);
int GetBestL4Spell(object oTarget, int nSpell);
int GetBestL5Spell(object oTarget, int nSpell);
int GetBestL6Spell(object oTarget, int nSpell);
int GetBestL7Spell(object oTarget, int nSpell);
int GetBestL8Spell(object oTarget, int nSpell);
int GetBestL9Spell(object oTarget, int nSpell);

// Returns the best available-for-casting spell from oTarget's repertoire.
int GetBestAvailableSpell(object oTarget);

int GetBestL0Spell(object oTarget, int nSpell)
{
    if(GetHasSpell(SPELL_ACID_SPLASH, oTarget))                    return SPELL_ACID_SPLASH;
    if(GetHasSpell(SPELL_RAY_OF_FROST, oTarget))                   return SPELL_RAY_OF_FROST;
    if(GetHasSpell(SPELL_DAZE, oTarget))                           return SPELL_DAZE;
    if(GetHasSpell(SPELL_ELECTRIC_JOLT, oTarget))                  return SPELL_ELECTRIC_JOLT;
    if(GetHasSpell(SPELL_FLARE, oTarget))                          return SPELL_FLARE;
    if(GetHasSpell(SPELL_RESISTANCE, oTarget))                     return SPELL_RESISTANCE;
    if(GetHasSpell(SPELL_INFLICT_MINOR_WOUNDS, oTarget))           return SPELL_INFLICT_MINOR_WOUNDS;
    if(GetHasSpell(SPELL_CURE_MINOR_WOUNDS, oTarget))              return SPELL_CURE_MINOR_WOUNDS;
    if(GetHasSpell(SPELL_LIGHT, oTarget))                          return SPELL_LIGHT;
    if(GetHasSpell(SPELL_VIRTUE, oTarget))                         return SPELL_VIRTUE;
    return nSpell;
}

int GetBestL1Spell(object oTarget, int nSpell)
{
    if(GetHasSpell(SPELL_CURE_LIGHT_WOUNDS, oTarget))              return SPELL_CURE_LIGHT_WOUNDS;
    if(GetHasSpell(SPELL_MAGIC_MISSILE, oTarget))                  return SPELL_MAGIC_MISSILE;
    if(GetHasSpell(SPELL_SUMMON_CREATURE_I, oTarget))              return SPELL_SUMMON_CREATURE_I;
    if(GetHasSpell(SPELL_DOOM, oTarget))                           return SPELL_DOOM;
    if(GetHasSpell(SPELL_BANE, oTarget))                           return SPELL_BANE;
    if(GetHasSpell(SPELL_BLESS, oTarget))                          return SPELL_BLESS;
    if(GetHasSpell(SPELL_MAGIC_FANG, oTarget))                     return SPELL_MAGIC_FANG;
    if(GetHasSpell(SPELL_MAGE_ARMOR, oTarget))                     return SPELL_MAGE_ARMOR;
    if(GetHasSpell(SPELL_ENDURE_ELEMENTS, oTarget))                return SPELL_ENDURE_ELEMENTS;
    if(GetHasSpell(SPELL_LESSER_DISPEL, oTarget))                  return SPELL_LESSER_DISPEL;
    if(GetHasSpell(SPELL_SANCTUARY, oTarget))                      return SPELL_SANCTUARY;
    if(GetHasSpell(SPELL_SHIELD, oTarget))                         return SPELL_SHIELD;
    if(GetHasSpell(SPELL_CHARM_PERSON, oTarget))                   return SPELL_CHARM_PERSON;
    if(GetHasSpell(SPELL_DEAFENING_CLANG, oTarget))                return SPELL_DEAFENING_CLANG;
    if(GetHasSpell(SPELL_BALAGARNSIRONHORN, oTarget))              return SPELL_BALAGARNSIRONHORN;
    if(GetHasSpell(SPELL_BLESS_WEAPON, oTarget))                   return SPELL_BLESS_WEAPON;
    if(GetHasSpell(SPELL_SHELGARNS_PERSISTENT_BLADE, oTarget))     return SPELL_SHELGARNS_PERSISTENT_BLADE;
    if(GetHasSpell(SPELL_NEGATIVE_ENERGY_RAY, oTarget))            return SPELL_NEGATIVE_ENERGY_RAY;
    if(GetHasSpell(SPELL_BURNING_HANDS, oTarget))                  return SPELL_BURNING_HANDS;
    if(GetHasSpell(SPELL_HORIZIKAULS_BOOM, oTarget))               return SPELL_HORIZIKAULS_BOOM;
    if(GetHasSpell(SPELL_SHIELD_OF_FAITH, oTarget))                return SPELL_SHIELD_OF_FAITH;
    if(GetHasSpell(SPELL_AMPLIFY, oTarget))                        return SPELL_AMPLIFY;
    if(GetHasSpell(SPELL_TRUE_STRIKE, oTarget))                    return SPELL_TRUE_STRIKE;
    if(GetHasSpell(SPELL_RAY_OF_ENFEEBLEMENT, oTarget))            return SPELL_RAY_OF_ENFEEBLEMENT;
    if(GetHasSpell(SPELL_EXPEDITIOUS_RETREAT, oTarget))            return SPELL_EXPEDITIOUS_RETREAT;
    if(GetHasSpell(SPELL_ICE_DAGGER, oTarget))                     return SPELL_ICE_DAGGER;
    if(GetHasSpell(SPELL_ENTROPIC_SHIELD, oTarget))                return SPELL_ENTROPIC_SHIELD;
    if(GetHasSpell(SPELL_ENTANGLE, oTarget))                       return SPELL_ENTANGLE;
    if(GetHasSpell(SPELL_DIVINE_FAVOR, oTarget))                   return SPELL_DIVINE_FAVOR;
    if(GetHasSpell(SPELL_SLEEP, oTarget))                          return SPELL_SLEEP;
    if(GetHasSpell(SPELL_MAGIC_WEAPON, oTarget))                   return SPELL_MAGIC_WEAPON;
    if(GetHasSpell(SPELL_SCARE, oTarget))                          return SPELL_SCARE;
    if(GetHasSpell(SPELL_GREASE, oTarget))                         return SPELL_GREASE;
    if(GetHasSpell(SPELL_CAMOFLAGE, oTarget))                      return SPELL_CAMOFLAGE;
    if(GetHasSpell(SPELL_COLOR_SPRAY, oTarget))                    return SPELL_COLOR_SPRAY;
    if(GetHasSpell(SPELL_RESIST_ELEMENTS, oTarget))                return SPELL_RESIST_ELEMENTS;
    if(GetHasSpell(SPELL_REMOVE_FEAR, oTarget))                    return SPELL_REMOVE_FEAR;
    if(GetHasSpell(SPELL_IRONGUTS, oTarget))                       return SPELL_IRONGUTS;
    if(GetHasSpell(SPELL_INFLICT_LIGHT_WOUNDS, oTarget))           return SPELL_INFLICT_LIGHT_WOUNDS;
    if(GetHasSpell(SPELL_PROTECTION_FROM_LAW, oTarget))            return SPELL_PROTECTION_FROM_LAW;
    if(GetHasSpell(SPELL_PROTECTION_FROM_GOOD, oTarget))           return SPELL_PROTECTION_FROM_GOOD;
    if(GetHasSpell(SPELL_PROTECTION__FROM_CHAOS, oTarget))         return SPELL_PROTECTION__FROM_CHAOS;
    if(GetHasSpell(SPELL_PROTECTION_FROM_EVIL, oTarget))           return SPELL_PROTECTION_FROM_EVIL;
    if(GetHasSpell(SPELL_IDENTIFY, oTarget))                       return SPELL_IDENTIFY;
    return nSpell;
}

int GetBestL2Spell(object oTarget, int nSpell)
{
    if(GetHasSpell(SPELL_MELFS_ACID_ARROW, oTarget))               return SPELL_MELFS_ACID_ARROW;
    if(GetHasSpell(SPELL_CURE_MODERATE_WOUNDS, oTarget))           return SPELL_CURE_MODERATE_WOUNDS;
    if(GetHasSpell(SPELL_BULLS_STRENGTH, oTarget))                 return SPELL_BULLS_STRENGTH;
    if(GetHasSpell(SPELL_CATS_GRACE, oTarget))                     return SPELL_CATS_GRACE;
    if(GetHasSpell(SPELL_ENDURANCE, oTarget))                      return SPELL_ENDURANCE;
    if(GetHasSpell(SPELL_FOXS_CUNNING, oTarget))                   return SPELL_FOXS_CUNNING;
    if(GetHasSpell(SPELL_EAGLE_SPLEDOR, oTarget))                  return SPELL_EAGLE_SPLEDOR;
    if(GetHasSpell(SPELL_OWLS_WISDOM, oTarget))                    return SPELL_OWLS_WISDOM;
    if(GetHasSpell(SPELL_PROTECTION_FROM_ELEMENTS, oTarget))       return SPELL_PROTECTION_FROM_ELEMENTS;
    if(GetHasSpell(SPELL_SUMMON_CREATURE_II, oTarget))             return SPELL_SUMMON_CREATURE_II;
    if(GetHasSpell(SPELL_ONE_WITH_THE_LAND, oTarget))              return SPELL_ONE_WITH_THE_LAND;
    if(GetHasSpell(SPELL_INVISIBILITY, oTarget))                   return SPELL_INVISIBILITY;
    if(GetHasSpell(SPELL_CLARITY, oTarget))                        return SPELL_CLARITY;
    if(GetHasSpell(SPELL_FIND_TRAPS, oTarget))                     return SPELL_FIND_TRAPS;
    if(GetHasSpell(SPELL_LESSER_RESTORATION, oTarget))             return SPELL_LESSER_RESTORATION;
    if(GetHasSpell(SPELL_FLAME_LASH, oTarget))                     return SPELL_FLAME_LASH;
    if(GetHasSpell(SPELL_FLAME_WEAPON, oTarget))                   return SPELL_FLAME_WEAPON;
    if(GetHasSpell(SPELL_WEB, oTarget))                            return SPELL_WEB;
    if(GetHasSpell(SPELL_COMBUST, oTarget))                        return SPELL_COMBUST;
    if(GetHasSpell(SPELL_GHOUL_TOUCH, oTarget))                    return SPELL_GHOUL_TOUCH;
    if(GetHasSpell(SPELL_KNOCK, oTarget))                          return SPELL_KNOCK;
    if(GetHasSpell(SPELL_GHOSTLY_VISAGE, oTarget))                 return SPELL_GHOSTLY_VISAGE;
    if(GetHasSpell(SPELL_SOUND_BURST, oTarget))                    return SPELL_SOUND_BURST;
    if(GetHasSpell(SPELL_SILENCE, oTarget))                        return SPELL_SILENCE;
    if(GetHasSpell(SPELL_SEE_INVISIBILITY, oTarget))               return SPELL_SEE_INVISIBILITY;
    if(GetHasSpell(SPELL_HOLD_PERSON, oTarget))                    return SPELL_HOLD_PERSON;
    if(GetHasSpell(SPELL_GEDLEES_ELECTRIC_LOOP, oTarget))          return SPELL_GEDLEES_ELECTRIC_LOOP;
    if(GetHasSpell(SPELL_REMOVE_PARALYSIS, oTarget))               return SPELL_REMOVE_PARALYSIS;
    if(GetHasSpell(SPELL_CLOUD_OF_BEWILDERMENT, oTarget))          return SPELL_CLOUD_OF_BEWILDERMENT;
    if(GetHasSpell(SPELL_TASHAS_HIDEOUS_LAUGHTER, oTarget))        return SPELL_TASHAS_HIDEOUS_LAUGHTER;
    if(GetHasSpell(SPELL_BLOOD_FRENZY, oTarget))                   return SPELL_BLOOD_FRENZY;
    if(GetHasSpell(SPELL_BLINDNESS_AND_DEAFNESS, oTarget))         return SPELL_BLINDNESS_AND_DEAFNESS;
    if(GetHasSpell(SPELL_STONE_BONES, oTarget))                    return SPELL_STONE_BONES;
    if(GetHasSpell(SPELL_BARKSKIN, oTarget))                       return SPELL_BARKSKIN;
    if(GetHasSpell(SPELL_DARKVISION, oTarget))                     return SPELL_DARKVISION;
    if(GetHasSpell(SPELL_DEATH_ARMOR, oTarget))                    return SPELL_DEATH_ARMOR;
    if(GetHasSpell(SPELL_DARKNESS, oTarget))                       return SPELL_DARKNESS;
    if(GetHasSpell(SPELL_CHARM_PERSON_OR_ANIMAL, oTarget))         return SPELL_CHARM_PERSON_OR_ANIMAL;
    if(GetHasSpell(SPELL_AURAOFGLORY, oTarget))                    return SPELL_AURAOFGLORY;
    if(GetHasSpell(SPELL_HOLD_ANIMAL, oTarget))                    return SPELL_HOLD_ANIMAL;
    if(GetHasSpell(SPELL_INFLICT_MODERATE_WOUNDS, oTarget))        return SPELL_INFLICT_MODERATE_WOUNDS;
    if(GetHasSpell(SPELL_AID, oTarget))                            return SPELL_AID;
    if(GetHasSpell(SPELL_CONTINUAL_FLAME, oTarget))                return SPELL_CONTINUAL_FLAME;
    return nSpell;
}

int GetBestL3Spell(object oTarget, int nSpell)
{
    if(GetHasSpell(SPELL_FLAME_ARROW, oTarget))                    return SPELL_FLAME_ARROW;
    if(GetHasSpell(SPELL_CURE_SERIOUS_WOUNDS, oTarget))            return SPELL_CURE_SERIOUS_WOUNDS;
    if(GetHasSpell(SPELL_CALL_LIGHTNING, oTarget))                 return SPELL_CALL_LIGHTNING;
    if(GetHasSpell(SPELL_FIREBALL, oTarget))                       return SPELL_FIREBALL;
    if(GetHasSpell(SPELL_DISPLACEMENT, oTarget))                   return SPELL_DISPLACEMENT;
    if(GetHasSpell(SPELL_DISPEL_MAGIC, oTarget))                   return SPELL_DISPEL_MAGIC;
    if(GetHasSpell(SPELL_HASTE, oTarget))                          return SPELL_HASTE;
    if(GetHasSpell(SPELL_SLOW, oTarget))                           return SPELL_SLOW;
    if(GetHasSpell(SPELL_VAMPIRIC_TOUCH, oTarget))                 return SPELL_VAMPIRIC_TOUCH;
    if(GetHasSpell(SPELL_SEARING_LIGHT, oTarget))                  return SPELL_SEARING_LIGHT;
    if(GetHasSpell(SPELL_SCINTILLATING_SPHERE, oTarget))           return SPELL_SCINTILLATING_SPHERE;
    if(GetHasSpell(SPELL_MESTILS_ACID_BREATH, oTarget))            return SPELL_MESTILS_ACID_BREATH;
    if(GetHasSpell(SPELL_MAGIC_CIRCLE_AGAINST_LAW, oTarget))       return SPELL_MAGIC_CIRCLE_AGAINST_LAW;
    if(GetHasSpell(SPELL_MAGIC_CIRCLE_AGAINST_GOOD, oTarget))      return SPELL_MAGIC_CIRCLE_AGAINST_GOOD;
    if(GetHasSpell(SPELL_MAGIC_CIRCLE_AGAINST_EVIL, oTarget))      return SPELL_MAGIC_CIRCLE_AGAINST_EVIL;
    if(GetHasSpell(SPELL_MAGIC_CIRCLE_AGAINST_CHAOS, oTarget))     return SPELL_MAGIC_CIRCLE_AGAINST_CHAOS;
    if(GetHasSpell(SPELL_LIGHTNING_BOLT, oTarget))                 return SPELL_LIGHTNING_BOLT;
    if(GetHasSpell(SPELL_NEGATIVE_ENERGY_BURST, oTarget))          return SPELL_NEGATIVE_ENERGY_BURST;
    if(GetHasSpell(SPELL_SUMMON_CREATURE_III, oTarget))            return SPELL_SUMMON_CREATURE_III;
    if(GetHasSpell(SPELL_KEEN_EDGE, oTarget))                      return SPELL_KEEN_EDGE;
    if(GetHasSpell(SPELL_MAGIC_VESTMENT, oTarget))                 return SPELL_MAGIC_VESTMENT;
    if(GetHasSpell(SPELL_DOMINATE_ANIMAL, oTarget))                return SPELL_DOMINATE_ANIMAL;
    if(GetHasSpell(SPELL_GLYPH_OF_WARDING, oTarget))               return SPELL_GLYPH_OF_WARDING;
    if(GetHasSpell(SPELL_INVISIBILITY_SPHERE, oTarget))            return SPELL_INVISIBILITY_SPHERE;
    if(GetHasSpell(SPELL_INVISIBILITY_PURGE, oTarget))             return SPELL_INVISIBILITY_PURGE;
    if(GetHasSpell(SPELL_FEAR, oTarget))                           return SPELL_FEAR;
    if(GetHasSpell(SPELL_BLADE_THIRST, oTarget))                   return SPELL_BLADE_THIRST;
    if(GetHasSpell(SPELL_GREATER_MAGIC_WEAPON, oTarget))           return SPELL_GREATER_MAGIC_WEAPON;
    if(GetHasSpell(SPELL_POISON, oTarget))                         return SPELL_POISON;
    if(GetHasSpell(SPELL_STINKING_CLOUD, oTarget))                 return SPELL_STINKING_CLOUD;
    if(GetHasSpell(SPELL_SPIKE_GROWTH, oTarget))                   return SPELL_SPIKE_GROWTH;
    if(GetHasSpell(SPELL_WOUNDING_WHISPERS, oTarget))              return SPELL_WOUNDING_WHISPERS;
    if(GetHasSpell(SPELL_QUILLFIRE, oTarget))                      return SPELL_QUILLFIRE;
    if(GetHasSpell(SPELL_GREATER_MAGIC_FANG, oTarget))             return SPELL_GREATER_MAGIC_FANG;
    if(GetHasSpell(SPELL_GUST_OF_WIND, oTarget))                   return SPELL_GUST_OF_WIND;
    if(GetHasSpell(SPELL_INFESTATION_OF_MAGGOTS, oTarget))         return SPELL_INFESTATION_OF_MAGGOTS;
    if(GetHasSpell(SPELL_ANIMATE_DEAD, oTarget))                   return SPELL_ANIMATE_DEAD;
    if(GetHasSpell(SPELL_NEUTRALIZE_POISON, oTarget))              return SPELL_NEUTRALIZE_POISON;
    if(GetHasSpell(SPELL_NEGATIVE_ENERGY_PROTECTION, oTarget))     return SPELL_NEGATIVE_ENERGY_PROTECTION;
    if(GetHasSpell(SPELL_CONTAGION, oTarget))                      return SPELL_CONTAGION;
    if(GetHasSpell(SPELL_HEALING_STING, oTarget))                  return SPELL_HEALING_STING;
    if(GetHasSpell(SPELL_REMOVE_DISEASE, oTarget))                 return SPELL_REMOVE_DISEASE;
    if(GetHasSpell(SPELL_REMOVE_CURSE, oTarget))                   return SPELL_REMOVE_CURSE;
    if(GetHasSpell(SPELL_REMOVE_BLINDNESS_AND_DEAFNESS, oTarget))  return SPELL_REMOVE_BLINDNESS_AND_DEAFNESS;
    if(GetHasSpell(SPELL_CONFUSION, oTarget))                      return SPELL_CONFUSION;
    if(GetHasSpell(SPELL_PRAYER, oTarget))                         return SPELL_PRAYER;
    if(GetHasSpell(SPELL_DARKFIRE, oTarget))                       return SPELL_DARKFIRE;
    if(GetHasSpell(SPELL_INFLICT_SERIOUS_WOUNDS, oTarget))         return SPELL_INFLICT_SERIOUS_WOUNDS;
    if(GetHasSpell(SPELL_CLAIRAUDIENCE_AND_CLAIRVOYANCE, oTarget)) return SPELL_CLAIRAUDIENCE_AND_CLAIRVOYANCE;
    if(GetHasSpell(SPELL_CHARM_MONSTER, oTarget))                  return SPELL_CHARM_MONSTER;
    if(GetHasSpell(SPELL_BESTOW_CURSE, oTarget))                   return SPELL_BESTOW_CURSE;
    return nSpell;
}

int GetBestL4Spell(object oTarget, int nSpell)
{
    if(GetHasSpell(SPELL_ISAACS_LESSER_MISSILE_STORM, oTarget))    return SPELL_ISAACS_LESSER_MISSILE_STORM;
    if(GetHasSpell(SPELL_CURE_CRITICAL_WOUNDS, oTarget))           return SPELL_CURE_CRITICAL_WOUNDS;
    if(GetHasSpell(SPELL_STONESKIN, oTarget))                      return SPELL_STONESKIN;
    if(GetHasSpell(SPELL_DOMINATE_PERSON, oTarget))                return SPELL_DOMINATE_PERSON;
    if(GetHasSpell(SPELL_LESSER_SPELL_BREACH, oTarget))            return SPELL_LESSER_SPELL_BREACH;
    if(GetHasSpell(SPELL_ELEMENTAL_SHIELD, oTarget))               return SPELL_ELEMENTAL_SHIELD;
    if(GetHasSpell(SPELL_IMPROVED_INVISIBILITY, oTarget))          return SPELL_IMPROVED_INVISIBILITY;
    if(GetHasSpell(SPELL_ICE_STORM, oTarget))                      return SPELL_ICE_STORM;
    if(GetHasSpell(SPELL_HAMMER_OF_THE_GODS, oTarget))             return SPELL_HAMMER_OF_THE_GODS;
    if(GetHasSpell(SPELL_SUMMON_CREATURE_IV, oTarget))             return SPELL_SUMMON_CREATURE_IV;
    if(GetHasSpell(SPELL_EVARDS_BLACK_TENTACLES, oTarget))         return SPELL_EVARDS_BLACK_TENTACLES;
    if(GetHasSpell(SPELL_MINOR_GLOBE_OF_INVULNERABILITY, oTarget)) return SPELL_MINOR_GLOBE_OF_INVULNERABILITY;
    if(GetHasSpell(SPELL_LEGEND_LORE, oTarget))                    return SPELL_LEGEND_LORE;
    if(GetHasSpell(SPELL_POLYMORPH_SELF, oTarget))                 return SPELL_POLYMORPH_SELF;
    if(GetHasSpell(SPELL_PHANTASMAL_KILLER, oTarget))              return SPELL_PHANTASMAL_KILLER;
    if(GetHasSpell(SPELL_DIVINE_POWER, oTarget))                   return SPELL_DIVINE_POWER;
    if(GetHasSpell(SPELL_DEATH_WARD, oTarget))                     return SPELL_DEATH_WARD;
    if(GetHasSpell(SPELL_FREEDOM_OF_MOVEMENT, oTarget))            return SPELL_FREEDOM_OF_MOVEMENT;
    if(GetHasSpell(SPELL_WAR_CRY, oTarget))                        return SPELL_WAR_CRY;
    if(GetHasSpell(SPELL_WALL_OF_FIRE, oTarget))                   return SPELL_WALL_OF_FIRE;
    if(GetHasSpell(SPELL_RESTORATION, oTarget))                    return SPELL_RESTORATION;
    if(GetHasSpell(SPELL_MASS_CAMOFLAGE, oTarget))                 return SPELL_MASS_CAMOFLAGE;
    if(GetHasSpell(SPELL_ENERVATION, oTarget))                     return SPELL_ENERVATION;
    if(GetHasSpell(SPELL_HOLY_SWORD, oTarget))                     return SPELL_HOLY_SWORD;
    if(GetHasSpell(SPELL_HOLD_MONSTER, oTarget))                   return SPELL_HOLD_MONSTER;
    if(GetHasSpell(SPELL_INFLICT_CRITICAL_WOUNDS, oTarget))        return SPELL_INFLICT_CRITICAL_WOUNDS;
    if(GetHasSpell(SPELL_DISMISSAL, oTarget))                      return SPELL_DISMISSAL;
    return nSpell;
}

int GetBestL5Spell(object oTarget, int nSpell)
{
    if(GetHasSpell(SPELL_TRUE_SEEING, oTarget))                    return SPELL_TRUE_SEEING;
    if(GetHasSpell(SPELL_BIGBYS_INTERPOSING_HAND, oTarget))        return SPELL_BIGBYS_INTERPOSING_HAND;
    if(GetHasSpell(SPELL_GREATER_DISPELLING, oTarget))             return SPELL_GREATER_DISPELLING;
    if(GetHasSpell(SPELL_LESSER_SPELL_MANTLE, oTarget))            return SPELL_LESSER_SPELL_MANTLE;
    if(GetHasSpell(SPELL_SPELL_RESISTANCE, oTarget))               return SPELL_SPELL_RESISTANCE;
    if(GetHasSpell(SPELL_MONSTROUS_REGENERATION, oTarget))         return SPELL_MONSTROUS_REGENERATION;
    if(GetHasSpell(SPELL_RAISE_DEAD, oTarget))                     return SPELL_RAISE_DEAD;
    if(GetHasSpell(SPELL_MIND_FOG, oTarget))                       return SPELL_MIND_FOG;
    if(GetHasSpell(SPELL_SLAY_LIVING, oTarget))                    return SPELL_SLAY_LIVING;
    if(GetHasSpell(SPELL_LESSER_PLANAR_BINDING, oTarget))          return SPELL_LESSER_PLANAR_BINDING;
    if(GetHasSpell(SPELL_LESSER_MIND_BLANK, oTarget))              return SPELL_LESSER_MIND_BLANK;
    if(GetHasSpell(SPELL_FLAME_STRIKE, oTarget))                   return SPELL_FLAME_STRIKE;
    if(GetHasSpell(SPELL_FIREBRAND, oTarget))                      return SPELL_FIREBRAND;
    if(GetHasSpell(SPELL_INFERNO, oTarget))                        return SPELL_INFERNO;
    if(GetHasSpell(SPELL_CONE_OF_COLD, oTarget))                   return SPELL_CONE_OF_COLD;
    if(GetHasSpell(SPELL_BALL_LIGHTNING, oTarget))                 return SPELL_BALL_LIGHTNING;
    if(GetHasSpell(SPELL_ENERGY_BUFFER, oTarget))                  return SPELL_ENERGY_BUFFER;
    if(GetHasSpell(SPELL_HEALING_CIRCLE, oTarget))                 return SPELL_HEALING_CIRCLE;
    if(GetHasSpell(SPELL_SUMMON_CREATURE_V, oTarget))              return SPELL_SUMMON_CREATURE_V;
    if(GetHasSpell(SPELL_MESTILS_ACID_SHEATH, oTarget))            return SPELL_MESTILS_ACID_SHEATH;
    if(GetHasSpell(SPELL_FEEBLEMIND, oTarget))                     return SPELL_FEEBLEMIND;
    if(GetHasSpell(SPELL_ETHEREAL_VISAGE, oTarget))                return SPELL_ETHEREAL_VISAGE;
    if(GetHasSpell(SPELL_VINE_MINE, oTarget))                      return SPELL_VINE_MINE;
    if(GetHasSpell(SPELL_BATTLETIDE, oTarget))                     return SPELL_BATTLETIDE;
    if(GetHasSpell(SPELL_CIRCLE_OF_DOOM, oTarget))                 return SPELL_CIRCLE_OF_DOOM;
    if(GetHasSpell(SPELL_CLOUDKILL, oTarget))                      return SPELL_CLOUDKILL;
    if(GetHasSpell(SPELL_AWAKEN, oTarget))                         return SPELL_AWAKEN;
    return nSpell;
}

int GetBestL6Spell(object oTarget, int nSpell)
{
    if(GetHasSpell(SPELL_HARM, oTarget))                           return SPELL_HARM;
    if(GetHasSpell(SPELL_ISAACS_GREATER_MISSILE_STORM, oTarget))   return SPELL_ISAACS_GREATER_MISSILE_STORM;
    if(GetHasSpell(SPELL_HEAL, oTarget))                           return SPELL_HEAL;
    if(GetHasSpell(SPELL_BIGBYS_FORCEFUL_HAND, oTarget))           return SPELL_BIGBYS_FORCEFUL_HAND;
    if(GetHasSpell(SPELL_CHAIN_LIGHTNING, oTarget))                return SPELL_CHAIN_LIGHTNING;
    if(GetHasSpell(SPELL_MASS_HASTE, oTarget))                     return SPELL_MASS_HASTE;
    if(GetHasSpell(SPELL_DROWN, oTarget))                          return SPELL_DROWN;
    if(GetHasSpell(SPELL_GREATER_STONESKIN, oTarget))              return SPELL_GREATER_STONESKIN;
    if(GetHasSpell(SPELL_GREATER_SPELL_BREACH, oTarget))           return SPELL_GREATER_SPELL_BREACH;
    if(GetHasSpell(SPELL_CIRCLE_OF_DEATH, oTarget))                return SPELL_CIRCLE_OF_DEATH;
    if(GetHasSpell(SPELL_GLOBE_OF_INVULNERABILITY, oTarget))       return SPELL_GLOBE_OF_INVULNERABILITY;
    if(GetHasSpell(SPELL_UNDEATH_TO_DEATH, oTarget))               return SPELL_UNDEATH_TO_DEATH;
    if(GetHasSpell(SPELL_CRUMBLE, oTarget))                        return SPELL_CRUMBLE;
    if(GetHasSpell(SPELL_REGENERATE, oTarget))                     return SPELL_REGENERATE;
    if(GetHasSpell(SPELL_SUMMON_CREATURE_VI, oTarget))             return SPELL_SUMMON_CREATURE_VI;
    if(GetHasSpell(SPELL_STONEHOLD, oTarget))                      return SPELL_STONEHOLD;
    if(GetHasSpell(SPELL_FLESH_TO_STONE, oTarget))                 return SPELL_FLESH_TO_STONE;
    if(GetHasSpell(SPELL_STONE_TO_FLESH, oTarget))                 return SPELL_STONE_TO_FLESH;
    if(GetHasSpell(SPELL_TENSERS_TRANSFORMATION, oTarget))         return SPELL_TENSERS_TRANSFORMATION;
    if(GetHasSpell(SPELL_CREATE_UNDEAD, oTarget))                  return SPELL_CREATE_UNDEAD;
    if(GetHasSpell(SPELL_CONTROL_UNDEAD, oTarget))                 return SPELL_CONTROL_UNDEAD;
    if(GetHasSpell(SPELL_PLANAR_BINDING, oTarget))                 return SPELL_PLANAR_BINDING;
    if(GetHasSpell(SPELL_PLANAR_ALLY, oTarget))                    return SPELL_PLANAR_ALLY;
    if(GetHasSpell(SPELL_DIRGE, oTarget))                          return SPELL_DIRGE;
    if(GetHasSpell(SPELL_BLADE_BARRIER, oTarget))                  return SPELL_BLADE_BARRIER;
    if(GetHasSpell(SPELL_BANISHMENT, oTarget))                     return SPELL_BANISHMENT;
    if(GetHasSpell(SPELL_ACID_FOG, oTarget))                       return SPELL_ACID_FOG;
    return nSpell;
}

int GetBestL7Spell(object oTarget, int nSpell)
{
    if(GetHasSpell(SPELL_SPELL_MANTLE, oTarget))                   return SPELL_SPELL_MANTLE;
    if(GetHasSpell(SPELL_BIGBYS_GRASPING_HAND, oTarget))           return SPELL_BIGBYS_GRASPING_HAND;
    if(GetHasSpell(SPELL_FIRE_STORM, oTarget))                     return SPELL_FIRE_STORM;
    if(GetHasSpell(SPELL_FINGER_OF_DEATH, oTarget))                return SPELL_FINGER_OF_DEATH;
    if(GetHasSpell(SPELL_PROTECTION_FROM_SPELLS, oTarget))         return SPELL_PROTECTION_FROM_SPELLS;
    if(GetHasSpell(SPELL_WORD_OF_FAITH, oTarget))                  return SPELL_WORD_OF_FAITH;
    if(GetHasSpell(SPELL_SHADOW_SHIELD, oTarget))                  return SPELL_SHADOW_SHIELD;
    if(GetHasSpell(SPELL_CREEPING_DOOM, oTarget))                  return SPELL_CREEPING_DOOM;
    if(GetHasSpell(SPELL_DESTRUCTION, oTarget))                    return SPELL_DESTRUCTION;
    if(GetHasSpell(SPELL_PRISMATIC_SPRAY, oTarget))                return SPELL_PRISMATIC_SPRAY;
    if(GetHasSpell(SPELL_DELAYED_BLAST_FIREBALL, oTarget))         return SPELL_DELAYED_BLAST_FIREBALL;
    if(GetHasSpell(SPELL_GREAT_THUNDERCLAP, oTarget))              return SPELL_GREAT_THUNDERCLAP;
    if(GetHasSpell(SPELL_POWER_WORD_STUN, oTarget))                return SPELL_POWER_WORD_STUN;
    if(GetHasSpell(SPELL_MORDENKAINENS_SWORD, oTarget))            return SPELL_MORDENKAINENS_SWORD;
    if(GetHasSpell(SPELL_RESURRECTION, oTarget))                   return SPELL_RESURRECTION;
    if(GetHasSpell(SPELL_GREATER_RESTORATION, oTarget))            return SPELL_GREATER_RESTORATION;
    if(GetHasSpell(SPELL_SUMMON_CREATURE_VII, oTarget))            return SPELL_SUMMON_CREATURE_VII;
    if(GetHasSpell(SPELL_AURA_OF_VITALITY, oTarget))               return SPELL_AURA_OF_VITALITY;
    return nSpell;
}

int GetBestL8Spell(object oTarget, int nSpell)
{
    if(GetHasSpell(SPELL_BIGBYS_CLENCHED_FIST, oTarget))           return SPELL_BIGBYS_CLENCHED_FIST;
    if(GetHasSpell(SPELL_HORRID_WILTING, oTarget))                 return SPELL_HORRID_WILTING;
    if(GetHasSpell(SPELL_EARTHQUAKE, oTarget))                     return SPELL_EARTHQUAKE;
    if(GetHasSpell(SPELL_NATURES_BALANCE, oTarget))                return SPELL_NATURES_BALANCE;
    if(GetHasSpell(SPELL_INCENDIARY_CLOUD, oTarget))               return SPELL_INCENDIARY_CLOUD;
    if(GetHasSpell(SPELL_MASS_HEAL, oTarget))                      return SPELL_MASS_HEAL;
    if(GetHasSpell(SPELL_MIND_BLANK, oTarget))                     return SPELL_MIND_BLANK;
    if(GetHasSpell(SPELL_PREMONITION, oTarget))                    return SPELL_PREMONITION;
    if(GetHasSpell(SPELL_SUNBURST, oTarget))                       return SPELL_SUNBURST;
    if(GetHasSpell(SPELL_SUNBEAM, oTarget))                        return SPELL_SUNBEAM;
    if(GetHasSpell(SPELL_MASS_CHARM, oTarget))                     return SPELL_MASS_CHARM;
    if(GetHasSpell(SPELL_MASS_BLINDNESS_AND_DEAFNESS, oTarget))    return SPELL_MASS_BLINDNESS_AND_DEAFNESS;
    if(GetHasSpell(SPELL_BOMBARDMENT, oTarget))                    return SPELL_BOMBARDMENT;
    if(GetHasSpell(SPELL_GREATER_PLANAR_BINDING, oTarget))         return SPELL_GREATER_PLANAR_BINDING;
    if(GetHasSpell(SPELL_SUMMON_CREATURE_VIII, oTarget))           return SPELL_SUMMON_CREATURE_VIII;
    if(GetHasSpell(SPELL_CREATE_GREATER_UNDEAD, oTarget))          return SPELL_CREATE_GREATER_UNDEAD;
    if(GetHasSpell(SPELL_BLACKSTAFF, oTarget))                     return SPELL_BLACKSTAFF;
    return nSpell;
}

int GetBestL9Spell(object oTarget, int nSpell)
{
    if(GetHasSpell(SPELL_TIME_STOP, oTarget))                      return SPELL_TIME_STOP;
    if(GetHasSpell(SPELL_BLACK_BLADE_OF_DISASTER, oTarget))        return SPELL_BLACK_BLADE_OF_DISASTER;
    if(GetHasSpell(SPELL_MORDENKAINENS_DISJUNCTION, oTarget))      return SPELL_MORDENKAINENS_DISJUNCTION;
    if(GetHasSpell(SPELL_GREATER_SPELL_MANTLE, oTarget))           return SPELL_GREATER_SPELL_MANTLE;
    if(GetHasSpell(SPELL_BIGBYS_CRUSHING_HAND, oTarget))           return SPELL_BIGBYS_CRUSHING_HAND;
    if(GetHasSpell(SPELL_WAIL_OF_THE_BANSHEE, oTarget))            return SPELL_WAIL_OF_THE_BANSHEE;
    if(GetHasSpell(SPELL_WEIRD, oTarget))                          return SPELL_WEIRD;
    if(GetHasSpell(SPELL_METEOR_SWARM, oTarget))                   return SPELL_METEOR_SWARM;
    if(GetHasSpell(SPELL_IMPLOSION, oTarget))                      return SPELL_IMPLOSION;
    if(GetHasSpell(SPELL_POWER_WORD_KILL, oTarget))                return SPELL_POWER_WORD_KILL;
    if(GetHasSpell(SPELL_STORM_OF_VENGEANCE, oTarget))             return SPELL_STORM_OF_VENGEANCE;
    if(GetHasSpell(SPELL_SHAPECHANGE, oTarget))                    return SPELL_SHAPECHANGE;
    if(GetHasSpell(SPELL_DOMINATE_MONSTER, oTarget))               return SPELL_DOMINATE_MONSTER;
    if(GetHasSpell(SPELL_ELEMENTAL_SWARM, oTarget))                return SPELL_ELEMENTAL_SWARM;
    if(GetHasSpell(SPELL_SUMMON_CREATURE_IX, oTarget))             return SPELL_SUMMON_CREATURE_IX;
    if(GetHasSpell(SPELL_GATE, oTarget))                           return SPELL_GATE;
    if(GetHasSpell(SPELL_ENERGY_DRAIN, oTarget))                   return SPELL_ENERGY_DRAIN;
    if(GetHasSpell(SPELL_UNDEATHS_ETERNAL_FOE, oTarget))           return SPELL_UNDEATHS_ETERNAL_FOE;
    return nSpell;
}

int GetBestAvailableSpell(object oTarget)
{
    int nBestSpell = GetBestL9Spell(oTarget, 99999);
    if(nBestSpell == 99999) nBestSpell = GetBestL8Spell(oTarget, nBestSpell);
    if(nBestSpell == 99999) nBestSpell = GetBestL7Spell(oTarget, nBestSpell);
    if(nBestSpell == 99999) nBestSpell = GetBestL6Spell(oTarget, nBestSpell);
    if(nBestSpell == 99999) nBestSpell = GetBestL5Spell(oTarget, nBestSpell);
    if(nBestSpell == 99999) nBestSpell = GetBestL4Spell(oTarget, nBestSpell);
    if(nBestSpell == 99999) nBestSpell = GetBestL3Spell(oTarget, nBestSpell);
    if(nBestSpell == 99999) nBestSpell = GetBestL2Spell(oTarget, nBestSpell);
    if(nBestSpell == 99999) nBestSpell = GetBestL1Spell(oTarget, nBestSpell);
    if(nBestSpell == 99999) nBestSpell = GetBestL0Spell(oTarget, nBestSpell);
    return nBestSpell;
}