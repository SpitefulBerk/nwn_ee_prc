
/*
     Script for all racial abilities / penalties that require a heartbeat check
*/

#include "prc_alterations"

void main()
{
    object oPC = OBJECT_SELF;
    object oArea = GetArea(oPC);
    object oSkin = GetPCSkin(oPC);

    int bHasLightSensitive = GetHasFeat(FEAT_LTSENSE, oPC);
    int bHasLightBlindness = GetHasFeat(FEAT_LTBLIND, oPC);

    if(bHasLightSensitive || bHasLightBlindness)
    {
        int bIsEffectedByLight = FALSE;

        if(GetIsObjectValid(oArea)
        && !GetHasFeat(FEAT_DAYLIGHTADAPT, oPC)
        && !GetHasFeat(FEAT_NS_LIGHT_ADAPTION, oPC)
        && GetIsDay()
        && GetIsAreaAboveGround(oArea) == AREA_ABOVEGROUND
        && !GetIsAreaInterior(oArea))
            bIsEffectedByLight = TRUE;

        // light sensitivity
        // those with lightblindess are also sensitive
        if(bIsEffectedByLight)
            ApplyEffectToObject(DURATION_TYPE_TEMPORARY, EffectDazzle(), oPC, 6.5);

        // light blindness
        if(bHasLightBlindness && bIsEffectedByLight)
        {
            // on first entering bright light
            // cause blindness for 1 round
            if(!GetLocalInt(oPC, "EnteredDaylight"))
            {
                effect eBlind = EffectBlindness();
                ApplyEffectToObject(DURATION_TYPE_TEMPORARY, eBlind, oPC, 5.99);
                SetLocalInt(oPC, "EnteredDaylight", TRUE);
                ApplyEffectToObject(DURATION_TYPE_TEMPORARY, EffectDazzle(), oPC, 6.5);
            }
        }

        if(!bIsEffectedByLight && GetLocalInt(oPC, "EnteredDaylight"))
              DeleteLocalInt(oPC, "EnteredDaylight");
    }

    // imaskari underground hide bonus
    //this is in addition to the normal bonus
    if(GetHasFeat(FEAT_SA_HIDEU, oPC))
    {
        if(GetIsAreaAboveGround(oArea) == AREA_UNDERGROUND)
            SetCompositeBonus(oSkin, "SA_Hide_Underground", 2, ITEM_PROPERTY_SKILL_BONUS, SKILL_HIDE);
        else
            SetCompositeBonus(oSkin, "SA_Hide_Underground", 0, ITEM_PROPERTY_SKILL_BONUS, SKILL_HIDE);
    }

    // troglodyte underground hide bonus
    //this is in addition to the normal bonus
    if(GetHasFeat(FEAT_SA_HIDE_TROG, oPC))
    {
        if(GetIsAreaAboveGround(oArea) == AREA_UNDERGROUND)
            SetCompositeBonus(oSkin, "SA_Hide_Underground", 4, ITEM_PROPERTY_SKILL_BONUS, SKILL_HIDE);
        else
            SetCompositeBonus(oSkin, "SA_Hide_Underground", 0, ITEM_PROPERTY_SKILL_BONUS, SKILL_HIDE);
    }

    // forest gnomes bonus to hide in the woods
    //this is in addition to the normal bonus
    if(GetHasFeat(FEAT_SA_HIDEF, oPC) || GetHasFeat(FEAT_BONUS_BAMBOO, oPC))
    {
       if(GetIsAreaNatural(oArea) == AREA_NATURAL
       && GetIsAreaAboveGround(oArea) == AREA_ABOVEGROUND)
           SetCompositeBonus(oSkin, "SA_Hide_Forest", 4, ITEM_PROPERTY_SKILL_BONUS, SKILL_HIDE);
       else
           SetCompositeBonus(oSkin, "SA_Hide_Forest", 0, ITEM_PROPERTY_SKILL_BONUS, SKILL_HIDE);
    }

    // grig bonus to hide in the woods
    if(GetHasFeat(FEAT_SA_HIDEF_5, oPC))
    {
       if(GetIsAreaNatural(oArea) == AREA_NATURAL && 
          GetIsAreaAboveGround(oArea) == AREA_ABOVEGROUND)
           SetCompositeBonus(oSkin, "SA_Hide_Forest", 5, ITEM_PROPERTY_SKILL_BONUS, SKILL_HIDE);
       else
           SetCompositeBonus(oSkin, "SA_Hide_Forest", 0, ITEM_PROPERTY_SKILL_BONUS, SKILL_HIDE);
    }

    // ranger Camouflage
    if(GetHasFeat(FEAT_CAMOUFLAGE, oPC))
    {
       if(GetIsAreaNatural(oArea) == AREA_NATURAL && 
          GetIsAreaAboveGround(oArea) == AREA_ABOVEGROUND)
           SetCompositeBonus(oSkin, "Cls_Camouflage", 5, ITEM_PROPERTY_SKILL_BONUS, SKILL_HIDE);
       else
           SetCompositeBonus(oSkin, "Cls_Camouflage", 0, ITEM_PROPERTY_SKILL_BONUS, SKILL_HIDE);
    }

    //Chameleon Skin Hide bonus for Poison Dusk Lizardfolk
    //+5 to Hide if most of skin is uncovered
    if(GetHasFeat(FEAT_CHAMELEON, oPC))
    {
        if(GetItemInSlot(INVENTORY_SLOT_CHEST, oPC) == OBJECT_INVALID)
            SetCompositeBonus(oSkin, "Chameleon", 5, ITEM_PROPERTY_SKILL_BONUS, SKILL_HIDE);
        else
            SetCompositeBonus(oSkin, "Chameleon", 0, ITEM_PROPERTY_SKILL_BONUS, SKILL_HIDE);
    }
}
